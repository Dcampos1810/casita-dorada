<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use Barryvdh\DomPDF\Facade as PDF;


class OrdersController extends Controller
{
    public function getOrders()
    {
        $orders = DB::table('orders')
            ->join('shippings', 'shippings.id', '=', 'orders.shipping_id')
            ->join('payments', 'payments.id', '=', 'orders.payment_id')
            ->join('customer_addresses', 'customer_addresses.id', '=', 'orders.customer_address_id')
            ->join('customers', 'customers.id', '=', 'customer_addresses.customer_id')
            ->join('postal_codes', 'postal_codes.id', '=', 'customer_addresses.postal_code_id')
            ->select('orders.*', DB::raw('DATE_FORMAT(orders.created_at, "%d/%m/%Y") AS fechaCompra'),
                'shippings.name as tipoEnvio',
                'payments.name as metodoPago',
                'postal_codes.code as postal_code',
                DB::raw('concat(customer_addresses.address, customer_addresses.colony, customer_addresses.city) as customer_address'),
                'customers.name as customer')
            ->orderBy('orders.id', 'desc')
            ->get();

            // dd($orders);
        return view('backend.orders.index', compact('orders'));
    }

    public function adminDownloadPDF(Request $request)
    {
        setlocale(LC_TIME, "spanish");
        $fecha_orden = Order::find($request->order)->first();
        $date = str_replace('-', '/', $fecha_orden->created_at);
        $fecha = strftime("%d/%B/%Y", strtotime($date));
        $order = DB::table('orders')
            ->join('customer_addresses', 'customer_addresses.id', '=', 'orders.customer_address_id')
            ->join('customers', 'customers.id', '=', 'customer_addresses.customer_id')
            ->join('shippings', 'shippings.id', '=', 'orders.shipping_id')
            ->join('payments', 'payments.id', '=', 'orders.payment_id')
            ->join('postal_codes', 'postal_codes.id', '=', 'customer_addresses.postal_code_id')
                ->where('orders.id', '=', $request->order)
                ->select('orders.id', 'orders.total', 'orders.status',
                         'customer_addresses.address', 'customer_addresses.colony', 'customer_addresses.city',
                         'postal_codes.code as code',
                         'customers.id as customer_id','customers.name as nombre', 'customers.phone',
                         'shippings.name as tipoEnvio', 'shippings.time as tiempoEnvio', 'shippings.price as precioEnvio',
                         'payments.name as metodoPago', 'payments.slug as tipoPago')
                ->first();

        $subtotal = ($order->total - $order->precioEnvio);

        $details = DB::table('order_details')
        ->join('product_contents', 'product_contents.id', '=', 'order_details.product_content_id')
        ->join('products', 'products.id', '=', 'product_contents.product_id')
        ->where('order_details.order_id', '=', $request->order)
        ->select('products.name', 'products.image',
                'order_details.price', 'order_details.quantity', 'order_details.total', 'order_details.comments')
        ->get();

        $pdf_admin = PDF::loadView('layouts.emails.adminOrder', compact('order', 'fecha', 'details', 'subtotal'));
        $pdf_admin->setPaper("legal", "portrait");
        return $pdf_admin->download();
    }
}
