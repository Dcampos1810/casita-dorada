<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Shippings;
use Illuminate\Http\Request;
use Response;

class ShippingsController extends Controller
{
    public function getShippings()
    {
        $shippings = Shippings::get();
        return view('backend.enterprises.shippings', compact('shippings'));
    }

    public function saveShipping(Request $request)
    {
        $newShipping = Shippings::create([
            'name' => $request->name,
            'price' => $request->price,
            'time' => $request->time
        ]);

        return Response::json([
            'success' => true,
            'message' => "Costo de Envío Creado Correctamente",
            'shipping' => $newShipping
        ]);
    }

    public function getShipping(Request $request)
    {
        $shipping = Shippings::where('id', '=', $request->id)->first();
        return $shipping;
    }

    public function updateShipping(Request $request)
    {
        $shipping = Shippings::where('id', '=', $request->id)->first();

        if(!is_null($shipping)){

            $shipping->name = $request->name;
            $shipping->price = $request->price;
            $shipping->time = $request->time;
            $shipping->status = $request->status;
            $shipping->update();

            return Response::json([
                'success' => true,
                'message' => "Costo de Envío Actualizado Correctamente",
                'shipping' => $shipping
            ]);



        }else{
            return Response::json([
                'success' => false,
                'message' => "No existe el Costo de Envío"
            ]);
        }
    }

    public function deleteshipping(Request $request)
    {
        $shipping = Shippings::where('id', $request->id)->first();
        $shipping->delete();
    }
}
