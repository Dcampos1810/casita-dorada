<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Bebida;
use App\Models\ProductContent;
use App\Models\Subcategory;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;

class BebidasController extends Controller
{
    public function getBebidas()
    {
        $bebidas = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->join('categories', 'categories.id', '=', 'subcategories.category_id')
            ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
            ->where('categories.slug', '=', 'bebidas')
            ->whereIn('subcategories.slug', ['cervezas', 'refrescos', 'licores', 'cocteleria', 'micheladas'])
            ->select('products.*', 'subcategories.name as subcategory', 'product_contents.price as price')
            ->get();

        $subcate = DB::table('subcategories')
            ->join('categories', 'categories.id', '=', 'subcategories.category_id')
            ->where('categories.slug', '=', 'bebidas')
            ->select('subcategories.*')
            ->orderBy('subcategories.name', 'asc')
            ->get();
        // dd($subcate);

        return view('backend.bebidas.index', compact('bebidas', 'subcate'));
    }

    public function saveBebida(Request $request)
    {
        $newBebida = Product::where('slug', '=', $request->modal_slug)->first();

        if(is_null($newBebida)){
            if($request->hasFile('modal_image')){
                $imagen = dirname(base_path(), 1).'/public_html/backend/img/bebidas/'.$request->modal_slug.'.'.$request->modal_image->getClientOriginalExtension();
                File::delete($imagen);

                $imageName = $request->modal_slug.'.'.$request->modal_image->getClientOriginalExtension();
                $routeImage = 'backend/img/bebidas/'.$imageName;

                $bebida = new Product();
                $bebida->name = $request->modal_name;
                $bebida->slug = $request->modal_slug;
                $bebida->image = $routeImage;
                $bebida->description = $request->modal_description;
                $bebida->subcategory_id = $request->modal_subcategory_id;
                $bebida->status = 'I';

                $bebida->save();

                $request->modal_image->move(dirname(base_path(), 1).'/public_html/backend/img/bebidas/', $imageName);

                ProductContent::create([
                    'product_id' => $bebida->id,
                    'type' => 'BEBIDA',
                    'price' => $request->modal_price
                ]);

                return redirect()->back()->with('success', 'Bebida Creada Correctamente');
            }
            else{
                $routeImage = 'backend/img/logos/logo_casita.png';
                $bebida = new Product();
                $bebida->name = $request->modal_name;
                $bebida->slug = $request->modal_slug;
                $bebida->image = $routeImage;
                $bebida->description = $request->modal_description;
                $bebida->subcategory_id = $request->modal_subcategory_id;
                $bebida->status = 'I';

                $bebida->save();

                ProductContent::create([
                    'product_id' => $bebida->id,
                    'type' => 'BEBIDA',
                    'price' => $request->modal_price
                ]);

                return redirect()->back()->with('success', 'Bebida Creada Correctamente');

            }

        }
        else{
            return redirect()->back()->with('errors', 'Ya existe esa Bebida');
        }
    }

    public function getBebida(Request $request)
    {
        $bebida = DB::table('products')
            ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
            ->where('products.id', '=', $request->id)
            ->select('products.*')
            ->get();
        // dd($bebida);
        return $bebida;
    }

    public function updateBebida(Request $request)
    {
        $bebida = Product::where('id', '=', $request->modal_update_id)->first();

        if(!is_null($bebida)){

            $slug = DB::table('products')
                ->where('id', '<>', $request->modal_update_id)
                ->where('slug', '=', $request->modal_update_slug)
                ->first();

            if(!$slug){
                if($request->hasFile('modal_update_image')){
                    $file = dirname(base_path(), 1).'/public_html/'.$bebida->image;
                        File::delete($file);
                        $imageName = $request->modal_update_slug.'.'.$request->modal_update_image->getClientOriginalExtension();
                        $route = 'backend/img/bebidas/'.$imageName;

                        $bebida->name = $request->modal_update_name;
                        $bebida->slug = $request->modal_update_slug;
                        $bebida->image = $route;
                        $bebida->subcategory_id = $request->modal_update_subcategory_id;
                        $bebida->status = $request->modal_update_status;
                        $bebida->description = $request->modal_update_description;
                        $bebida->update();

                        $request->modal_update_image->move(dirname(base_path(), 1).'/public_html/backend/img/bebidas/',$imageName);

                        $contenido = ProductContent::where('product_id', $bebida->id)->first();

                        $contenido->price = $request->modal_update_price;
                        $contenido->update();

                    return redirect()->back()->with('success', 'Bebida Actualizada Correctamente');
                }
                else{
                    $bebida->name = $request->modal_update_name;
                    $bebida->slug = $request->modal_update_slug;
                    $bebida->subcategory_id = $request->modal_update_subcategory_id;
                    $bebida->description = $request->modal_update_description;
                    $bebida->status = $request->modal_update_status;
                    $bebida->update();

                    $contenido = ProductContent::where('product_id', $bebida->id)->first();

                    $contenido->price = $request->modal_update_price;
                    $contenido->update();

                    return redirect()->back()->with('success', 'Bebida Actualizada Correctamente');
                }

            }
            else{
                return redirect()->back()->with('errors', 'Ya existe esa Bebida');
            }

        }
        else{
            return redirect()->back()->with('errors', 'No existe esa Bebida');
        }
    }

    public function deleteBebida(Request $request)
    {
        $logo = Product::where('id', $request->id)->select('image')->first();

        $image = dirname(base_path(), 1).'/public_html/'.$logo->image;


        File::delete($image);
        $bebida = Product::where('id', $request->id)->first();

        $bebida->delete();
    }
}
