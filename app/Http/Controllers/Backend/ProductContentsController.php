<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductContent;
use Illuminate\Http\Request;
use DB;
use Response;

class ProductContentsController extends Controller
{
    public function getProductContents()
    {
        $product_contents = DB::table('product_contents')
            ->join('products', 'products.id', '=', 'product_contents.product_id')
            ->select('product_contents.*', 'products.name as product', 'products.image')
            ->orderby('products.name', 'asc')
            ->get();

        $products = Product::orderBy('name', 'asc')->get();

        return view('backend.products.contents', compact('product_contents', 'products'));
    }

    public function saveProductContent(Request $request)
    {
        $product = Product::where('id', $request->modal_product_id)->first();
        // dd($product);

        if(!is_null($product)){
            $productContent = ProductContent::create([
                'product_id' => $request->modal_product_id,
                'type' => $request->modal_type,
                'price' => $request->modal_price
            ]);

            $product = Product::where('id', $request->modal_product_id)->select('image', 'name')->first();

            return Response::json([
                'success' => true,
                'message' => "Contenido de Producto Guardado Correctamente",
                'productContent' => $productContent,
                'product' => $product
            ]);
        }
        else{
            return Response::json([
                'success' => false,
                'message' => "No Existe ese Producto"
            ]);
        }

    }

    public function getProductContent(Request $request)
    {
        $product_content = ProductContent::where('id', $request->id)->first();
        return $product_content;
    }

    public function updateProductContent(Request $request)
    {
        $product_content = ProductContent::where('id', $request->modal_update_id)->first();
        // dd($product_content);

        if(!is_null($product_content)){
            $product = Product::where('id', $request->modal_update_product_id)->first();

            if(!is_null($product)){
                $product_content->product_id = $request->modal_update_product_id;
                $product_content->type = $request->modal_update_type;
                $product_content->price = $request->modal_update_price;
                $product_content->status = $request->modal_update_status;
                $product_content->update();

                return Response::json([
                    'success' => true,
                    'message' => "Contenido de Producto Guardado Correctamente",
                    'productContent' => $product_content,
                    'product' => $product
                ]);
            }
            else{
                return Response::json([
                    'success' => false,
                    'message' => "No Existe ese Producto"
                ]);
            }
        }
        else{
            return Response::json([
                'success' => false,
                'message' => "No Existe ese Contenido de Producto"
            ]);
        }
    }

    public function deleteProductContent(Request $request)
    {
        $product_content = ProductContent::where('id', $request->id);
        $product_content->delete();
    }
}
