<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;

class ProductsController extends Controller
{
    public function getProducts()
    {
        $products = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->join('categories', 'categories.id', '=', 'subcategories.category_id')
            ->where('categories.slug', '=', 'alimentos')
            ->select('products.*', 'subcategories.name as subcategory')
            ->get();

        $subcategories = DB::table('subcategories')->orderBy('name', 'asc')->get();

        return view('backend.products.index', compact('products', 'subcategories'));
    }

    public function saveProduct(Request $request)
    {
        $newProduct = Product::where('slug', '=', $request->modal_slug)->first();

        if(is_null($newProduct)){
            if($request->hasFile('modal_image')){
                $imagen = dirname(base_path(), 1).'/public_html/backend/img/products/'.$request->modal_slug.'.'.$request->modal_image->getClientOriginalExtension();
                File::delete($imagen);

                $imageName = $request->modal_slug.'.'.$request->modal_image->getClientOriginalExtension();
                $routeImage = 'backend/img/products/'.$imageName;

                $product = new Product();
                $product->name = $request->modal_name;
                $product->slug = $request->modal_slug;
                $product->image = $routeImage;
                $product->description = $request->modal_description;
                $product->subcategory_id = $request->modal_subcategory_id;
                $product->status = 'I';

                $product->save();

                $request->modal_image->move(dirname(base_path(), 1).'/public_html/backend/img/products/', $imageName);
                return redirect()->back()->with('success', 'Producto Creado Correctamente');
            }
            else{
                $routeImage = 'backend/img/logos/logo_nombre.png';
                $product = new Product();
                $product->name = $request->modal_name;
                $product->slug = $request->modal_slug;
                $product->image = $routeImage;
                $product->description = $request->modal_description;
                $product->subcategory_id = $request->modal_subcategory_id;
                $product->status = 'I';

                $product->save();

                return redirect()->back()->with('success', 'Producto Creado Correctamente');
            }




        }
        else{
            return redirect()->back()->with('errors', 'Ya existe ese Producto');
        }
    }

    public function getProduct(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        return $product;
    }

    public function updateProduct(Request $request)
    {
        $product = Product::where('id', '=', $request->modal_update_id)->first();

        if(!is_null($product)){

            $slug = DB::table('products')
                ->where('id', '<>', $request->modal_update_id)
                ->where('slug', '=', $request->modal_update_slug)
                ->first();

            if(!$slug){
                if($request->hasFile('modal_update_image')){
                    $file = dirname(base_path(), 1).'/public_html/'.$product->image;
                        File::delete($file);
                        $imageName = $request->modal_update_slug.'.'.$request->modal_update_image->getClientOriginalExtension();
                        $route = 'backend/img/products/'.$imageName;

                        $product->name = $request->modal_update_name;
                        $product->slug = $request->modal_update_slug;
                        $product->image = $route;
                        $product->subcategory_id = $request->modal_update_subcategory_id;
                        $product->status = $request->modal_update_status;
                        $product->update();

                        $request->modal_update_image->move(dirname(base_path(), 1).'/public_html/backend/img/products/',$imageName);

                    return redirect()->back()->with('success', 'Producto Actualizado Correctamente');
                }
                else{
                    $product->name = $request->modal_update_name;
                    $product->slug = $request->modal_update_slug;
                    $product->subcategory_id = $request->modal_update_subcategory_id;
                    $product->status = $request->modal_update_status;
                    $product->update();

                    return redirect()->back()->with('success', 'Producto Actualizado Correctamente');
                }

            }
            else{
                return redirect()->back()->with('errors', 'Ya existe ese Producto');
            }

        }
        else{
            return redirect()->back()->with('errors', 'No existe ese Producto');
        }
    }

    public function deleteProduct(Request $request)
    {
        $producto = Product::where('id', $request->id)->select('image')->first();

        $image = dirname(base_path(), 1).'/public_html/'.$producto->image;


        File::delete($image);
        $product = Product::where('id', $request->id)->first();

        $product->delete();
    }
}
