<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Response;

class CategoriesController extends Controller
{
    public function getCategories()
    {
        $categories = Category::get();
        return view('backend.categories.list', compact('categories'));
    }

    public function saveCategory(Request $request)
    {
        $category = Category::where('slug', '=', $request->modal_slug)->first();

        if(is_null($category)){
            $category = Category::create([
                'name' => $request->modal_name,
                'slug' => $request->modal_slug,
                'status' => 'I'
            ]);

            return Response::json([
                'success' => true,
                'message' => 'Categoría Creada Correctamente',
                'category' => $category
            ]);
        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'Ya existe esa Categoría',
                'category' => null
            ]);
        }
    }

    public function getCategory(Request $request)
    {
        $category = Category::where('id', '=', $request->id)->first();
        return $category;
    }

    public function updateCategory(Request $request)
    {
        $category = Category::where('id', '=', $request->modal_update_id)->first();

        if(!is_null($category)){

            $slug = Category::where([['slug', '=', $request->modal_update_slug], ['id', '<>', $request->modal_update_id]])->first();

            if(is_null($slug)){
                $category->name = $request->modal_update_name;
                $category->slug = $request->modal_update_slug;
                $category->status = $request->modal_update_status;
                $category->update();

                return Response::json([
                    'success' => true,
                    'message' => 'Categoría Actualizada Correctamente',
                    'category' => $category
                ]);

            }
            else{
                return Response::json([
                    'success' => false,
                    'message' => 'Ya existe esa Categoría',
                    'category' => null
                ]);
            }

        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'No existe esa Categoría',
                'category' => null
            ]);
        }
    }

    public function deleteCategory(Request $request)
    {
        $category = Category::where('id', $request->id)->first();
        $category->delete();
    }
}
