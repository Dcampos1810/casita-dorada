<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Enterprise;
use Illuminate\Http\Request;
use File;

class EnterprisesController extends Controller
{
    public function getEnterprises()
    {
        $enterprises = Enterprise::get();
        // dd($enterprises);
        return view('backend.enterprises.enterprise', compact('enterprises'));
    }

    public function saveEnterprise(Request $request)
    {
        $image = dirname(base_path(), 1).'/public_html/backend/img/logos/'.$request->modal_logo->getClientOriginalName();

        File::delete($image);

        $imageName = $request->modal_logo->getClientOriginalName();
        $route = 'backend/img/logos/'.$imageName;

        $enterprise = new Enterprise();
        $enterprise->name = $request->modal_name;
        $enterprise->logo = $route;
        $enterprise->address = $request->modal_address;
        $enterprise->email = $request->modal_email;
        $enterprise->phone = $request->modal_phone;
        $enterprise->whatsapp = $request->modal_whatsapp;
        $enterprise->description = $request->modal_description;
        $enterprise->card = $request->modal_card;
        $enterprise->clabe = $request->modal_clabe;
        $enterprise->save();


        $request->modal_logo->move(dirname(base_path(), 1).'/public_html/backend/img/logos/', $imageName);
        return redirect()->back()->with('success', 'Empresa Creada Correctamente');

    }

    public function getEnterprise(Request $request)
    {
        $enterprise = Enterprise::where('id', '=', $request->id)->first();
        // dd($enterprise);
        return $enterprise;
    }

    public function updateEnterprise(Request $request)
    {

        $enterprise = Enterprise::where('id', $request->modal_update_id)->first();
        // dd($enterprise);

        if(!is_null($enterprise)){
            if($request->hasFile('modal_update_logo')){
                 // Server
                 $file = dirname(base_path(), 1).'/public_html/backend/img/logos/'.$request->modal_update_logo->getClientOriginalName();
                 File::delete($file);


                 $imageName = $request->modal_update_logo->getClientOriginalName();

                 $enterprise->name = $request->modal_update_name;
                 $enterprise->logo = 'backend/img/logos/'.$imageName;
                 $enterprise->address = $request->modal_update_address;
                 $enterprise->email = $request->modal_update_email;
                 $enterprise->phone = $request->modal_update_phone;
                 $enterprise->whatsapp = $request->modal_update_whatsapp;
                 $enterprise->card = $request->modal_update_card;
                 $enterprise->clabe = $request->modal_update_clabe;
                 $enterprise->description = $request->modal_update_description;
                 $enterprise->status = $request->modal_update_status;
                 $enterprise->update();
                 $request->modal_update_logo->move(dirname(base_path(), 1).'/public_html/backend/img/logos/',$imageName);

                 return redirect()->back()->with('success', 'Empresa Actualizada Correctamente');
            }
            else{
                $enterprise->name = $request->modal_update_name;
                $enterprise->address = $request->modal_update_address;
                $enterprise->email = $request->modal_update_email;
                $enterprise->phone = $request->modal_update_phone;
                $enterprise->whatsapp = $request->modal_update_whatsapp;
                $enterprise->description = $request->modal_update_description;
                $enterprise->status = $request->modal_update_status;
                $enterprise->update();

                return redirect()->back()->with('success', 'Empresa Actualizada Correctamente');
            }
        }
        else{
            return redirect()->back()->with('errors', 'No existe esa Empresa');
        }
    }

    public function deleteEnterprise(Request $request)
    {
        $image = Enterprise::where('id', $request->id)->select('logo')->first();
        $file = dirname(base_path(), 1).'/public_html/'.$image->logo;


        File::delete($file);
        $enterprise = Enterprise::where('id', $request->id)->first();

        $enterprise->delete();
    }
}
