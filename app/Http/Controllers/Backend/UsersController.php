<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Response;

class UsersController extends Controller
{
    public function getUsers()
    {
        $users = User::get();
        return view('backend.users.list', compact('users'));
    }

    public function saveUser(Request $request)
    {
        $user = DB::table('users')
            ->where('email', '=', $request->email)
            ->first();

        if(is_null($user)){
            $newUser = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password
            ]);

            return Response::json([
                'success' => true,
                'message' => "Usuario Creado Correctamente",
                'user' => $newUser
            ]);
        }
        else{
            return Response::json([
                'success' => false,
                'message' => "El Usuario con estos datos ya existe",
                'user' => null
            ]);
        }
    }

    public function getUser(Request $request)
    {
        $user = User::where('id', '=', $request->id)->first();
        return $user;
    }

    public function updateUser(Request $request)
    {
        $user = User::where('id', '=', $request->id)->first();

        if(!is_null($user)){

            $email = User::where([['email', '=', $request->email], ['id', '<>', $request->id]])->first();

            if(!$email){
                $user->name = $request->name;
                $user->email = $request->email;
                $user->status = $request->status;
                $user->update();

                return Response::json([
                    'success' => true,
                    'message' => "Usuario Actualizado Correctamente",
                    'user' => $user
                ]);
            }
            else{
                return Response::json([
                    'success' => false,
                    'message' => "Existe un Usuario con este correo"
                ]);
            }

        }else{
            return Response::json([
                'success' => false,
                'message' => "No existe el usuario"
            ]);
        }
    }

    public function deleteUser(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        $user->delete();
    }
}
