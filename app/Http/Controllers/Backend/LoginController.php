<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected $redirectTo = '/admin';
    protected $guard = 'admin';

    public function showLogin()
    {
        return view('backend.login');
    }

    public function login(Request $request)
    {
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]) == false){
            $errors = [
                'error' => 'Usuarios y/o Contraseña Incorrectos'
            ];
            return redirect()->route('admin.showLogin')->withErrors($errors);
        }
        else{
            return redirect()->intended('/admin/dashboard');
        }

    }

    public function logout()
    {
        Auth::guard($this->guard)->logout();
        return redirect()->to('/admin/login');
    }
}
