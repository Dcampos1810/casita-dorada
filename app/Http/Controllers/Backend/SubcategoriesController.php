<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class SubcategoriesController extends Controller
{
    public function getSubcategories()
    {
        $subs = DB::table('subcategories')
            ->join('categories', 'categories.id', '=', 'subcategories.category_id')
            ->select('subcategories.*', 'categories.name as categoria')
            ->get();
        $categories = DB::table('categories')->orderBy('name', 'asc')->get();

        // dd($subcategories);

        return view('backend.subcategories.subcategories', compact('subs', 'categories'));
    }

    public function saveSubcategory(Request $request)
    {
        $newSubcategory =  Subcategory::where('slug', '=', $request->modal_slug)->first();

        if(is_null($newSubcategory)){

            $subcategory = new Subcategory();
            $subcategory->name = $request->modal_name;
            $subcategory->slug = $request->modal_slug;
            $subcategory->category_id = $request->modal_category_id;
            $subcategory->status = 'I';

            $subcategory->save();

            $category = Category::where('id', $subcategory->category_id)->select('name')->first();

            return Response::json([
                'success' => true,
                'message' => 'Subcategoría Creada Correctamente',
                'subcategory' => $subcategory,
                'category' => $category
            ]);

        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'Ya Existe esa Subcategoría',
                'subcategory' => null
            ]);
        }
    }

    public function getSubcategory(Request $request)
    {
        $subcategory = Subcategory::where('id', $request->id)->first();

        return $subcategory;
    }

    public function updateSubcategory(Request $request)
    {
        $subcategory = Subcategory::where('id', '=', $request->modal_update_id)->first();

        if(!is_null($subcategory)){

            $slug = DB::table('subcategories')
                ->where('slug', '=', $request->modal_update_slug)
                ->where('id', '<>', $request->modal_update_id)
                ->first();

            if(is_null($slug)){
                // dd("Entra");
                $subcategory->name = $request->modal_update_name;
                $subcategory->slug = $request->modal_update_slug;
                $subcategory->category_id = $request->modal_update_category_id;
                $subcategory->status = $request->modal_update_status;
                $subcategory->update();

                $category = Category::where('id', $subcategory->category_id)->select('name')->first();

                return Response::json([
                    'success' => true,
                    'message' => 'Subcategoría Actualizada Correctamente',
                    'subcategory' => $subcategory,
                    'category' => $category
                ]);
            }
            else{
                return Response::json([
                    'success' => false,
                    'message' => 'Ya Existe esa Subcategoría',
                    'subcategory' => null
                ]);
            }

        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'No Existe esa Subcategoría',
                'subcategory' => null
            ]);
        }
    }

    public function deleteSubcategory(Request $request)
    {
        $subcategory = Subcategory::where('id', $request->id)->first();

        $subcategory->delete();
    }
}
