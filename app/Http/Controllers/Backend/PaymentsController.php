<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use App\Models\Payment;

class PaymentsController extends Controller
{
    public function getPayments()
    {
        $payments = Payment::get();
        // dd($enterprises);
        return view('backend.enterprises.payments', compact('payments'));
    }

    public function savePayment(Request $request)
    {
        $image = dirname(base_path(), 1).'/public_html/backend/img/payments/'.$request->modal_logo->getClientOriginalName();

        File::delete($image);

        $imageName = $request->modal_logo->getClientOriginalName();
        $route = 'backend/img/payments/'.$imageName;

        $payment = new Payment();
        $payment->name = $request->modal_name;
        $payment->image = $route;
        $payment->slug = $request->modal_slug;

        $payment->save();


        $request->modal_logo->move(dirname(base_path(), 1).'/public_html/backend/img/payments/', $imageName);
        return redirect()->back()->with('success', 'Forma de Pago Creada Correctamente');

    }

    public function getPayment(Request $request)
    {
        $payment = Payment::where('id', '=', $request->id)->first();
        // dd($enterprise);
        return $payment;
    }

    public function updatePayment(Request $request)
    {

        $payment = Payment::where('id', $request->modal_update_id)->first();
        // dd($enterprise);

        if(!is_null($payment)){
            if($request->hasFile('modal_update_logo')){
                 // Server
                 $file = dirname(base_path(), 1).'/public_html/backend/img/payments/'.$request->modal_update_logo->getClientOriginalName();
                 File::delete($file);


                 $imageName = $request->modal_update_logo->getClientOriginalName();

                 $payment->name = $request->modal_update_name;
                 $payment->image = 'backend/img/payments/'.$imageName;
                 $payment->slug = $request->modal_update_slug;
                 $payment->status = $request->modal_update_status;
                 $payment->update();
                 $request->modal_update_logo->move(dirname(base_path(), 1).'/public_html/backend/img/payments/',$imageName);

                 return redirect()->back()->with('success', 'Forma de Pago Actualizada Correctamente');
            }
            else{
                $payment->name = $request->modal_update_name;
                $payment->slug = $request->modal_update_slug;
                $payment->status = $request->modal_update_status;
                $payment->update();

                return redirect()->back()->with('success', 'Forma de Pago Actualizada Correctamente');
            }
        }
        else{
            return redirect()->back()->with('errors', 'No existe esa Forma de Pago');
        }
    }

    public function deletePayment(Request $request)
    {
        $image = Payment::where('id', $request->id)->select('image')->first();
        $file = dirname(base_path(), 1).'/public_html/'.$image->logo;


        File::delete($file);
        $payment = Payment::where('id', $request->id)->first();

        $payment->delete();
    }
}
