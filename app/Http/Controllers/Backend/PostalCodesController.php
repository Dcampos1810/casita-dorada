<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PostalCode;
use App\Models\Shippings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class PostalCodesController extends Controller
{
    public function getPostalCodes()
    {
        $postalCodes = DB::table('postal_codes')
            ->join('shippings', 'shippings.id', '=', 'postal_codes.shipping_id')
            ->select('postal_codes.*', 'shippings.name as zona')
            ->get();

        $shippings = Shippings::get();

        return view('backend.enterprises.postal_codes', compact('postalCodes', 'shippings'));
    }

    public function savePostalCode(Request $request)
    {
        $shipping = Shippings::where('id', $request->shipping_id)->first();

        if(!is_null($shipping)){
            $code = PostalCode::create([
                'code' => $request->code,
                'shipping_id' => $request->shipping_id
            ]);
            $shipping = Shippings::where('id', $code->shipping_id)->select('name')->first();

            return Response::json([
                'success' => true,
                'message' => 'Código Postal Agregado Correctamente',
                'code' => $code,
                'zona' => $shipping
            ]);

        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'No Existe esa Zona de Envío',
                'code' => null
            ]);
        }
    }

    public function getPostalCode(Request $request)
    {
        $code = PostalCode::where('id', $request->id)->first();
        return $code;
    }

    public function updatePostalCode(Request $request)
    {
        $code = PostalCode::where('id', $request->id)->first();
        $shipping = Shippings::where('id', $request->shipping_id)->first();

        if(!is_null($code)){
            if(!is_null($shipping)){
                $code->code = $request->code;
                $code->shipping_id = $request->shipping_id;
                $code->status = $request->status;
                $code->update();

                return Response::json([
                    'success' => true,
                    'message' => 'Código Postal Actualizado Correctamente',
                    'code' => $code,
                    'zona' => $shipping
                ]);
            }
            else{

                return Response::json([
                    'success' => false,
                    'message' => 'No Existe esa Zona de Envío',
                    'code' => null
                ]);
            }
        }
        else{

            return Response::json([
                'success' => false,
                'message' => 'No Existe ese Código Postal',
                'code' => null
            ]);
        }
    }

    public function deletePostalCode(Request $request)
    {
        $code = PostalCode::where('id', $request->id)->first();
        $code->delete();
    }

    public function validarCobertura(Request $request)
    {
        $code = PostalCode::where('code', '=', $request->code)->first();

        if(!is_null($code)){
            return Response::json([
                'success' => true,
                'message' => 'Contamos con cobertura para tu área',
                'code' => $code->id
            ]);
        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'Por el momento no contamos con cobertura para tu área'
            ]);
        }
    }

    public function setCP(Request $request)
    {
        $request->session()->put('postal_code', $request->code);
        dd($request->session()->get('postal_code'));

    }

    public function getShippingPrice(Request $request)
    {
        $price = DB::table('postal_codes')
            ->join('shippings', 'shippings.id', '=', 'postal_codes.shipping_id')
            ->where('postal_codes.id', '=', $request->postal_code_id)
            ->select('shippings.price as price', "shippings.id as shipping_id")
            ->get();

        return $price;
    }
}
