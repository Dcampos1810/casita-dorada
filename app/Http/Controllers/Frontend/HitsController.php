<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class HitsController extends Controller
{
    public function getHits()
    {
        $hits = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'hits-casita'],
                ['products.status', '=', 'A']
            ])
            ->select('products.*')
            ->get();

        return $hits;
    }
}
