<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subcategory;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{
    public function welcome(Request $request)
    {
        $platillos = new Request(array(
            "todos",
            "especialidades",
            "cheviches",
            "cocteles",
            "empanizados",
            "aguachiles",
            "pastas",
            "hits-casita"
        ));

        $all = $this->getProducts($platillos[0]);
        $especialidades = $this->getProducts($platillos[1]);
        $cheviches = $this->getProducts($platillos[2]);
        $cocteles = $this->getProducts($platillos[3]);
        $empanizados =$this->getProducts($platillos[4]);
        $aguachiles = $this->getProducts($platillos[5]);
        $pastas = $this->getProducts($platillos[6]);
        $hits = $this->getProducts($platillos[7]);
        $subcate = Subcategory::whereNotIn("slug", ['entradas', 'cervezas', 'licores', 'tacos-tostadas', 'refrescos', 'micheladas', 'cocteleria'])
            ->where('status', '=', 'A')
            ->get();

        return view('welcome', compact('especialidades','cheviches', 'cocteles', 'empanizados', 'aguachiles', 'pastas', 'hits', 'subcate', 'all'));
    }

    public function getProducts(String $subcategory)
    {
        if($subcategory == "todos"){
            $products = DB::table('products')
                ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
                ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
                ->whereNotIn('subcategories.slug', ['entradas', 'cervezas', 'licores', 'tacos-tostadas', 'refrescos', 'micheladas', 'cocteleria'])
                ->whereIn('product_contents.type', ['PLATILLO', 'GRANDE'])
                ->where([
                    ['products.status', '=', 'A'],
                    ['product_contents.status', '=', 'A']
                ])
                ->select('products.*', 'product_contents.price as price', 'product_contents.id as product_content_id')
                ->inRandomOrder()
                ->get();
        }
        else{
            $products = DB::table('products')
                ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
                ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
                ->whereIn('product_contents.type', ['PLATILLO', 'GRANDE'])
                ->where([
                    ['subcategories.slug', '=', $subcategory],
                    ['products.status', '=', 'A'],
                    ['product_contents.status', '=', 'A']
                ])
                ->select('products.*', 'product_contents.price as price', 'product_contents.id as product_content_id')
                ->get();
        }

        // dd($products);
        return $products;
    }


}
