<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ChevichesController extends Controller
{
    public function getCheviches()
    {
        $cheviches = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'cheviches'],
                ['products.status', '=', 'A']
            ])
            ->select('products.*')
            ->get();

        return $cheviches;
    }
}
