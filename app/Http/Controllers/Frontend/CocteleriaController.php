<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CocteleriaController extends Controller
{
    public function getCocteleria()
    {
        $cocteleria = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'cocteleria'],
            ])
            ->select('products.*')
            ->get();

        return $cocteleria;
    }
}
