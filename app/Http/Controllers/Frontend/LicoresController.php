<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class LicoresController extends Controller
{
    public function getLicores()
    {
        $licores = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'licores'],
            ])
            ->select('products.*')
            ->get();

        return $licores;
    }
}
