<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\VentaRealizada;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{
    public function createOrder(Request $request)
    {
        setlocale(LC_TIME, "spanish");

        if($request->payment_id == 1){
            $status = 'recibido';
        }
        else{
            $status = 'transaccion';
        }

        $newOrder = Order::create([
            'customer_address_id' => $request->customer_address_id,
            'shipping_id' => $request->shipping_id,
            'payment_id' => $request->payment_id,
            'subtotal' => $request->checkout_subtotal,
            'total' => $request->checkout_total,
            'status' => $status
        ]);

        $order_details = json_decode($request->cartshop_items, true);

        foreach($order_details as $detail){
            OrderDetail::create([
                'order_id' => $newOrder->id,
                'product_content_id' => $detail['product_content_id'],
                'quantity' => $detail['quantity'],
                'price' => $detail['price'],
                'total' => $detail['total'],
                'comments' => $detail['comments']
            ]);

            DB::table('cartshop_items')->delete($detail['id']);
        }

        $order = DB::table('orders')
        ->join('customer_addresses', 'customer_addresses.id', '=', 'orders.customer_address_id')
        ->join('customers', 'customers.id', '=', 'customer_addresses.customer_id')
        ->join('shippings', 'shippings.id', '=', 'orders.shipping_id')
        ->join('payments', 'payments.id', '=', 'orders.payment_id')
        ->join('postal_codes', 'postal_codes.id', '=', 'customer_addresses.postal_code_id')
            ->where('orders.id', '=', $newOrder->id)
            ->select('orders.id', 'orders.total', 'orders.status',
                     'customer_addresses.address', 'customer_addresses.colony', 'postal_codes.code', 'customer_addresses.city',
                     'customers.id as customer_id','customers.name as nombre', 'customers.phone',
                     'shippings.name as tipoEnvio', 'shippings.time as tiempoEnvio', 'shippings.price as precioEnvio',
                     'payments.name as metodoPago', 'payments.slug as tipoPago')
            ->first();

        $details = DB::table('order_details')
            ->join('product_contents', 'product_contents.id', '=', 'order_details.product_content_id')
            ->join('products', 'products.id', '=', 'product_contents.product_id')
            ->where('order_details.order_id', '=', $newOrder->id)
            ->select('products.name', 'products.image',
                    'order_details.price', 'order_details.quantity', 'order_details.total', 'order_details.comments')
            ->get();


        $subtotal = ($order->total - $order->precioEnvio);
        $articulos = json_decode($details, true);
        $detail = "\t";
        foreach ($articulos as $cliente) {

            $detail.= ($cliente["quantity"]. ' '.$cliente["name"]. ' '.$cliente["price"]. ' '.$cliente["total"]. "\n \t\t");
        }

        $fecha_orden = Order::find($order->id)->first();
        $date = str_replace('-', '/', $fecha_orden->created_at);
        $fecha = strftime("%d/%B/%Y", strtotime($date));

        //Creamos el PDF de los Administradores
        $venta = [
            'title' => "Venta de Casita Cheviche Dorada",
            'order_id' => $order->id,
            'payment_method' => $order->metodoPago,
            'customer' => $order->nombre,
            'address' => ($order->address. ' Colonia. '.$order->colony.' C.P. '.$order->code. ' '.$order->city),
            'phone' => $order->phone,
            'total' => $order->total,
            'order_details' => $detail,
            'tipoEnvio' => $order->tipoEnvio
        ];

        $pdf_admin = PDF::loadView('layouts.emails.adminOrder', compact('order', 'fecha', 'details', 'subtotal'));
        $pdf_admin->setPaper("legal", "portrait");
        $output = $pdf_admin->output();
        Mail::to("administracion@casitadorada.com")
        ->send(new VentaRealizada($output, $venta));




        $view = '';
        switch($request->payment_id){
            case 1:
                $view .= 'layouts.frontend.payments.efectivo';
                break;
            case 2:
                $view .= 'layouts.frontend.payments.transferencia';
                break;
        }
        $pedido = $newOrder->id;
        $total = $newOrder->total;
        return view($view, compact('total', 'pedido'));
    }

    public function downloadPDF(Request $request)
    {
        setlocale(LC_TIME, "spanish");
        $fecha_orden = Order::find($request->order)->first();
        $date = str_replace('-', '/', $fecha_orden->created_at);
        $fecha = strftime("%d/%B/%Y", strtotime($date));
        $order = DB::table('orders')
            ->join('customer_addresses', 'customer_addresses.id', '=', 'orders.customer_address_id')
            ->join('customers', 'customers.id', '=', 'customer_addresses.customer_id')
            ->join('shippings', 'shippings.id', '=', 'orders.shipping_id')
            ->join('payments', 'payments.id', '=', 'orders.payment_id')
            ->join('postal_codes', 'postal_codes.id', '=', 'customer_addresses.postal_code_id')
                ->where('orders.id', '=', $request->order)
                ->select('orders.id', 'orders.total', 'orders.status',
                         'customer_addresses.address', 'customer_addresses.colony', 'customer_addresses.city',
                         'postal_codes.code as code',
                         'customers.id as customer_id','customers.name as nombre', 'customers.phone',
                         'shippings.name as tipoEnvio', 'shippings.time as tiempoEnvio', 'shippings.price as precioEnvio',
                         'payments.name as metodoPago', 'payments.slug as tipoPago')
                ->first();

        $subtotal = ($order->total - $order->precioEnvio);

        $order_details = DB::table('order_details')
        ->join('product_contents', 'product_contents.id', '=', 'order_details.product_content_id')
        ->join('products', 'products.id', '=', 'product_contents.product_id')
        ->where('order_details.order_id', '=', $request->order)
        ->select('products.name', 'products.image',
                'order_details.price', 'order_details.quantity', 'order_details.total', 'order_details.comments')
        ->get();


        $view = "";

        switch($order->tipoPago){
            case "efectivo":
                $view .= "layouts.frontend.pdf.efectivo";
            break;

            case "transferencia":
                $view .= "layouts.frontend.pdf.transferencia";
            break;
        }


        //Creamos el PDF del Cliente
        $pdf_cliente = PDF::loadView($view, compact('order', 'fecha', 'order_details', 'subtotal'));
        $pdf_cliente->setPaper("legal", "portrait");
        // $pdf_cliente->set_option('isRemoteEnabled', TRUE);
        // return $pdf_cliente->stream('Ticket de Compra #'.$order->id.'.pdf');
        return $pdf_cliente->download();
        // return $pdf_cliente->download('Ticket de Compra #'.$order->id.'.pdf');
    }

    public function getOrderDetails(Request $request)
    {

        $order = DB::table('orders')
            ->join('customer_addresses', 'orders.customer_address_id', '=', 'customer_addresses.id')
            ->join('customers', 'customers.id', '=', 'customer_addresses.customer_id')
            ->join('postal_codes', 'postal_codes.id', 'customer_addresses.postal_code_id')
            ->join('payments', 'payments.id', '=', 'orders.payment_id')
            ->join('shippings', 'shippings.id', '=', 'postal_codes.shipping_id')
            ->where('orders.id', '=', $request->order_id)
            ->select('orders.id', 'orders.total', 'orders.status',
                    'customer_addresses.address', 'customer_addresses.colony', 'customer_addresses.city',
                    'customers.name as nombre',
                    'postal_codes.code as codigoPostal',
                    'payments.name as metodoPago', 'payments.image as imagenPago',
                    'shippings.price as costoEnvio'
                    )
            ->get();

            $order_details = DB::table('order_details')
            ->join('orders', 'orders.id', '=', 'order_details.order_id')
            ->join('product_contents', 'product_contents.id', '=', 'order_details.product_content_id')
            ->join('products', 'products.id', '=', 'product_contents.product_id')
            ->where('orders.id', '=', $request->order_id)
            ->select('order_details.*',
                        'products.name as producto', 'products.image as imagenProducto'
            )
            ->get();

            $order["order_details"] = $order_details;


            return $order;
    }
}
