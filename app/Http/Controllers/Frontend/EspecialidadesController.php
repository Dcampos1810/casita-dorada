<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class EspecialidadesController extends Controller
{
    public function getEspecialidades()
    {
        $especialidades = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'especialidades'],
                ['products.status', '=', 'A']
            ])
            ->select('products.*')
            ->get();

        return $especialidades;
    }
}
