<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class MicheladasController extends Controller
{
    public function getMicheladas()
    {
        $micheladas = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'micheladas'],
            ])
            ->select('products.*')
            ->get();

        return $micheladas;
    }
}
