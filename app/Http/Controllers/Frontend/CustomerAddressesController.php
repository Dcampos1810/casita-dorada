<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CustomerAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerAddressesController extends Controller
{
    public function getAddresses($customer_id)
    {
        $address = DB::table('customer_addresses')
            ->join('postal_codes', 'postal_codes.id', '=', 'customer_addresses.postal_code_id')
            ->where('customer_id', $customer_id)
            ->select('customer_addresses.*', 'postal_codes.code as postal_code')
            ->first();

        return $address;
    }

    public function saveAddress(Request $request)
    {
        $direccion = CustomerAddress::where('customer_id', $request->customer_id)->first();
        if(is_null($direccion)){
            $address = CustomerAddress::create([
                'customer_id' => $request->customer_id,
                'name' => $request->name,
                'postal_code_id' => $request->postal_code_id,
                'city' => $request->city,
                'address' => $request->direccion,
                'colony' => $request->colony,
                'references' => $request->references
            ]);

            $postal_code = DB::table('postal_codes')
                ->join('customer_addresses', 'customer_addresses.postal_code_id', '=', 'postal_codes.id')
                ->select('postal_codes.code as code')
                ->first();

            return Response::json([
                'success' => true,
                'message' => 'Dirección Guardada Correctamente',
                'address' => $address,
                'postal_code' => $postal_code
            ]);
        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'Ya tiene creado un domicilio'
            ]);
        }

    }

    public function getAddress(Request $request)
    {
        $address = CustomerAddress::where('id', $request->address_id)->first();
        $postal_code = DB::table('postal_codes')
            ->join('customer_addresses', 'customer_addresses.postal_code_id', '=', 'postal_codes.id')
            ->select('postal_codes.code as code')
            ->first();
        $address["postal_code"] = $postal_code;
        return $address;
    }

    public function updateAddress(Request $request)
    {
        $address = CustomerAddress::where('id', $request->id)->first();
        $address->name = $request->name_address;
        $address->address = $request->direccion;
        $address->postal_code_id = $request->postal_code_id;
        $address->customer_id = $request->customer_id;
        $address->city = $request->city;
        $address->colony = $request->colony;
        $address->references = $request->references;
        $address->update();

        $postal_code = DB::table('postal_codes')
            ->join('customer_addresses', 'customer_addresses.postal_code_id', '=', 'postal_codes.id')
            ->where('customer_addresses.id', $address->id)
            ->select('postal_codes.code as code')
            ->first();

        return Response::json([
            'success' => true,
            'message' => 'Dirección Actualizada Correctamente',
            'address' => $address,
            'postal_code' => $postal_code
        ]);
    }

    public function deleteAddress(Request $request)
    {
        $address = CustomerAddress::where('id', $request->address_id)->first();
        $address->delete();
    }
}
