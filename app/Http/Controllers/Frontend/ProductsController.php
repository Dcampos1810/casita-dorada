<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function getProduct(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        $content = DB::table('product_contents')
            ->join('products', 'product_contents.product_id', '=', 'products.id')
            ->whereIn('product_contents.type', ['GRANDE', 'PLATILLO'])
            ->where('product_contents.product_id', '=', $product->id)
            ->select('product_contents.price', 'product_contents.id as product_content_id')
            ->first();

            $product["content"] = $content;
        // dd($product);
        return $product;
    }

    public function getAdicional(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        $content = DB::table('product_contents')
            ->join('products', 'product_contents.product_id', '=', 'products.id')
            ->where('product_contents.product_id', '=', $product->id)
            ->select('product_contents.price as price', 'product_contents.id as product_content_id')
            ->first();

            $product["content"] = $content;
        // dd($product);
        return $product;
    }


}
