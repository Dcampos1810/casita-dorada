<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Subcategory;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ProductContent;

class MenuController extends Controller
{
    public function getMenu()
    {
        $request = new Request(array(
            "todos",
            "entradas",
            "tacos-tostadas",
            "pastas",
            "cheviches",
            "aguachiles",
            "cocteles",
            "especialidades",
            "empanizados",
            "hits-casita"
        ));

        $drinks = new Request(array(
            "refrescos",
            "cervezas",
            "micheladas",
            "cocteleria",
            "licores"
        ));

        $all = $this->getProducts($request[0]);
        $entradas = $this->getProducts($request[1]);
        $tts = $this->getProducts($request[2]);
        $pastas = $this->getProducts($request[3]);
        $cheviches = $this->getProducts($request[4]);
        $aguachiles = $this->getProducts($request[5]);
        $cocteles = $this->getProducts($request[6]);
        $especialidades = $this->getProducts($request[7]);
        $empanizados = $this->getProducts($request[8]);
        $hits = $this->getProducts($request[9]);

        $bebidas = $this->getBebidas($drinks[0]);
        $cervezas = $this->getBebidas($drinks[1]);
        $micheladas = $this->getBebidas($drinks[2]);
        $coctelerias = $this->getBebidas($drinks[3]);
        $licores = $this->getBebidas($drinks[4]);

        $subcate = Subcategory::where('status', '=', 'A')->get();

        return view('frontend.menu', compact('all', 'subcate', 'entradas', 'tts', 'pastas', 'cheviches', 'aguachiles', 'cocteles', 'especialidades',
            'empanizados', 'hits', 'cervezas', 'bebidas', 'micheladas', 'coctelerias', 'licores'));
    }

    public function getProducts(String $subcategory)
    {
        if($subcategory == "todos"){
            $products = DB::table('products')
                ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
                ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
                ->where([
                    ['products.status', '=', 'A'],
                    ['product_contents.status', '=', 'A']
                ])
                ->select(DB::raw("GROUP_CONCAT(DISTINCT product_contents.price SEPARATOR ', ') as prices, GROUP_CONCAT(DISTINCT product_contents.type order by product_contents.type desc SEPARATOR ', ') as presentaciones"), 'products.*')
                ->groupBy('products.id')
                ->get();
        }
        else{
            $products = DB::table('products')
                ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
                ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
                ->where([
                    ['subcategories.slug', '=', $subcategory],
                    ['products.status', '=', 'A'],
                    ['product_contents.status', '=', 'A']
                ])
                ->select(DB::raw("GROUP_CONCAT(DISTINCT product_contents.price SEPARATOR ', ') as prices, GROUP_CONCAT(DISTINCT product_contents.type order by product_contents.type desc SEPARATOR ', ') as presentaciones"), 'products.*')
                ->groupBy('products.id')
                ->get();
        }

        // dd($products);

        return $products;
    }

    public function getBebidas(String $subcategory)
    {
        $bebidas = DB::table('bebidas')
            ->join('subcategories', 'subcategories.id', '=', 'bebidas.subcategory_id')
            ->where([
                ['subcategories.slug', '=', $subcategory],
                ['subcategories.status', '=', 'A'],
                ['bebidas.status', '=', 'A'],
            ])
            ->select('bebidas.*')
            ->get();

        return $bebidas;
    }
}
