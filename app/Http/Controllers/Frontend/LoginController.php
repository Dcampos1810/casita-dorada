<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class LoginController extends Controller
{
    public function login()
    {
        $login = Auth::guard('customers')->attempt(request(['phone', 'password']));
        if($login == false)
        {
            return Response::json([
                "success" => false,
                "message" => "Teléfono y/o Contraseña Incorrectos"
            ]);
        }else{
            return redirect()->to('/');
        }


    }

    public function destroy()
    {
        auth()->logout();

        return redirect()->to('/');
    }
}
