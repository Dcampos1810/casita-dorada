<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class AguachilesController extends Controller
{
    public function getAguachiles()
    {
        $aguachiles = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'aguachiles'],
                ['products.status', '=', 'A']
            ])
            ->select('products.*')
            ->get();

        return $aguachiles;
    }
}
