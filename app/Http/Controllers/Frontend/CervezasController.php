<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CervezasController extends Controller
{
    public function getCervezas()
    {
        $cervezas = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'cervezas']
            ])
            ->select('products.*')
            ->get();

        return $cervezas;
    }
}
