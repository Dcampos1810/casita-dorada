<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cartshop;
use App\Models\CartshopItem;
use Response;
use DB;
use App\Http\Controllers\Frontend\CustomerAddressesController;
use App\Models\Payment;

class CartController extends Controller
{

    public function getCart(Request $request)
    {
        $customer_id = auth()->user()->id;
        $cartshop_items = DB::table('cartshop_items')
            ->join('cartshops', 'cartshops.id', '=', 'cartshop_items.cartshop_id')
            ->join('product_contents', 'product_contents.id', 'cartshop_items.product_content_id')
            ->join('products', 'products.id', '=', 'product_contents.product_id')
            ->where('cartshops.customer_id', '=', $customer_id)
            ->select('cartshop_items.*', 'products.image', 'products.name', 'product_contents.type as type')
            ->get();

        $antojos = DB::table('products')
            ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->whereIn('subcategories.slug', ['entradas', 'tacos-tostadas'])
            ->where([
                ['products.status', '=', 'A'],
                ['subcategories.status', '=', 'A']
            ])
            ->select('products.*', 'product_contents.price as price', 'product_contents.id as product_content_id')
            ->limit(15)
            ->inRandomOrder()
            ->get();

        $bebidas = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->join('product_contents', 'product_contents.product_id', '=', 'products.id')
            ->whereIn('subcategories.slug', ['refrescos', 'micheladas', 'licores', 'cervezas', 'cocteleria'])
            ->where([
                ['products.status', '=', 'A'],
                ['subcategories.status', '=', 'A']
            ])
            ->select('products.*', 'product_contents.price as price', 'product_contents.id as product_content_id')
            ->limit(15)
            ->inRandomOrder()
            ->get();

        $address = (new CustomerAddressesController)->getAddresses($customer_id);

        $payments = Payment::where('status', '=', 'A')->get();

        return view('frontend.cartshop', compact('cartshop_items', 'antojos', 'bebidas', 'address', 'payments'));
    }

    public function getCountItemsCart(Request $request)
    {
        $items = DB::table('cartshop_items')
            ->join('cartshops', 'cartshops.id', '=', 'cartshop_items.cartshop_id')
            ->where('cartshops.customer_id', '=', $request->customer_id)
            ->select('cartshop_items.*')
            ->get();

        // dd($items);
        if(!is_null($items)){
            $cartshop_items = count($items);
        }
        else{
            $cartshop_items = 0;
        }

        return $cartshop_items;
    }

    public function addToCart(Request $request)
    {
        $cartshop = Cartshop::where('customer_id', '=', $request->customer)->first();
        $cartshop_item = CartshopItem::where([
            ['product_content_id', '=', $request->product_content_id],
            ['cartshop_id', '=', $cartshop->id]
        ])->first();

        // dd($request, $cartshop, $cartshop_item);

        if(!is_null($cartshop)){
            if(is_null($cartshop_item)){
                $cartshop_item = CartshopItem::create([
                    'cartshop_id' => $cartshop->id,
                    'product_content_id' => $request->product_content_id,
                    'price' => $request->price,
                    'total' => $request->total,
                    'quantity' => $request->quantity,
                    'comments' => $request->comments
                ]);

            }
            else{
                $cantidad = ($cartshop_item->quantity + $request->quantity);
                $total = ($cantidad * $cartshop_item->price);
                $cartshop_item->quantity = $cantidad;
                $cartshop_item->total = $total;
                $cartshop_item->comments = $request->comments;
                $cartshop_item->update();
            }


            return Response::json([
                'success' => true,
                'message' => 'Producto Agregado Correctamente',
                'item' => $cartshop_item,
                'title' => $request->name,
                'image' => $request->image
            ]);
        }

    }

    public function updateItemCart(Request $request)
    {
        $item = CartshopItem::where('id', $request->id)->first();
        $item->quantity = $request->quantity;
        $item->price = $request->price;
        $item->total = $request->total;
        $item->comments = $request->comments;
        $item->update();
    }

    public function deleteItemCart(Request $request)
    {
        $item = CartshopItem::where('id', $request->id)->first();
        $item->delete();
    }

    public function updateCart(Request $request)
    {
        $cartshop_items = DB::table('cartshop_items')
            ->join('cartshops', 'cartshops.id', '=', 'cartshop_items.cartshop_id')
            ->join('product_contents', 'product_contents.id', 'cartshop_items.product_content_id')
            ->join('products', 'products.id', '=', 'product_contents.product_id')
            ->where('cartshops.customer_id', '=', $request->customer_id)
            ->select('cartshop_items.*', 'products.image', 'products.name', 'product_contents.type as type')
            ->get();

        return json_encode($cartshop_items, true);
    }
}
