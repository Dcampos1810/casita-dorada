<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class EntradasController extends Controller
{
    public function getEntradas()
    {
        $entradas = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'entradas'],
            ])
            ->select('products.*')
            ->get();

        return $entradas;
    }
}
