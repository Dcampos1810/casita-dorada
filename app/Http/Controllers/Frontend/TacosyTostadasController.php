<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class TacosyTostadasController extends Controller
{
    public function getTacosTostadas()
    {
        $tts = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'tacos-tostadas'],
            ])
            ->select('products.*')
            ->get();

        return $tts;
    }
}
