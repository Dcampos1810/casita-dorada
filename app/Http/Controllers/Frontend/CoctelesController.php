<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CoctelesController extends Controller
{
    public function getCocteles()
    {
        $cocteles = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'cocteles'],
                ['products.status', '=', 'A']
            ])
            ->select('products.*')
            ->get();

        return $cocteles;
    }
}
