<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\CustomerAddressesController;
use Illuminate\Support\Facades\DB;

class CustomersController extends Controller
{
    public function getProfile(Request $request)
    {
        $customer_id = auth()->user()->id;
        $customer = DB::table('customers')
            ->where('id', $customer_id)
            ->first();

        $address = (new CustomerAddressesController)->getAddresses($customer_id);

        return view('frontend.profile', compact('customer', 'address'));
    }

    function getMyOrders(Request $request)
    {
        $orders = DB::table('orders')
            ->join('customer_addresses', 'customer_addresses.id', '=', 'orders.customer_address_id')
            ->join('customers', 'customers.id', '=', 'customer_addresses.customer_id')
            ->where('customers.id', '=', $request->customer_id)
            ->select('orders.*')
            ->get();

        return $orders;
    }
}
