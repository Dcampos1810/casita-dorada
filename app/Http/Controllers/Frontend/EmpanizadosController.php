<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class EmpanizadosController extends Controller
{
    public function getEmpanizados()
    {
        $empanizados = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'empanizados'],
                ['products.status', '=', 'A']
            ])
            ->select('products.*')
            ->get();

        return $empanizados;
    }
}
