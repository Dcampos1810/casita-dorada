<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class PastasController extends Controller
{
    public function getPastas()
    {
        $pastas = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'pastas'],
                ['products.status', '=', 'A']
            ])
            ->select('products.*')
            ->get();

        return $pastas;
    }
}
