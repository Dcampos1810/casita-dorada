<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Cartshop;
use Response;

class RegisterController extends Controller
{
    public function store(Request $request)
    {
        $customer = Customer::create([
            'name' => $request->name_register,
           'phone' => $request->phone_register,
            'password' => $request->password_register
        ]);


        Cartshop::create(['customer_id' => $customer->id]);

        auth()->login($customer);

        return redirect()->to('/');
    }

    public function verifyCustomer(Request $request)
    {
        $phone = Customer::where("phone", $request->phone)->first();

        if(is_null($phone)){
            return Response::json([
                'success' => true
            ]);
        }
        else{
            return Response::json([
                'success' => false,
                'message' => 'Ya Existe un Usuario con este Teléfono'
            ]);
        }
    }


}
