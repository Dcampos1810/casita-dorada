<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class BebidasController extends Controller
{
    public function getBebidas()
    {
        $bebidas = DB::table('products')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->where([
                ['subcategories.slug', '=', 'refrescos']
            ])
            ->select('products.*')
            ->get();

        return $bebidas;
    }
}
