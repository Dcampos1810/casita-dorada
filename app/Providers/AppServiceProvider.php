<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Subcategory;
use App\Models\Enterprise;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view) {
            $view->with('subcategories', Subcategory::get());
            $view->with('enterprise', Enterprise::first());
        });
    }
}
