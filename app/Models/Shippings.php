<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shippings extends Model
{
    use HasFactory;

    protected $table = 'shippings';
    protected $fillable = ['id', 'name', 'price', 'time', 'status'];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function postal_codes()
    {
        return $this->hasMany('App\Models\PostalCode');
    }
}
