<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $table = 'order_details';
    protected $fillable = ['id', 'order_id', 'product_content_id', 'quantity', 'price', 'total', 'comments'];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function product_content()
    {
        return $this->belongsTo('App\Models\ProductContent');
    }
}
