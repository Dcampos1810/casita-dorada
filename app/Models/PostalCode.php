<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostalCode extends Model
{
    use HasFactory;
    protected $table = 'postal_codes';
    protected $fillable = ['id', 'code', 'shipping_id', 'status'];

    public function shipping()
    {
        return $this->belongsTo('App\Models\Shipping');
    }

    public function customer_addresses()
    {
        return $this->hasMany('App\Models\CustomerAddress');
    }
}
