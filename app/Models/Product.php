<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['id', 'subcategory_id', 'name', 'image', 'status', 'slug', 'description'];

    public function subcategory()
    {
        return $this->belongsTo('App\Models\Subcategory');
    }

    public function product_contents()
    {
        return $this->hasMany('App\Models\ProductContent');
    }

}
