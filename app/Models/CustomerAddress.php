<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    use HasFactory;
    protected $table = 'customer_addresses';
    protected $fillable = ['id', 'customer_id', 'name', 'address', 'postal_code_id', 'colony', 'city', 'references'];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function postal_code()
    {
        return $this->belongsTo('App\Models\PostalCode');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
