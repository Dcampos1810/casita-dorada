<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';
    protected $fillable = ['id', 'customer_address_id', 'shipping_id', 'payment_id', 'subtotal', 'total', 'status'];

    public function shipping()
    {
        return $this->belongsTo('App\Models\Shipping');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\Payment');
    }

    public function order_details()
    {
        return $this->belongsTo('App\Models\OrderDetail');
    }

    public function customer_address()
    {
        return $this->hasMany('App\Models\CustomerAddress');
    }
}
