<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartshopItem extends Model
{
    use HasFactory;
    protected $table = 'cartshop_items';
    protected $fillable = ['id', 'cartshop_id', 'product_content_id', 'quantity', 'price', 'total', 'comments'];

    public function cartshop()
    {
        return $this->belongsTo('App\Models\Cartshop');
    }

    public function product_content()
    {
        return $this->belongsTo('App\Models\ProductContent');
    }
}
