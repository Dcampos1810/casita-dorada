<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $table = 'payments';
    protected $fillable = ['id', 'name', 'slug', 'image', 'status'];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
