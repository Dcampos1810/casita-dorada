<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Customer extends Authenticatable {

    use HasFactory;
    protected $table = 'customers';
    protected $fillable = ['id', 'name', 'phone', 'password'];

    protected $hidden = [
        'password'
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }


    public function addresses()
    {
        return $this->hasMany('App\Models\CustomerAddress');
    }

    public function cartshop()
    {
        return $this->hasOne('App\Models\Cartshop');
    }
}
