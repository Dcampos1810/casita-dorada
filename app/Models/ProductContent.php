<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductContent extends Model
{
    use HasFactory;
    protected $table = 'product_contents';
    protected $fillable = ['id', 'product_id', 'type', 'price', 'status'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function cartshop_items()
    {
        return $this->hasMany('App\Models\CartshopItem');
    }

    public function order_details()
    {
        return $this->hasMany('App\Models\OrderDetail');
    }
}
