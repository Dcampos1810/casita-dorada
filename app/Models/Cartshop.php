<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cartshop extends Model
{
    use HasFactory;
    protected $table = 'cartshops';
    protected $fillable = ['id', 'customer_id'];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function cartshop_items()
    {
        return $this->hasMany('App\Models\CartshopItem');
    }
}
