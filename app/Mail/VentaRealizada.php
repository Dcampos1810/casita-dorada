<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VentaRealizada extends Mailable
{
    use Queueable, SerializesModels;
    public $pdf;
    public $pedido;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf, $pedido)
    {
        $this->pdf = $pdf;
        $this->pedido = $pedido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("ventas@casitadorada.com", "Venta Pedido #".$this->pedido['order_id'])
        ->view('layouts.emails.order')
        ->subject($this->pedido['title'])
        ->attachData($this->pdf, 'pedido #'.$this->pedido['order_id'].'.pdf', ['mime' =>
            'application/pdf']);
    }
}
