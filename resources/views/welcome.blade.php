@extends('layouts.frontend.main')

@section('custom-css')

@endsection

@section('content')
    @include('frontend.tienda.todos')
    @include('frontend.tienda.cheviches')
    @include('frontend.tienda.aguachiles')
    @include('frontend.tienda.cocteles')
    @include('frontend.tienda.especialidades')
    @include('frontend.tienda.hits')
    @include('frontend.tienda.empanizados')
    @include('frontend.tienda.pastas')
    @include('layouts.frontend.modalAddProduct')
    @include('layouts.frontend.modalViewProduct')
    @include('layouts.frontend.modalMayorEdad')

    <input type="hidden" value="@if(auth()->check()) {{ auth()->user()->id }} @endif" id="customer_id" name="customer_id">

@endsection

@section('custom-js')
    <script type="text/javascript">

        var AuthUser = {{ auth()->check() ? 'true' : 'false' }};
        if(AuthUser != false){
            var customer_id = {{ auth()->check() ? auth()->user()->id : '0' }};

        }

        function getCountItemsCart(customer){
            var counter = document.getElementById("cartItems");
            $.ajax({
                type: "POST",
                url: '{{ route('getCountItemsCart') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    customer_id: customer
                },
                success: function(response){
                    if(response > 0){
                        counter.innerHTML = response;
                        document.getElementById("counter").style.display = "block"
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        }

        function openModal(product_id){
            $.ajax({
                type: "POST",
                url: "{{ route('getProduct') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: product_id
                },
                success: function(response) {
                    $('#modalAddProduct').modal('show');
                    $("#imagen").attr("src", "https://casitadorada.com/"+response.image);
                    document.getElementById("title").innerHTML = response.name;
                    document.getElementById("description").innerHTML = response.description;
                    document.getElementById("label_subtotal").innerHTML = response.content["price"];
                    $("#price").val(response.content["price"]);
                    $("#quantity").val(1);
                    $("#product_content_id").val(response.content["product_content_id"]);

                },
                error: function() {
                    alert("Error de Backend");
                }
            });
        }

        function addToCart(){
            var quantity = document.getElementById("quantity").value;
            var customer = customer_id;
            var product_content_id = document.getElementById("product_content_id").value;
            var total = parseInt(document.getElementById("label_subtotal").innerHTML);
            var price = document.getElementById("price").value;
            var comments = document.getElementById("comments").value;

            $.ajax({
                type: "POST",
                url: "{{ route('addToCart') }}",
                data:{
                    "_token": "{{ csrf_token() }}",
                    product_content_id: product_content_id,
                    quantity: quantity,
                    customer: customer_id,
                    price: price,
                    total: total,
                    comments: comments
                },
                success: function(response){
                    $("#comments").val("");
                    $(function() {
                        Swal.fire({
                            position: 'center-middle',
                            showConfirmButton: false,
                            timer: 1500,
                            icon: 'success',
                            title: response.message
                        });

                    });
                    $("#modalAddProduct").modal("hide");
                    getCountItemsCart(customer)
                },
                error: function(response) {
                    $(function() {
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        Toast.fire({
                            icon: 'error',
                            title: response.message
                        })
                    });
                }
            })
        }


        function multiplicar(cantidad){
            var total = 0;
            var cantidad = parseInt(cantidad);
            var price = document.getElementById("price").value;
            total = (cantidad * price);
            total = (total == null || total == undefined || isNaN(total) || total <= 0 ) ? price : total;
            document.getElementById("label_subtotal").innerHTML = total;
        }

        function sumar(){
            var quantity = document.getElementById("quantity").value;
            quantity++;
            document.getElementById("quantity").value = quantity;
            multiplicar(quantity);
            buttons();
        }
        function restar(){
            var quantity = document.getElementById("quantity").value;
            if(quantity != 1){
                quantity--;
            }
            document.getElementById("quantity").value = quantity;
            multiplicar(quantity);
            buttons();
        }

        function buttons(){
            var quantity = document.getElementById("quantity").value;
            var btn_restar = document.getElementById("btn_restar");

            if(quantity == 1){
                btn_restar.classList.add("disabled")
            }
            else{
                btn_restar.classList.remove("disabled");
            }
        }

        function openLoginModal(){
            $("#modalRegisterForm").modal("hide");
            $("#modalLoginForm").modal("show");
        }

        function openRegisterModal(){
            $("#modalRegisterForm").modal("show");
            $("#modalLoginForm").modal("hide");
        }

        $("#btn_add_cart").on('click', function() {
            if(!AuthUser){
                $("#modalAddProduct").modal("hide");
                $("#modalCP").modal("show");
            }
            else{
                addToCart();
            }
        });

        $(document).ready(function() {

            if(!AuthUser){
                $("#modalCP").modal("show");
            }
            else{
                getCountItemsCart(customer_id);
                getMyOrders(customer_id);
            }

            buttons();
        });

        $("#formRegister").on('submit', function(e) {
            var telefono = document.getElementById("phone_register").value;
            $.ajax({
                type: "POST",
                url: '{{ route('verifyCustomer') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    phone: telefono
                },
                beforeSend: function(){
                    e.preventDefault();
                    $('#btn-register').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Validando...').addClass('disabled');
                },
                success: function(response){
                    if(response.success == false){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        });
                    }
                    else{
                        document.getElementById("formRegister").submit();
                    }
                },
                error: function(response){
                    console.log("Error en el Backend");
                }
            }).done(function (response){
                if(response.success == false)
                $('#btn-register').find('.spin').remove();
                $("#btn-register").html('Regístrate').removeClass('disabled');
                $("#btn-register").addClass('enabled')
            });
        })

        $("#btn-login").on('click', function(e) {
            var telefono = document.getElementById("phone").value;
            var password = document.getElementById("password").value;
            $.ajax({
                type: "POST",
                url: '{{ route("customer.login") }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    phone: telefono,
                    password: password
                },
                beforeSend: function(){
                    e.preventDefault();
                    $('#btn-login').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Validando...').addClass('disabled');
                },
                success: function(response){
                    if(response.success == false){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            })
                        });
                        $('#btn-login').find('.spin').remove();
                        $("#btn-login").html('Iniciar Sesión').removeClass('disabled');
                        $("#btn-login").addClass('enabled')
                    }
                    else{
                        window.location.href = '{{ route("welcome") }}'
                    }

                },
                error: function(response){
                    console.log("Error en el Backend");
                }
            });
        })

        function getMyOrders(customer){
            $.ajax({
                type: "POST",
                url: '{{ route('getMyOrders') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    customer_id: customer
                },
                success: function(response){
                    $.each(response, function(i){
                        var row = ''
                        row += '<tr id="'+response[i]["id"]+'">'
                            row += '<td style="width: 10%" align="center" class="align-middle">'
                                row += ''+response[i]["id"]+''
                            row += '</td>'
                            row += '<td style="width: 30%" class="align-middle">'
                                row += ''+response[i]["status"]+''
                            row += '</td>'
                            row += '<td style="width: 60% class="text-center" class="align-middle">'
                                row += '<div class="d-flex justify-content-center">'
                                    row += '<button class="btn btn-md btn-danger" type="button" onclick="downloadPDF('+response[i]["id"]+')" id="btnDownloadPDF['+response[i]["id"]+']">'
                                        row += '<i class="far fa-file-pdf mr-2" aria-hidden="true"></i>'
                                            row += 'Descargar'
                                    row += '</button>'
                                row += '</div>'
                            row += '</td>'
                        row += '</tr>'
                        $("#tableOrders>tbody").append(row);
                    })
                },
                error: function(response){
                    console.log("Error de Backend");
                }
            })
        }



        function downloadPDF(order_id){
            $.ajax({
                url: '{{ route('downloadPDF') }}',
                method: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "order": order_id
                },
                xhrFields: {
                    responseType: 'blob'
                },
                beforeSend: function(){
                    $('#btnDownloadPDF['+order_id+']').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Descargando...').addClass('disabled');
                },
                success: function (data) {
                    var blob = new Blob([data]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Ticket de Compra #"+order_id+".pdf";
                    link.click();
                    $('#btnDownloadPDF['+order_id+']').find('.spin').remove();
                    $("#btnDownloadPDF["+order_id+"]").html('<i class="far fa-file-pdf mr-3" aria-hidden="true"></i>Descargar').removeClass('disabled');
                    $("#btnDownloadPDF["+order_id+"]").addClass('enabled')
                    // document.body.removeChild(link);
                },error: function(xhr, ajaxOptions, thrownError){
                    console.log(xhr.status+" ,"+" "+ajaxOptions+", "+thrownError);
                }
            }).done(function (response){

            });
        }

    </script>
@endsection

