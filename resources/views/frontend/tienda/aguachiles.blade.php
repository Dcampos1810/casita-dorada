<div style="margin-top: 20px" class="tab-pane" id="aguachiles" role="tabpanel" aria-labelledby="aguachiles-tab">
    <div class="container-fluid">
        <h2>Aguachiles</h2>
        <hr />
        <div class="container-fluid">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light" style="margin-bottom: 50px;">
                @foreach ($aguachiles as $aguachile)
                    <div class=" col-12 col-sm-4 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                    <div class="card h-100">

                        <!-- Card image -->
                        <img class="card-img-top" src="{{ $aguachile->image }}" alt="Card image cap" height="300" width="300">

                        <!-- Card content -->
                        <div class="card-body">

                        <!-- Title -->
                        <h4 class="card-title mb-5 "><a>{{ $aguachile->name }}</a></h4>
                        <!-- Text -->
                            <p class="h5">$ {{ $aguachile->price }}</p>
                        <button class="btn btn-primary btn-bg" onclick="openModal({{ $aguachile->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>
                        </div>

                    </div>
                    <!-- Card -->
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
