<div style="margin-top: 20px" class="tab-pane" id="hits-casita" role="tabpanel" aria-labelledby="hits-casita-tab">
    <div class="container-fluid">
        <h2>Hits de la Casita</h2>
        <hr />
        <div class="container-fluid py-2">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light">
                @foreach ($hits as $hit)
                    <div class=" col-12 col-sm-4 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                        <div class="card h-100">
                            <!-- Card image -->
                            <img class="card-img-top" src="{{ $hit->image }}" alt="Card image cap" height="300" width="300">
                            <!-- Card content -->
                            <div class="card-body">
                            <!-- Title -->
                                <h4 class="card-title mb-5"><a>{{ $hit->name }}</a></h4>
                                <!-- Text -->
                                <p class="h5">$ {{ $hit->price }}</p>
                                <button class="btn btn-primary btn-bg" onclick="openModal({{ $hit->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>
                            </div>
                        </div>
                    <!-- Card -->
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
