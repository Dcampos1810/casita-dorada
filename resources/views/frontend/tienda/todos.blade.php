<div style="margin-top: 20px" class="tab-pane active" id="todos" role="tabpanel" aria-labelledby="todos-tab">
    <div class="container-fluid">
        <h2>Todos los Productos</h2>
        <hr />
        <div class="container-fluid">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light bg-light">
                @foreach ($all as $item)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                        <div class="card h-100">

                            <!-- Card image -->
                            <img class="card-img-top" src="{{ $item->image }}" alt="Card image cap" height="300" width="300">

                            <!-- Card content -->
                            <div class="card-body text-center">

                                <!-- Title -->
                                <h5 class="card-title mb-5"><a>{{ $item->name }}</a></h5>
                                <!-- Text -->
                                <p class="h5">$ {{ $item->price }}</p>
                                <button class="btn btn-primary btn-bg" onclick="openModal({{ $item->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>

                            </div>

                        </div>
                        <!-- Card -->
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

