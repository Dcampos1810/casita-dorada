<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('backend/img/logos/logo_casita.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/logos/logo_casita.png') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('frontend/css/mdb.min.css') }}" rel="stylesheet">

    @yield('meta')
    <style>

        div.scrollmenu {
            overflow: auto;
            white-space: nowrap;
        }

        div.scrollmenu a {
            display: inline-block;
            color: black;
            text-align: center;
            padding: 14px;
            text-decoration: none;
        }

        div.scrollmenu a:hover {
            background-color: #777;
        }

    </style>
</head>
<body>
    <main>
        <header>
            @include('layouts.frontend.navbar-menu')
            @include('layouts.frontend.carousel')
        </header>
        <div class="container-fluid" style="margin-bottom: 100px">
            <div class="row">
                <div class="col-sm-12 col-md-2">
                    {{-- Horizontal --}}
                    <div class="row">
                        <div class="col-12">
                            <div class="col-12 text-center d-sm-block d-md-none">
                                <h2 style="margin-bottom: 10px; margin-top: 10px"><b>Menú</b></h2>
                                <i class="fas fa-arrows-alt-h"></i>
                                <p class="small">Deslize para ver todo el menú</p>
                            </div>
                            <div class="nav navbar-fixed-top flex-column nav-pills d-md-none d-sm-none d-block d-sm-block scrollmenu text-center sticky-content" aria-orientation="vertical" role="tablist" id="pills-tab">
                                <a class="nav-link active" href="#todos" id="todos-tab" data-toggle="tab" role="tab" aria-controls="todos" aria-selected="true">Todos</a>
                                @foreach ($subcate as $menu)
                                    <a class="nav-link" href="#{{ $menu->slug }}" id="{{ $menu->slug }}-tab" data-toggle="tab" role="tab" aria-controls="{{ $menu->slug }}" aria-selected="true">{{ $menu->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                        {{-- Vertical --}}
                    <ul class="nav nav-pills d-md-block d-xl-block d-lg-block d-sm-none d-none" id="pills-tab" role="tablist" style="margin-top: 50px;">
                        <h2 style="margin-left: 15px; margin-bottom: 10px"><b>Menú</b></h2>
                        <li class="nav-item active" role="presentation">
                            <a class="nav-link" id="todos-tab" data-toggle="tab" href="#todos" role="tab" aria-controls="todos" aria-selected="true">Todos los Productos</a>
                        </li>
                        @foreach ($subcate as $menu)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="{{ $menu->slug }}-tab" data-toggle="tab" href="#{{ $menu->slug }}" role="tab" aria-controls="{{ $menu->slug }}" aria-selected="false">{{ $menu->name }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-12 col-md-10">
                   <div class="tab-content text-center">
                    @include('frontend.menu.todos')
                    @include('frontend.menu.entradas')
                    @include('frontend.menu.tacos-tostadas')
                    @include('frontend.menu.cheviches')
                    @include('frontend.menu.aguachiles')
                    @include('frontend.menu.cocteles')
                    @include('frontend.menu.especialidades')
                    @include('frontend.menu.hits')
                    @include('frontend.menu.empanizados')
                    @include('frontend.menu.pastas')
                    @include('frontend.menu.bebidas')
                    @include('frontend.menu.cervezas')
                    @include('frontend.menu.micheladas')
                    @include('frontend.menu.cocteleria')
                    @include('frontend.menu.licores')
                    @include('layouts.frontend.modalViewProduct')
                   </div>
                </div>
            </div>
        </div>
    @include('layouts.frontend.footer')
    </main>


    {{-- SCRIPTS --}}
    <script type="text/javascript" src="{{ asset('frontend/js/jquery-3.4.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/js/mdb.min.js') }}"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>
    <script type="text/javascript" src="{{ asset('frontend/js/sticky.js') }}"></script>
    <!-- Initializations -->
    <script type="text/javascript">
        function openModal(product_id){
            $.ajax({
                type: "POST",
                url: "{{ route('getProduct') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: product_id
                },
                success: function(response) {
                    $("#imagen-view").attr("src", "https://casitadorada.com/"+response.image);
                    document.getElementById("title-view").innerHTML = response.name;
                    document.getElementById("description-view").innerHTML = response.description;
                    $('#modalViewProduct').modal('show');

                },
                error: function() {
                    alert("Error de Backend");
                }
            });
        }

    </script>

    {{-- Custom JS --}}
    @yield('custom-js')



</body>
</html>
