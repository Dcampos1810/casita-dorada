@extends('layouts.frontend.cartshop')

@section('content')
    <div class="container" style="margin-top: 100px;" id="content">
        <div class="row" style="display: none" id="cart">
            @if(!$cartshop_items->isEmpty())
                {{-- Adicionales --}}
                <div class="text-center mb-5">
                    <div class="col-12 col-sm-12 col-md-12" id="adicionales">
                        <div class="accordion row" id="accordion-antojos">
                            <div class="card z-depth-0 col-md-6" >
                                <div class="card-header" id="headingAntojos" style="color: white">
                                    <button class="btn btn-link btn-block" type="button" data-toggle="collapse" data-target="#collapseAntojos" aria-expanded="true" aria-controls="collapseAntojos">
                                        <h6 class="text-black" class="mr-auto ml-auto"><b>¿Un antojo?</b> &nbsp;&nbsp;<i class="fas fa-angle-down rotate-icon text-black"></i></h6>
                                    </button>
                                </div>
                                <div id="collapseAntojos" class="collapse hide" aria-labelledby="headingAntojos" data-parent="#accordion-antojos">
                                    <div class="card-body">
                                        <div class="d-flex flex-row flex-nowrap overflow-auto bg-light bg-light">
                                            @foreach ($antojos as $antojo)
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-5 col-xl-6">
                                                    <!-- Card -->
                                                    <div class="card h-100">

                                                        <!-- Card image -->
                                                        <img class="card-img-top" src="{{ $antojo->image }}" alt="Card image cap" height="300" width="300">

                                                        <!-- Card content -->
                                                        <div class="card-body">

                                                            <!-- Title -->
                                                            <h5 class="card-title mb-5"><a>{{ $antojo->name }}</a></h5>
                                                            <!-- Text -->
                                                            <p class="h5">Precio: $ {{ $antojo->price }}</p>
                                                            <button class="btn btn-primary btn-bg" onclick="openModal({{ $antojo->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>

                                                        </div>

                                                    </div>
                                                    <!-- Card -->
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Sección de Bebidas --}}
                            <div class="card z-depth-0 col-md-6" >
                                <div class="card-header" id="headingBebidas" style="color: white">
                                    <button class="btn btn-link btn-block" type="button" data-toggle="collapse" data-target="#collapseBebidas" aria-expanded="true" aria-controls="collapseAntojos">
                                        <h6 class="text-black" class="mr-auto ml-auto"><b>¿Una bebida?</b> &nbsp;&nbsp;<i class="fas fa-angle-down rotate-icon text-black"></i></h6>
                                    </button>
                                </div>
                                <div id="collapseBebidas" class="collapse hide" aria-labelledby="headingBebidas" data-parent="#accordion-antojos">
                                    <div class="card-body">
                                        <div class="d-flex flex-row flex-nowrap overflow-auto bg-light bg-light">
                                            @foreach ($bebidas as $bebida)
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-5 col-xl-6">
                                                    <!-- Card -->
                                                    <div class="card h-100">

                                                        <!-- Card image -->
                                                        <img class="card-img-top" src="{{ $bebida->image }}" alt="Card image cap" height="300" width="300">

                                                        <!-- Card content -->
                                                        <div class="card-body">

                                                            <!-- Title -->
                                                            <h5 class="card-title mb-5"><a>{{ $bebida->name }}</a></h5>
                                                            <!-- Text -->
                                                            <p class="h5">Precio: $ {{ $bebida->price }}</p>
                                                            <button class="btn btn-primary btn-bg" onclick="openModal({{ $bebida->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>

                                                        </div>

                                                    </div>
                                                    <!-- Card -->
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                {{-- Tabla de Items --}}
                <table class="table table-borderless" style="width: 100%" id="itemsTable">
                    <thead class="info-color-dark">
                        <tr>
                            <th class="text-left"><h6 class="text-white">Imagen</h6></th>
                            <th class="text-left"><h6 class="text-white">Producto</h6></th>
                            <th class="text-right"><h6 class="text-white">Total</h6></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cartshop_items as $item)
                            <tr id="{{ $item->id }}">
                                <td style="width: 25%">
                                    <img src="../{{ $item->image }}" alt="item" width="100" height="100" class="rounded"/>
                                </td>
                                <td style="width: 45%">
                                    <div class="row">
                                        <div class="col-12 ml-auto mr-auto">
                                            <p class="h6">{{ $item->name }}</p>
                                            <div class="d-flex justify-content-start mb-1">
                                                <button type="button" class="btn-sm btn-danger btn-rounded" onclick="restar({{ $item->id }})" id="btn_restar[{{ $item->id }}]" style="width: 30px; height: 30px;"><i class="fas fa-minus"></i></button>
                                                <input type="number" onkeyup="multiplicar(this.value, {{ $item->id }});" class="form-control" id="quantity[{{ $item->id }}]"
                                                    name="quantity[{{ $item->id }}]" value="{{ $item->quantity }}" min="1"
                                                    style="width: 35%; text-align: center; margin-top: -5px; border: 0px solid; outline:none;">
                                                <button type="button" class="btn-sm btn-primary btn-rounded" onclick="sumar({{ $item->id }})" style="width: 30px; height: 30px;"><i class="fas fa-plus"></i></button>
                                            </div>
                                            <p><b>Precio: ${{ $item->price }} C/U</b></p>
                                            @if($item->comments != "")
                                                <p class="small"><b>Comentarios:</b> {{ $item->comments }}</p>
                                            @endif
                                            <span class="item" style="display: none">{{ $item->type }}</span>
                                            <input class="btn btn-danger btn-sm" type="button" value="Eliminar" onclick="deleteItemCart({{ $item->id }})"/>
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 35%" class="justify-content-end" align="right">
                                    <input type="hidden" id="price[{{ $item->id }}]" name="price[{{ $item->id }}]" value="{{ $item->price }}">
                                    <span class="h6 total-producto " onchange="subtotal();" id="label_subtotal[{{ $item->id }}]">$ {{ $item->total }}</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot id="tfoot-items">
                        <tr>
                            <td style="width: 30%"></td>
                            <td align="right" style="width: 45%">
                                <p class="h6">Subtotal: </p>
                            </td>
                            <td align="right" style="width: 35%" colspan="1">
                                <span class="h6" id="subtotal" name="subtotal">$ </span>
                            </td>
                        </tr>
                        @if(!is_null($address))
                            <tr id="row-envio">
                                <input type="hidden" id="postal_code_id" name="postal_code_id" value="{{ $address->postal_code_id }}">
                                <td style="width: 30%"></td>
                                <td align="right" style="width: 45%">
                                    <p class="h6">Costo de envío: </p>
                                </td>
                                <td align="right" style="width: 35%" colspan="1">
                                    <span class="h6" id="envio" name="envio">Calculando...</span>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                        @else
                            <input type="hidden" id="postal_code_id" name="postal_code_id" value="0">
                        @endif
                    </tfoot>
                </table>

                {{-- Total --}}
                <table class="table table-borderless" style="width: 100%; margin-top: -15px">
                    <tbody>
                        <tr>
                            <td style="width: 30%"></td>
                            <td align="right" style="width: 45%">
                                <p class="h6">Total: </p>
                            </td>
                            <td align="right" style="width: 35%" colspan="1">
                                <span class="h6" id="total" name="total">Calculando...</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>

                {{-- Dirección --}}
                <div class="container" style="margin-bottom: 50px">
                    <h5 class="text-center mb-5"><b>Mi Domicilio</b></h5>
                    @if(!is_null($address))
                        <div class="container">
                            <div class="card">
                                <div class="card-horizontal" style="display: flex; flex: 1 1 auto;">
                                    <div class="card-body">
                                        <h4 class="card-title" id="profile_name_address">{{ $address->name }}</h4>
                                        <p class="card-text" id="profile_address">{{ $address->address }} Col. {{ $address->colony }} C.P. {{ $address->postal_code }} {{ $address->city }}</p>
                                        <p class="card-text" id="profile_references">{{ $address->references }}</p>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="address_id" name="address_id" value="{{ $address->id }}">
                            <button class="btn btn-block btn-info" onclick="modalUpdateAddress({{ $address->id }})">¿Cambiar Domicilio?</button>
                        </div>
                    @else
                        <div class="container" style="display: none" id="div-direccion">
                            <div class="card">
                                <div class="card-horizontal" style="display: flex; flex: 1 1 auto;">
                                    <div class="card-body">
                                        <h4 class="card-title" id="profile_name_address"></h4>
                                        <p class="card-text" id="profile_address"></p>
                                        <p class="card-text" id="profile_references"></p>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="address_id" name="address_id">
                            <button class="btn btn-block btn-info" id="btn-actualizar">¿Cambiar Domicilio?</button>
                        </div>
                        <div id="div-add-address" style="display: block" class="col-md-12 col-lg-12 col-sm-12">
                            <hr>
                            <button type="button" class="btn btn-success btn-bg btn-block" data-toggle="modal" data-target="#modalAddress">Agregar Dirección</button>
                        </div>
                    @endif
                </div>

                {{-- Formas de Pago --}}
                <div class="container" style="margin-bottom: 50px">
                    <h5 class="text-center mb-5"><b>Selecciona una Forma de Pago</b></h5>
                    <table class="table table-borderless" style="width: 100%">
                        <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td style="width: 20%" class="align-middle justify-content-center">
                                        <img src="https://casitadorada.com/{{ $payment->image }}" alt="logo" class="img-fluid" width="64" height="64">
                                    </td>
                                    <td style="width: 70%" class="align-middle justify-content-center">
                                        <p class="h6">{{ $payment->name }}</p>
                                        @if($payment->slug == "transferencia")
                                            <span class="small p">NOTA: Una vez que recibamos su transferencia, procederemos a preparar su pedido.</span>
                                        @elseif($payment->slug == "terminal")
                                            <span class="small p">NOTA: Nuestro repartidor irá con una Terminal Bancaria para su pedido.</span>
                                        @endif
                                    </td>
                                    <td style="width: 10%" class="align-middle" align="center">
                                        <label>
                                            <input type="radio" id="{{ $payment->slug }}" name="payment" value="{{ $payment->slug }}">
                                            <div></div>
                                        </label>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                {{-- Boton de Continuar --}}
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="{{ route('createOrder') }}" method="POST">
                                @csrf
                                <input type="hidden" id="shipping_id" name="shipping_id">
                                <input type="hidden" id="customer_address_id" name="customer_address_id" value="@if(!is_null($address)){{ $address->id }}@else 0 @endif">
                                <input type="hidden" name="checkout_total" id="checkout_total">
                                <input type="hidden" name="checkout_subtotal" id="checkout_subtotal">
                                <input type="hidden" name="payment_id" id="payment_id">
                                <input type="hidden" name="cartshop_items" id="cartshop_items" value="{{ $cartshop_items }}">
                                <button class="btn btn-block btn-md" id="btn_siguiente" disabled type="submit">Espere</button>
                            </form>
                        </div>
                    </div>
                </div>

            @else
                {{-- Carrito Vacio --}}
                <div class="row" style="margin-top: 40px; display: block" id="emptyCart">
                    <div class="col-12 text-center" style="margin-bottom: 20px">
                        <img src="{{ asset('frontend/img/emptyCart.png') }}" alt="emptyCart" class="img-fluid">
                    </div>
                </div>
            @endif
        </div>
        {{-- Carrito Vacío si el cliente elimina 1 por 1 sus artículos --}}
        <div class="row" style="margin-top: 40px; display: none" id="emptyCart">
            <div class="col-12 text-center" style="margin-bottom: 20px">
                <img src="{{ asset('frontend/img/emptyCart.png') }}" alt="emptyCart" class="img-fluid">
            </div>
        </div>
        @include('layouts.frontend.modalAddAdicional')
        @include('layouts.frontend.modalAddress')
    </div>
@endsection

@section('custom-js')
    <script type="text/javascript">

        function maxLengthCheck(object){
            if (object.value.length > object.maxLength)
            object.value = object.value.slice(0, object.maxLength)
        }

        function getCountItemsCart(customer){
            var counter = document.getElementById("cartItems");
            $.ajax({
                type: "POST",
                url: '{{ route('getCountItemsCart') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    customer_id: customer
                },
                success: function(response){
                    if(response > 0){
                        counter.innerHTML = response;
                        document.getElementById("counter").style.display = "block";
                        document.getElementById("cart").style.display = "block";
                        document.getElementById("emptyCart").style.display = "none";
                        subtotal();
                        getShippingPrice();
                        selectPayment()
                    }
                    else{
                        document.getElementById("counter").style.display = "none";
                        document.getElementById("cart").style.display = "none";
                        document.getElementById("emptyCart").style.display = "block";
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        }

        $(document).ready(function() {
            var customer_id = {{ auth()->user()->id }};
            getCountItemsCart(customer_id);
        })

        function subtotal(){
            var sum = 0;
            var subtotal = 0;

            $(".total-producto").each(function() {
                var valueArr = $.trim( $( this ).html() ).split( "$ " );
                sum += parseFloat( valueArr[ 1 ]);
            });
            subtotal = sum;
            document.getElementById('subtotal').innerHTML = "$ "+subtotal;
            document.getElementById("checkout_subtotal").value = subtotal;
        }

        function total(){
            var arrSubtotal = $.trim($("#subtotal").html()).split("$ ");
            var subtotal = parseInt(arrSubtotal[1]);
            if(document.getElementById("postal_code_id").value != 0){
                var arrEnvio = $.trim($("#envio").html()).split("$ ");
                var envio = parseInt(arrEnvio[1]);
            }
            else{
                var envio = 0;
            }
            var total = parseInt(subtotal + envio);
            document.getElementById("total").innerHTML = "$ "+total;
            document.getElementById("checkout_total").value = total;

        }

        function multiplicar(cantidad, item){
            var total = 0;
            var cantidad = parseInt(cantidad);
            var price = document.getElementById("price["+item+"]").value;
            total = (cantidad * price);
            total = (total == null || total == undefined || isNaN(total) || total <= 0 ) ? price : total;
            document.getElementById("label_subtotal["+item+"]").innerHTML = "$ "+total;
            subtotal();
            updateItemCart(item, cantidad, price, total)
        }

        function sumar(item){
            var quantity = document.getElementById("quantity["+item+"]").value;
            quantity++;
            document.getElementById("quantity["+item+"]").value = quantity;
            multiplicar(quantity, item);
            buttons(item);
            total()
        }
        function restar(item){
            var quantity = document.getElementById("quantity["+item+"]").value;
            if(quantity != 1){
                quantity--;
            }
            document.getElementById("quantity["+item+"]").value = quantity;
            multiplicar(quantity, item);
            buttons(item);
            total()
        }

        function buttons(item){
            var quantity = document.getElementById("quantity["+item+"]").value;
            var btn_restar = document.getElementById("btn_restar["+item+"]");

            if(quantity == 1){
                btn_restar.classList.add("disabled")
            }
            else{
                btn_restar.classList.remove("disabled");
            }
        }

        function updateItemCart(item, quantity, price, total){
            $.ajax({
                type: "PUT",
                url: '{{ route('updateItemCart') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: item,
                    quantity: quantity,
                    price: price,
                    total: total
                },
                success: function(){
                    console.log("Item Actualizado Correctamente");
                },
                error: function(){
                    console.log("Error al Actualizar el Item");
                }
            })
        }

        function deleteItemCart(item){
            var customer = {{ auth()->user()->id }}
            Swal.fire({
                title: '¿Eliminar del Carrito?',
                text: "Esta acción no se podrá revertir",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: "Cancelar",
                confirmButtonText: 'Eliminar',
                position: "center-middle"
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: '{{ route('deleteItemCart') }}',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            id: item
                        },
                        success: function() {
                            $('#'+item).remove();
                            subtotal();
                            getCountItemsCart(customer);
                            total()
                        },
                        error: function() {
                            $(function() {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000
                                });
                                Toast.fire({
                                    icon: 'error',
                                    title: "Ha ocurrido un error al eliminar el producto de tu carrito"
                                })
                            });
                        }
                    })
                    $(function() {
                        Swal.fire({
                            position: 'center-middle',
                            showConfirmButton: false,
                            timer: 2500,
                            icon: 'success',
                            title: "Producto Eliminado de Tu Carrito"
                        });
                    });
                }
            })

        }

        function addToCart(){
            var quantity = document.getElementById("modal_quantity").value;
            var customer = {{ auth()->user()->id }};
            var product_content_id = document.getElementById("modal_product_content_id").value;
            var total = parseInt(document.getElementById("modal_label_subtotal").innerHTML);
            var price = document.getElementById("modal_price").value;
            var comments = document.getElementById("modal_comments").value;
            var name = document.getElementById("modal_title").innerHTML;
            var image = $("#imagen").attr("src");


            $.ajax({
                type: "POST",
                url: "{{ route('addToCart') }}",
                data:{
                    "_token": "{{ csrf_token() }}",
                    product_content_id: product_content_id,
                    quantity: quantity,
                    customer: customer,
                    price: price,
                    total: total,
                    comments: comments,
                    name: name,
                    image: image
                },
                success: function(response){
                    var row = '';
                    row += '<tr id="'+response.item.id+'">'
                        row += '<td style="width: 25%">'
                        row +=      '<img src="'+response.image+'" alt="item" width="100" height="100" class="rounded"/>'
                        row += '</td>'
                        row += '<td style="width: 45%">'
                        row +=      '<div class="row">'
                        row +=          '<div class="col-12 ml-auto mr-auto">'
                        row +=              '<p class="h6">'+response.title+'</p>'
                        row +=              '<div class="d-flex justify-content-start mb-1">'
                        row +=                  '<button type="button" class="btn-sm btn-danger btn-rounded" onclick="restar\('+response.item.id+'\)" id="btn_restar\['+response.item.id+'\]" style="width: 30px; height: 30px;"><i class="fas fa-minus"></i></button>'
                        row +=                  '<input type="number" onkeyup="multiplicar\(this.value, '+response.item.id+'\);" class="form-control" id="quantity\['+response.item.id+'\]"'
                        row +=                    'name="quantity\['+response.item.id+'\]" value="'+response.item.quantity+'" min="1"'
                        row +=                    'style="width: 35%; text-align: center; margin-top: -5px; border: 0px solid; outline:none;">'
                        row +=                  '<button type="button" class="btn-sm btn-primary btn-rounded" onclick="sumar('+response.item.id+')" style="width: 30px; height: 30px;"><i class="fas fa-plus"></i></button>'
                        row +=              '</div>'
                        row +=              '<p class="small">Precio: $ '+response.item.price+' C/U</p>'
                        if(response.item.comments != null){
                            row +=           ' <p class="small"><b>Comentarios:</b>'+response.item.comments+'</p>'
                        }
                        row +=              '<input class="btn btn-danger btn-sm" type="button" value="Eliminar" onclick="deleteItemCart\('+response.item.id+'\)"/>'
                        row +=           '</div>'
                        row +=      '</div>'
                        row +=  '</td>'
                        row +=  '<td style="width: 35%" class="justify-content-end" align="right">'
                        row +=      '<input type="hidden" id="price\['+response.item.id+'\]" name="price\['+response.item.id+'\]" value="'+response.item.price+'">'
                        row +=      '<span class="h6 total-producto " onchange="subtotal\(\);" id="label_subtotal\['+response.item.id+'\]">$ '+response.item.total+'</span>'
                                '</td>'
                    row +=  '</tr>'

                    $("#itemsTable>tbody").append(row);
                    $("#adicionales .hide").slideUp();

                    $(function() {
                        Swal.fire({
                            position: 'center-middle',
                            showConfirmButton: false,
                            timer: 1500,
                            icon: 'success',
                            title: response.message
                        });

                    });
                    $("#modal_comments").val("");
                    $("#modalAddAdicional").modal("hide");
                    getCountItemsCart(customer);
                    updateCart(customer);
                },
                error: function(response) {
                    $(function() {
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        Toast.fire({
                            icon: 'error',
                            title: response.message
                        })
                    });
                    $("#modalAddAdicional").modal("hide");
                }
            })
        }

        function openModal(product_id){
            $.ajax({
                type: "POST",
                url: "{{ route('getAdicional') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: product_id
                },
                success: function(response) {
                    // console.log(response);
                    $('#modalAddAdicional').modal('show');
                    $("#imagen").attr("src", "https://casitadorada.com/"+response.image);
                    document.getElementById("modal_title").innerHTML = response.name;
                    document.getElementById("description").innerHTML = response.description;
                    document.getElementById("modal_label_subtotal").innerHTML = response.content["price"];
                    $("#modal_price").val(response.content["price"]);
                    $("#modal_quantity").val(1);
                    $("#modal_product_content_id").val(response.content["product_content_id"]);

                    $("#btn_add_cart").on('click', function(e) {
                        addToCart()
                    });
                },
                error: function() {
                    alert("Error de Backend");
                }
            });
        }

        function multiply(cantidad){
            var total = 0;
            var cantidad = parseInt(cantidad);
            var price = document.getElementById("modal_price").value;
            total = (cantidad * price);
            total = (total == null || total == undefined || isNaN(total) || total <= 0 ) ? price : total;
            document.getElementById("modal_label_subtotal").innerHTML = total;
        }

        function add(){
            var quantity = document.getElementById("modal_quantity").value;
            quantity++;
            document.getElementById("modal_quantity").value = quantity;
            multiply(quantity);
        }

        function subs(){
            var quantity = document.getElementById("modal_quantity").value;
            if(quantity != 1){
                quantity--;
            }
            document.getElementById("modal_quantity").value = quantity;
            multiply(quantity);
        }

        function counterITems(){
            var counter = 0;

            $(".item").each(function() {
                var tipo = $(this).html();
                if(tipo === "PLATILLO" || tipo === "GRANDE"){
                    counter++;
                }
            });
            var items = counter;

            return items

        }

        function selectPayment(){
            var counter = counterITems();
            var direccion = document.getElementById("address_id").value;
            var button = document.getElementById("btn_siguiente");
            var transferencia = document.getElementById("transferencia");
            var efectivo=  document.getElementById("efectivo");
            efectivo.checked = false;
            transferencia.checked = false;
            console.log(counter);

            if(counter >= 1){
                if(direccion > 0){
                    button.classList.remove('btn-warning');
                    button.classList.remove('btn-success');
                    button.classList.add('btn-info');
                    button.disabled = true;
                    button.innerText = "Seleccione una Forma de Pago";


                    $("input:radio[name=payment]").click(function() {
                    var sku = $(this).val();
                        if(sku == "efectivo"){
                            document.getElementById("transferencia").checked = false;
                            button.classList.remove('btn-info');
                            button.classList.add('btn-success');
                            button.disabled = false;
                            button.innerText = "Procesar Pedido";
                            document.getElementById("payment_id").value = 1;


                        }
                        else if(sku == "transferencia"){
                            document.getElementById("efectivo").checked = false;
                            button.classList.remove('btn-info');
                            button.classList.add('btn-success');
                            button.disabled = false;
                            button.innerText = "Procesar Pedido";
                            document.getElementById("payment_id").value = 2;

                        }

                    });

                }
                else{
                    button.classList.remove('btn-warning');
                    button.classList.remove('btn-success');
                    button.classList.add('btn-danger');
                    button.disabled = true;
                    button.innerText = "Ingrese su Dirección";
                }

            }
            else {
                efectivo.disabled = true;
                transferencia.disabled = true;
                button.classList.add('btn-warning');
                button.classList.remove('btn-success');
                button.classList.remove('btn-danger');
                button.disabled = true;
                button.innerText = "El Mínimo de pedido es de 1 Platillo";
            }
        }

        $("#btn-modal-address").on('click', function() {
            var method = document.getElementById("methodAddress").value;
            if(method == "add"){
                saveAddress();
            }
            else {
                updateAddress();
            }
        })

        function saveAddress(){
            var name = document.getElementById("name_address").value;
            var postal_code_id = document.getElementById("code").value;
            var city = document.getElementById("city").value;
            var address = document.getElementById("direccion").value;
            var colony = document.getElementById("colony").value;
            var references = document.getElementById("references").value;
            var customer_id = {{ auth()->user()->id }};

            if(name === "" || postal_code_id == 0 || city=== ""  || address=== "" || colony === "" || references=== ""  || customer_id == 0){
                $(function() {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    Toast.fire({
                        icon: 'error',
                        title: "Debe llenar todos los datos"
                    })
                });
            }
            else{
                $.ajax({
                    type: "POST",
                    url: '{{ route('saveAddress') }}',
                    data:{
                        "_token": "{{ csrf_token() }}",
                        customer_id: customer_id,
                        name: name,
                        postal_code_id: postal_code_id,
                        city: city,
                        direccion: address,
                        colony: colony,
                        references: references
                    },
                    beforeSend: function(){
                        $('#btn-modal-address').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Agregando...').addClass('disabled');
                    },
                    success: function(response){
                        document.getElementById("profile_name_address").innerHTML = response.address.name;
                        document.getElementById("profile_address").innerHTML = response.address.address+ ' Col. '+ response.address.colony+ ' C.P. '+response.postal_code.code+ ' Ciudad: '+response.address.city;
                        document.getElementById("profile_references").innerHTML = response.address.references;
                        document.getElementById("div-direccion").style.display = "block";
                        document.getElementById("div-add-address").style.display = "none";
                        document.getElementById("address_id").value = response.address.id;
                        $("#btn-actualizar").on('click', function() {
                            modalUpdateAddress(response.address.id);
                        });
                        counterITems()
                        document.getElementById("postal_code_id").value = response.address.postal_code_id;
                        getShippingPrice();
                        var row = '';
                        row +=  '<tr id="row-envio">'
                        row +=      '<td style="width: 30%"></td>'
                        row +=      '<td align="right" style="width: 45%">'
                        row +=          '<p class="h6">Costo de envío: </p>'
                        row +=      '</td>'
                        row +=      '<td align="right" style="width: 35%" colspan="1">'
                        row +=          '<span class="h6" id="envio" name="envio">Calculando...</span>'
                        row +=      '</td>'
                        row +=  '</tr>'
                        $("#itemsTable>tfoot").append(row);
                        document.getElementById("customer_address_id").value = response.address.id;


                        $(function() {
                            Swal.fire({
                                position: 'center-middle',
                                showConfirmButton: false,
                                timer: 1500,
                                icon: 'success',
                                title: response.message
                            });

                        });
                        $("#modalAddress").modal("hide");
                    },
                    error: function(){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: "Error al Guardar la Dirección"
                            })
                        });
                    }
                }).done(function(response){
                    $('#btn-modal-address').find('.spin').remove();
                    $("#btn-modal-address").html('Añadir Dirección').removeClass('disabled');
                    $("#btn-modal-address").addClass('enabled')
                });
            }


        }

        function modalUpdateAddress(address_id){
            $("#methodAddress").val("update");
            document.getElementById("title-address").innerHTML = "Actualizar Dirección";
            $.ajax({
                type: "GET",
                url: '{{ route('getAddress') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    address_id: address_id
                },
                success: function(response){
                    document.getElementById("name_address").value = response.name;
                    document.getElementById("postal_code").value = response.postal_code["code"]
                    document.getElementById("code").value = response.postal_code_id;
                    document.getElementById("city").value = response.city;
                    getColonies(response.postal_code["code"]);
                    document.getElementById("colony").value = response.colony;
                    document.getElementById("direccion").value = response.address;
                    document.getElementById("references").value = response.references;
                    $("#modalAddress").modal("show");
                    document.getElementById("btn-modal-address").innerText = "Actualizar Dirección";
                    document.getElementById("address_id").value = response.id
                },
                error: function(response){
                    console.log("Error de Backend");
                }
            })

        }

        function updateAddress(){
            var name = document.getElementById("name_address").value;
            var postal_code_id = document.getElementById("code").value;
            var city = document.getElementById("city").value;
            var address = document.getElementById("direccion").value;
            var colony = document.getElementById("colony").value;
            var references = document.getElementById("references").value;
            var customer_id = {{ auth()->user()->id }};
            var id = document.getElementById("address_id").value;

            $.ajax({
                type: "PUT",
                url: '{{ route('updateAddress') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    customer_id: customer_id,
                    name_address: name,
                    postal_code_id: postal_code_id,
                    city: city,
                    direccion: address,
                    colony: colony,
                    references: references,
                    id: id
                },
                beforeSend: function(){
                    $('#btn-modal-address').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Actualizando...').addClass('disabled');
                },
                success: function(response){
                    document.getElementById("profile_name_address").innerHTML = response.address.name;
                    document.getElementById("profile_address").innerHTML = response.address.address+ ' Col. '+ response.address.colony+ ' C.P. '+response.postal_code.code+ ' Ciudad: '+response.address.city;
                    document.getElementById("profile_references").innerHTML = response.address.references;
                    document.getElementById("postal_code_id").value = response.address.postal_code_id;
                    document.getElementById("customer_address_id").value = response.address.id;
                    getShippingPrice();
                    $(function() {
                        Swal.fire({
                            position: 'center-middle',
                            showConfirmButton: false,
                            timer: 1500,
                            icon: 'success',
                            title: response.message
                        });

                    });
                    $("#modalAddress").modal("hide");
                },
                error: function(){
                    $(function() {
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        Toast.fire({
                            icon: 'error',
                            title: "Error al Guardar la Diracción"
                        })
                    });
                }
            }).done(function(response){
                $('#btn-modal-address').find('.spin').remove();
                $("#btn-modal-address").html('Añadir Dirección').removeClass('disabled');
                $("#btn-modal-address").addClass('enabled')
            });
        }

        function validateCP(cp){
            if(cp.length === 5){
                $.ajax({
                    type: "GET",
                    url:"https://api-sepomex.hckdrk.mx/query/search_cp/"+cp+"?limit=1",
                    beforeSend: function () {
                        $('#btnGetCP').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Validando...').addClass('disabled');
                    },
                    success: function (data) {
                        validarCobertura(data["response"]["cp"]["0"]);
                    },
                    error: function(data, ajaxOptions, thrownError){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: 'Código Postal Inválido'
                            })
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                        var colonies = $("#colony").empty();
                        var div_data='<option value="" disabled selected>Ingrese su Código Postal</option>';
                        $(div_data).appendTo(colonies);
                        $("#city").val("");
                    }

                });
            }
            else{
                $(function() {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    Toast.fire({
                        icon: 'error',
                        title: 'El Código Postal debe contar con 5 números'
                    })
                });
            }
        }

        function validarCobertura(code){
            $.ajax({
                type: "GET",
                url: '{{ route('validarCobertura') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    code: code
                },
                success: function(response){
                    if(response.success == true){
                        document.getElementById("code").value = response.code
                        getColonies(code);
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: response.message
                            });
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                        $("#btn-modal-address").removeClass('disabled');
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            });
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                        $("#btn-modal-address").addClass("disabled");
                        var colonies = $("#colony").empty();
                        var div_data='<option value="" disabled selected>Ingrese su Código Postal</option>';
                        $(div_data).appendTo(colonies);
                        $("#city").val("");

                    }
                },
                error: function(){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    Toast.fire({
                        icon: 'error',
                        title: "Tenemos inconvenientes encontrando su Código Postal"
                    });
                    var colonies = $("#colony").empty();
                        var div_data='<option value="" disabled selected>Ingrese su Código Postal</option>';
                        $(div_data).appendTo(colonies);
                        $("#city").val("");
                }
            })
        }

        function getColonies(cp){
            var colonies = $("#colony").empty();

            $.ajax({
                type: "GET",
                url:"https://api-sepomex.hckdrk.mx/query/info_cp/"+cp,
                dataType: "json",
                success: function (data) {
                    $.each(data, function(i,obj){
                        var colony = data[i]["response"]["asentamiento"];
                        var div_data="<option value='"+colony+"'>"+colony+"</option>";
                        $(div_data).appendTo(colonies);
                    });
                    $("#city").val(data[0]["response"]["municipio"]);

                },
                error: function(data, ajaxOptions, thrownError){
                    $(function() {
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        Toast.fire({
                            icon: 'error',
                            title: "Código Postal Inválido"
                        })
                    });
                }

            });
        }

        function getShippingPrice(){
            var postal_code_id = document.getElementById("postal_code_id").value;
            if(postal_code_id != 0){
                $.ajax({
                    type: "POST",
                    url: '{{ route('getShippingPrice') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        postal_code_id: postal_code_id
                    },
                    success: function(response){
                        document.getElementById("envio").innerHTML = "$ "+response[0].price;
                        document.getElementById("shipping_id").value = response[0].shipping_id;
                        total()
                    },
                    error: function(response){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: "Tenemos problemas calculando el precio de envio a tu Código Postal"
                            })
                        });
                    }
                })
            }
            else{
                total()
            }

        }

        function updateCart(customer_id){
            $.ajax({
                type: "POST",
                url: '{{ route('updateCart') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    customer_id: customer_id
                },
                success: function(response){
                    document.getElementById("cartshop_items").value = response;
                },
                error: function(response){
                    console.log("Error en el backend");
                }
            })
        }

     $("#btn_siguiente").on("click", function() {
        $('#btn_siguiente').html('<span class="spinner-border spinner-border-sm mr-3 spin" role="status" aria-hidden="true"></span>Creando tu pedido...').addClass('disabled');
     });


    </script>
@endsection
