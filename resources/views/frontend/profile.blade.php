@extends('layouts.frontend.profile')

@section('title', 'Mi Cuenta')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <style>
        .card.card-cascade .view.gradient-card-header {
                padding: 1.1rem 1rem;
            }

            .card.card-cascade .view {
                box-shadow: 0 5px 12px 0 rgba(34, 13, 223, 0.664), 0 2px 8px 0 rgba(8, 21, 216, 0.678);
            }
        </style>

@endsection

@section('content')
    <main style="margin-top: 100px">
        <div class="container">
            <section class="section">
                <div class="row">
                    <div class="col-lg-8 mb-4">
                        <div class="card card-cascade" style="padding: -20px">
                            <div class="view view-cascade gradient-card-header mdb-color" style="background-color: rgba(8, 22, 216, 0.945); height: 40px;">
                                <h5 class="mb-0 font-weight-bold text-center text-white mt-2">Editar Mi Cuenta</h5>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-form form-group" style="margin-left: 20px; margin-right: 20px">
                                            <label for="name" data-error="wrong" data-success="right">Nombre</label>
                                            <input type="text" id="name" name="name" class="form-control validate" value="{{ $customer->name }}" required>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form form-group" style="margin-left: 20px; margin-right: 20px">
                                            <input type="number" id="phone" name="phone" class="form-control validate" value="{{ $customer->phone }}" required>
                                            <label for="name" data-error="wrong" data-success="right">Teléfono</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-info btn-rounded btn-block" value="Actualizar Datos" onclick="updateProfile({{ $customer->id }})">Actualizar Datos</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 mb-4">
                        <div class="card card-cascade">
                            <div class="view view-cascade gradient-card-header mdb-color" style="background-color: rgba(8, 22, 216, 0.945); height: 40px;">
                                <h5 class="mb-0 font-weight-bold text-center text-white mt-2">Cambiar Contraseña</h5>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="md-form form-group" style="margin-left: 20px; margin-right: 20px">
                                                <label for="name" data-error="wrong" data-success="right">Nueva Contraseña</label>
                                                <input type="password" id="password" name="password" class="form-control validate" placeholder="*******" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="md-form form-group" style="margin-left: 20px; margin-right: 20px">
                                                <label for="password_confirmation" data-error="wrong" data-success="right">Repetir Nueva Contraseña</label>
                                                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control validate" placeholder="******" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-info btn-rounded btn-block" value="Actualizar Contraseña" onclick="updatePassword({{ $customer->id }})">Actualizar Contraseña</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
    <main>
        <div class="container">
            <div class="card card-cascade">
                <div class="view view-cascade gradient-card-header mdb-color" style="background-color: rgba(8, 22, 216, 0.945); height: 40px;">
                    <h5 class="mb-0 font-weight-bold text-center text-white mt-2">Mi Dirección</h5>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <section class="section">
                        <div class="row">
                            @if(!is_null($address))
                                <div class="col-md-12 col-lg-12 col-sm-12" style="margin-bottom: 20px" id="div-direccion">
                                    <div class="card card-body">
                                        <h4 class="card-title" id="profile_name_address">{{ $address->name }}</h4>
                                        <p class="card-text" id="profile_address">{{ $address->address }}</p>
                                        <p class="card-text" id="profile_colony">Colonia: {{ $address->colony }}</p><p class="card-text" id="profile_postal_code">C.P. {{ $address->postal_code }}</p>
                                        <p class="card-text" id="profile_city">Ciudad: {{ $address->city }}</p>
                                        <p class="card-text" id="profile_references">Referencias: {{ $address->references }}</p>
                                        <input type="hidden" name="postal_code_id" id="postal_code_id" value="{{ $address->postal_code_id }}">

                                        <div class="col-md-6 mb-2">
                                            <button type="button" class="btn btn-info btn-block" onclick="modalUpdateAddress({{ $address->id }})" id="btn-actualizar">Actualizar</a>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-danger btn-block" type="button" onclick="confirmDelete({{ $address->id }})" id="btn-eliminar">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                                <div id="div-add-address" style="display: none" class="col-md-12 col-lg-12 col-sm-12">
                                    <hr>
                                    <button type="button" class="btn btn-success btn-bg btn-block" data-toggle="modal" data-target="#modalAddress">Agregar Dirección</button>
                                </div>
                            @else
                                <div class="col-md-12 col-lg-12 col-sm-12" style="margin-bottom: 20px; display: none" id="div-direccion">
                                    <div class="card card-body">
                                        <h4 class="card-title" id="profile_name_address"></h4>
                                        <p class="card-text" id="profile_address"></p>
                                        <p class="card-text" id="profile_colony"></p><p class="card-text" id="profile_postal_code">C.P. </p>
                                        <p class="card-text" id="profile_city"></p>
                                        <p class="card-text" id="profile_references"></p>
                                        <input type="hidden" name="postal_code_id" id="postal_code_id" value="">

                                        <div class="col-md-6 mb-2">
                                            <button type="button" class="btn btn-info btn-block" id="btn-actualizar">Actualizar</a>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-danger btn-block" type="button" id="btn-eliminar">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                                <div id="div-add-address" style="display: block" class="col-md-12 col-lg-12 col-sm-12">
                                    <hr>
                                    <button type="button" class="btn btn-success btn-bg btn-block" data-toggle="modal" data-target="#modalAddress">Agregar Dirección</button>
                                </div>
                            @endif
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    @include('layouts.frontend.modalAddress')



@endsection

@section('custom-js')
    <script type="text/javascript">

        function maxLengthCheck(object){
            if (object.value.length > object.maxLength)
            object.value = object.value.slice(0, object.maxLength)
        }

        function validateCP(cp){
            if(cp.length === 5){
                $.ajax({
                    type: "GET",
                    url:"https://api-sepomex.hckdrk.mx/query/search_cp/"+cp+"?limit=1",
                    beforeSend: function () {
                        $('#btnGetCP').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Validando...').addClass('disabled');
                    },
                    success: function (data) {
                        validarCobertura(data["response"]["cp"]["0"]);
                    },
                    error: function(data, ajaxOptions, thrownError){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: 'Código Postal Inválido'
                            })
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                        var colonies = $("#colony").empty();
                        var div_data='<option value="" disabled selected>Ingrese su Código Postal</option>';
                        $(div_data).appendTo(colonies);
                        $("#city").val("");
                    }

                });
            }
            else{
                $(function() {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    Toast.fire({
                        icon: 'error',
                        title: 'El Código Postal debe contar con 5 números'
                    })
                });
            }
        }

        function validarCobertura(code){
            $.ajax({
                type: "GET",
                url: '{{ route('validarCobertura') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    code: code
                },
                success: function(response){
                    if(response.success == true){
                        document.getElementById("code").value = response.code
                        getColonies(code);
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: response.message
                            });
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                        $("#btn-modal-address").removeClass('disabled');
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            });
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                        $("#btn-modal-address").addClass("disabled");
                        var colonies = $("#colony").empty();
                        var div_data='<option value="" disabled selected>Ingrese su Código Postal</option>';
                        $(div_data).appendTo(colonies);
                        $("#city").val("");

                    }
                },
                error: function(){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    Toast.fire({
                        icon: 'error',
                        title: "Tenemos inconvenientes encontrando su Código Postal"
                    });
                    var colonies = $("#colony").empty();
                        var div_data='<option value="" disabled selected>Ingrese su Código Postal</option>';
                        $(div_data).appendTo(colonies);
                        $("#city").val("");
                }
            })
        }

        function getColonies(cp){
            var colonies = $("#colony").empty();

            $.ajax({
                type: "GET",
                url:"https://api-sepomex.hckdrk.mx/query/info_cp/"+cp,
                dataType: "json",
                success: function (data) {
                    $.each(data, function(i,obj){
                        var colony = data[i]["response"]["asentamiento"];
                        var div_data="<option value='"+colony+"'>"+colony+"</option>";
                        $(div_data).appendTo(colonies);
                    });
                    $("#city").val(data[0]["response"]["municipio"]);

                },
                error: function(data, ajaxOptions, thrownError){
                    $(function() {
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        Toast.fire({
                            icon: 'error',
                            title: "Código Postal Inválido"
                        })
                    });
                }

            });
        }

        $("#btn-modal-address").on('click', function() {
            var method = document.getElementById("methodAddress").value;
            if(method === "add"){
                saveAddress();
            }
            else {
                updateAddress();
            }
        })

        function saveAddress(){
            var name = document.getElementById("name_address").value;
            var postal_code_id = document.getElementById("code").value;
            var city = document.getElementById("city").value;
            var address = document.getElementById("direccion").value;
            var colony = document.getElementById("colony").value;
            var references = document.getElementById("references").value;
            var customer_id = {{ auth()->user()->id }};

            if(name === "" || postal_code_id == 0 || city=== ""  || address=== "" || colony === "" || references=== ""  || customer_id == 0){
                $(function() {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    Toast.fire({
                        icon: 'error',
                        title: "Debe llenar todos los datos"
                    })
                });
                return false;
            }
            else{
                $.ajax({
                    type: "POST",
                    url: '{{ route('saveAddress') }}',
                    data:{
                        "_token": "{{ csrf_token() }}",
                        customer_id: customer_id,
                        name: name,
                        postal_code_id: postal_code_id,
                        city: city,
                        direccion: address,
                        colony: colony,
                        references: references
                    },
                    beforeSend: function(){
                        $('#btn-modal-address').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Agregando...').addClass('disabled');
                    },
                    success: function(response){
                       if(response.success == true){
                            document.getElementById("profile_name_address").innerHTML = response.address.name;
                            document.getElementById("profile_address").innerHTML = response.address.address;
                            document.getElementById("profile_postal_code").innerHTML = "C.P. "+response.postal_code.code;
                            document.getElementById("profile_city").innerHTML = "Ciudad: "+response.address.city;
                            document.getElementById("profile_colony").innerHTML = "Colonia: "+response.address.colony;
                            document.getElementById("profile_references").innerHTML = "Referencias: "+response.address.references;
                            document.getElementById("div-direccion").style.display = "block";
                            document.getElementById("div-add-address").style.display = "none";
                            $("#btn-actualizar").on('click', function() {
                                modalUpdateAddress(response.address.id);
                            });
                            $("#btn-eliminar").on('click', function() {
                                confirmDelete(response.address.id);
                            });

                            $(function() {
                                Swal.fire({
                                    position: 'center-middle',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    icon: 'success',
                                    title: response.message
                                });

                            });
                            $("#modalAddress").modal("hide");
                       }
                       else{
                            $(function() {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000
                                });
                                Toast.fire({
                                    icon: 'error',
                                    title: response.message
                                })
                            });
                       }
                    },
                    error: function(){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: "Error al Guardar la Dirección"
                            })
                        });
                    }
                }).done(function(response){
                    $('#btn-modal-address').find('.spin').remove();
                    $("#btn-modal-address").html('Añadir Dirección').removeClass('disabled');
                    $("#btn-modal-address").addClass('enabled')
                });
            }


        }

        function modalUpdateAddress(address_id){
            document.getElementById("methodAddress").value = "update";
            document.getElementById("title-address").innerHTML = "Actualizar Dirección";
            $.ajax({
                type: "GET",
                url: '{{ route('getAddress') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    address_id: address_id
                },
                success: function(response){
                    document.getElementById("name_address").value = response.name;
                    document.getElementById("postal_code").value = response.postal_code["code"]
                    document.getElementById("code").value = response.postal_code_id;
                    document.getElementById("city").value = response.city;
                    getColonies(response.postal_code["code"]);
                    document.getElementById("colony").value = response.colony;
                    document.getElementById("direccion").value = response.address;
                    document.getElementById("references").value = response.references;
                    $("#modalAddress").modal("show");
                    document.getElementById("btn-modal-address").innerText = "Actualizar Dirección";
                    document.getElementById("address_id").value = response.id
                },
                error: function(response){
                    console.log("Error de Backend");
                }
            })

        }

        function updateAddress(){
            var name = document.getElementById("name_address").value;
            var postal_code_id = document.getElementById("code").value;
            var city = document.getElementById("city").value;
            var address = document.getElementById("direccion").value;
            var colony = document.getElementById("colony").value;
            var references = document.getElementById("references").value;
            var customer_id = {{ auth()->user()->id }};
            var id = document.getElementById("address_id").value;

            $.ajax({
                type: "PUT",
                url: '{{ route('updateAddress') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    customer_id: customer_id,
                    name_address: name,
                    postal_code_id: postal_code_id,
                    city: city,
                    direccion: address,
                    colony: colony,
                    references: references,
                    id: id
                },
                beforeSend: function(){
                    $('#btn-modal-address').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Actualizando...').addClass('disabled');
                },
                success: function(response){
                    document.getElementById("profile_name_address").innerHTML = response.address.name;
                    document.getElementById("profile_address").innerHTML = response.address.address;
                    document.getElementById("profile_postal_code").innerHTML = "C.P. "+response.postal_code.code;
                    document.getElementById("profile_city").innerHTML = "Ciudad: "+response.address.city;
                    document.getElementById("profile_colony").innerHTML = "Colonia: "+response.address.colony;
                    document.getElementById("profile_references").innerHTML = "Referencias: "+response.address.references;

                    $(function() {
                        Swal.fire({
                            position: 'center-middle',
                            showConfirmButton: false,
                            timer: 1500,
                            icon: 'success',
                            title: response.message
                        });

                    });
                    $("#modalAddress").modal("hide");
                },
                error: function(){
                    $(function() {
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        Toast.fire({
                            icon: 'error',
                            title: "Error al Guardar la Diracción"
                        })
                    });
                }
            }).done(function(response){
                $('#btn-modal-address').find('.spin').remove();
                $("#btn-modal-address").html('Añadir Dirección').removeClass('disabled');
                $("#btn-modal-address").addClass('enabled')
            });
        }

        function confirmDelete(address_id){
            Swal.fire({
                title: '¿Eliminar Dirección?',
                text: "Esta acción no se podrá revertir",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: "Cancelar",
                confirmButtonText: 'Eliminar',
                position: "center-middle"
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: '{{ route('deleteAddress') }}',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            address_id: address_id
                        },
                        success: function() {
                            document.getElementById("div-direccion").style.display = "none";
                            document.getElementById("div-add-address").style.display = "block"
                        },
                        error: function() {
                            $(function() {
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 3000
                                });
                                Toast.fire({
                                    icon: 'error',
                                    title: "Ha ocurrido un error al eliminar tu domicilio"
                                })
                            });
                        }
                    })
                    $(function() {
                        Swal.fire({
                            position: 'center-middle',
                            showConfirmButton: false,
                            timer: 2500,
                            icon: 'success',
                            title: "Dirección Eliminada"
                        });
                    });
                }
            })
        }



    </script>
@endsection
