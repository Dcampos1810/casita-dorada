<div style="margin-top: 20px" class="tab-pane active" id="todos" role="tabpanel" aria-labelledby="todos-tab">
    <div class="container-fluid">
        <h2>Todos los Productos</h2>
        <hr />
        <div class="container-fluid">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light bg-light">
                @foreach ($all as $item)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                        <div class="card h-100">

                            <!-- Card image -->
                            <img class="card-img-top" src="{{ $item->image }}" alt="Card image cap" height="300" width="300">

                            <!-- Card content -->
                            <div class="card-body text-center">

                                <!-- Title -->
                                <h5 class="card-title mb-5"><a>{{ $item->name }}</a></h5>
                                <!-- Text -->
                                @php
                                    if(!str_contains($item->presentaciones, "PLATILLO") && !str_contains($item->presentaciones, "ENTRADA") && !str_contains($item->presentaciones, "UNIDAD") && !str_contains($item->presentaciones, "BEBIDA")){
                                        $presentaciones = explode(",", $item->presentaciones);
                                        $prices = explode(",", $item->prices);

                                        echo '<p class="h5">'. $presentaciones[0].': $ '.$prices[0].'</p>';
                                        echo '<p class="h5">'. $presentaciones[1].': $ '.$prices[1].'</p>';
                                    }


                                    else{
                                        echo '<p class="h5">Precio: $ '. $item->prices.'</p>';
                                    }

                                @endphp
                                <button class="btn btn-primary btn-bg" onclick="openModal({{ $item->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>

                            </div>

                        </div>
                        <!-- Card -->
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

