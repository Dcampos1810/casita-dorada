<div style="margin-top: 20px" class="tab-pane" id="entradas" role="tabpanel" aria-labelledby="entradas-tab">
    <div class="container-fluid">
        <h2>Entradas</h2>
        <hr />
        <div class="container-fluid">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light bg-light">
                @foreach ($entradas as $entrada)
                    <div class="col-12 col-sm-4 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                        <div class="card h-100">

                            <!-- Card image -->
                            <img class="card-img-top" src="{{ $entrada->image }}" alt="Card image cap" height="300" width="300">

                            <!-- Card content -->
                            <div class="card-body">

                                <!-- Title -->
                                <h5 class="card-title mb-5"><a>{{ $entrada->name }}</a></h5>
                                <!-- Text -->
                                <p class="h5">Precio: $ {{ $entrada->prices }}</p>
                                <button class="btn btn-primary btn-bg" onclick="openModal({{ $entrada->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>

                            </div>

                        </div>
                        <!-- Card -->
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

