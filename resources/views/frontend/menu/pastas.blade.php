<div style="margin-top: 20px" class="tab-pane" id="pastas" role="tabpanel" aria-labelledby="pastas-tab">
    <div class="container-fluid">
        <h2>Pastas</h2>
        <hr />
        <div class="container-fluid py-2">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light">
                @foreach ($pastas as $pasta)
                    <div class=" col-12 col-sm-4 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                    <div class="card h-100">
                        <!-- Card image -->
                        <img class="card-img-top" src="{{ $pasta->image }}" alt="Card image cap" height="300" width="300">
                        <!-- Card content -->
                        <div class="card-body text-center">
                            <!-- Title -->
                            <h5 class="card-title mb-5"><a>{{ $pasta->name }}</a></h5>
                            @php
                                if(!str_contains($pasta->presentaciones, "PLATILLO")){
                                    $presentaciones = explode(",", $pasta->presentaciones);
                                    $prices = explode(",", $pasta->prices);

                                    echo '<p class="h5">'. $presentaciones[0].': $ '.$prices[0].'</p>';
                                    echo '<p class="h5">'. $presentaciones[1].': $ '.$prices[1].'</p>';
                                }


                                else{
                                    echo '<p class="h5">'.$pasta->presentaciones. ': $ '. $pasta->prices.'</p>';
                                }

                            @endphp
                             <button class="btn btn-primary btn-bg" onclick="openModal({{ $pasta->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>
                            <!-- Text -->
                        </div>
                    </div>
                    <!-- Card -->
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
