<div style="margin-top: 20px" class="tab-pane" id="cheviches" role="tabpanel" aria-labelledby="cheviches-tab">
    <div class="container-fluid">
        <h2>Cheviches</h2>
        <hr />
        <div class="container-fluid py-2">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light">
                @foreach ($cheviches as $cheviche)
                    <div class=" col-12 col-sm-4 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                        <div class="card h-100">
                            <!-- Card image -->
                            <img class="card-img-top" src="{{ $cheviche->image }}" alt="Card image cap" height="300" width="300">
                            <!-- Card content -->
                            <div class="card-body">
                                <!-- Title -->
                                <h4 class="card-title mb-5"><a>{{ $cheviche->name }}</a></h4>
                                <!-- Text -->
                                @php
                                    if(!str_contains($cheviche->presentaciones, "PLATILLO")){
                                        $presentaciones = explode(",", $cheviche->presentaciones);
                                        $prices = explode(",", $cheviche->prices);

                                        echo '<p class="h5">'. $presentaciones[0].': $ '.$prices[0].'</p>';
                                        echo '<p class="h5">'. $presentaciones[1].': $ '.$prices[1].'</p>';
                                    }


                                    else{
                                        echo '<p class="h5">'.$cheviche->presentaciones. ': $ '. $cheviche->prices.'</p>';
                                    }

                                @endphp
                                <button class="btn btn-primary btn-bg" onclick="openModal({{ $cheviche->id }})">Ver &nbsp;&nbsp;<i class="fas fa-eye"></i></button>
                            </div>

                        </div>
                        <!-- Card -->
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
