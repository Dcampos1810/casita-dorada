<div style="margin-top: 20px" class="tab-pane" id="refrescos" role="tabpanel" aria-labelledby="refrescos-tab">
    <div class="container-fluid">
        <h2>Bebidas</h2>
        <hr />
        <div class="container-fluid">
            <div class="d-flex flex-row flex-nowrap overflow-auto bg-light" style="margin-bottom: 50px;">
                @foreach ($bebidas as $bebida)
                    <div class=" col-12 col-sm-4 col-md-6 col-lg-5 col-xl-3">
                        <!-- Card -->
                    <div class="card h-100">

                        <!-- Card image -->
                        <img class="card-img-top" src="{{ $bebida->image }}" alt="Card image cap" height="300" width="300">

                        <!-- Card content -->
                        <div class="card-body">

                            <!-- Title -->
                            <h4 class="card-title mb-5"><a>{{ $bebida->name }}</a></h4>
                            <!-- Text -->
                            <p class="h5">$ {{ $bebida->price }}</p>

                        </div>

                    </div>
                    <!-- Card -->
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
