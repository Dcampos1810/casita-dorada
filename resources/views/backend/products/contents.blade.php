@extends('layouts.backend.main')

@section('title', 'Contenido de Productos')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/select2/css/select2.min.css') }}">
@endsection

@section('page-title', 'Listado de Contenido de Productos')


@section('content')
    <div class="card">
        @if(session()->has('success'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('success') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @elseif(session()->has('errors'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('errors') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-product-content">Agregar Contenido de Producto</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableProductContents" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Imagen</th>
                        <th>Producto</th>
                        <th>Presentación</th>
                        <th>Precio</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($product_contents as $content)
                        <tr id="{{ $content->id }}">
                            <td>
                                <img src="../{{ $content->image }}" alt="imagen" class="img-fluid" width="96" height="96">
                            </td>
                            <td>{{ $content->product }}</td>
                            <td>{{ $content->type }}</td>
                            <td>$ {{ $content->price }}</td>
                            <td>
                                @if($content->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getProductContent({{ $content->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $content->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th>Imagen</th>
                        <th>Producto</th>
                        <th>Presentación</th>
                        <th>Precio</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
            <!-- /.card-body -->
    </div>
    <!-- /.card -->

    {{-- Modal de Agregar Contenido de Producto --}}
    <div class="modal fade" id="modal-add-product-content">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Contenido de Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Producto</label>
                                <select class="form-control select2" id="modal_product_id" name="modal_product_id" required="required">
                                    <option value="">Seleccione</option>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Tipo de Contenido</label>
                                <select class="custom-select" id="modal_type" name="modal_type" required="required" >
                                    <option value="">Seleccione</option>
                                    <option value="MEDIANO">MEDIANO</option>
                                    <option value="GRANDE">GRANDE</option>
                                    <option value="PLATILLO">PLATILLO</option>
                                    <option value="UNIDAD">UNIDAD</option>
                                    <option value="ENTRADA">ENTRADA</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span><i class="fas fa-dollar-sign"></i></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Precio" id="modal_price" name="modal_price" required onkeypress="validate(event);">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" onclick="saveProductContent()">Guardar</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal de Agregar Contenido de Producto -->

    {{-- Modal de Update Contenido de Producto --}}
    <div class="modal fade" id="modal-update-product-content">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Contenido de Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Producto</label>
                                <select class="form-control select2" id="modal_update_product_id" name="modal_update_product_id" required="required">
                                    <option value="">Seleccione</option>
                                    @foreach($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Tipo de Contenido</label>
                                <select class="form-control select2" id="modal_update_type" name="modal_update_type" required="required" >
                                    <option value="">Seleccione</option>
                                    <option value="MEDIANO">MEDIANO</option>
                                    <option value="GRANDE">GRANDE</option>
                                    <option value="PLATILLO">PLATILLO</option>
                                    <option value="UNIDAD">UNIDAD</option>
                                    <option value="ENTRADA">ENTRADA</option>
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span><i class="fas fa-dollar-sign"></i></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Precio" id="modal_update_price" name="modal_update_price" required onkeypress="validate(event);">
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Status</label>
                                <select class="custom-select" id="modal_update_status" name="modal_update_status">
                                    <option value="A">Activo</option>
                                    <option value="I">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="modal_update_id" id="modal_update_id">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" id="btn_modal_update" name="btn_modal_update">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Actualizr Contenido de Producto -->

    {{-- Modal de Confirmacion Eliminar Contenido de Producto --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Contenido de Producto</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar el Contenido del Producto?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-js')
    {{-- Datatables --}}
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    {{-- jQuery Validation --}}
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/select2/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
          $('#tableProductContents').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
            "order": [[ 1, "asc" ]]
          });
        });


        //Validacion de Solo Numeros en Telefono
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }



        function deleteProductContent(id){
            var productContentsTable = $("#tableProductContents").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.productContents.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    productContentsTable.row("#"+id).remove().draw(false);
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Contenido de Producto Eliminado'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function getProductContent(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.productContents.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    console.log(response);
                    $('#modal-update-product-content').modal('show');
                    $("#modal_update_product_id").val(response.product_id);
                    $("#modal_update_type").val(response.type);
                    $("#modal_update_price").val(response.price)
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#modal_update_id").val(response.id);
                    $("#btn_modal_update").on('click', function() {
                        updateProductContent(id);
                    });
                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deleteProductContent(id);
            });
        }

        function saveProductContent(){
            var product_id = document.getElementById("modal_product_id").value;
            var price = document.getElementById("modal_price").value;
            var type = document.getElementById("modal_type").value;
            var tableProductContents = $("#tableProductContents").DataTable();

            $.ajax({
                type: "POST",
                url: '{{ route('admin.productContents.store') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    modal_product_id: product_id,
                    modal_price: price,
                    modal_type: type
                },
                success: function(response){
                    if(response.success === true){
                        id = response.productContent.id;
                        tableProductContents.row.add([
                        '<img src="../'+response.product.image+'" alt="imagen" class="img-fluid" width="96" height="96">',
                        response.product.name,
                        response.productContent.type,
                        "$ "+response.productContent.price,
                        '<span class="badge bg-danger">Inactivo</span>',
                        '<button class="btn btn-md btn-info px-3" type="button" onClick="getProductContent('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'
                        ]).node().id = id;
                        tableProductContents.draw( false );
                        $("#modal_product_id").val("");
                        $("#modal_type").val("");
                        $("#modal_price").val("");
                        $('#modal-add-product-content').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });

                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                                Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            });
                            console.log("Toast Iniciado");
                        });

                    }

                },
                error: function(response){
                    alert("Error al cargar");
                }
            })
        }

        function updateProductContent(id){
            var tableProductContents = $("#tableProductContents").DataTable();
            var product_id = document.getElementById("modal_update_product_id").value;
            var price = document.getElementById("modal_update_price").value;
            var type = document.getElementById("modal_update_type").value;
            var status = document.getElementById("modal_update_status").value;

            $.ajax({
                type: "PUT",
                url: '{{ route('admin.productContents.update') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    modal_update_id: id,
                    modal_update_product_id: product_id,
                    modal_update_price: price,
                    modal_update_type: type,
                    modal_update_status: status
                },
                success: function(response) {
                    if(response.success == true){
                        var status = response.productContent.status;
                        if(status == "A"){
                            status = '<span class="badge bg-success">Activo</span>';
                        }
                        else{
                            status = '<span class="badge bg-danger">Inactivo</span>';
                        }
                        var actions = '<button class="btn btn-md btn-info px-3" type="button" onClick="getProductContent('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'
                        var image = '<img src="../'+response.product.image+'" alt="imagen" class="img-fluid" width="96" height="96">';
                        var data = [image, response.product.name, response.productContent.type, "$ "+response.productContent.price, status, actions];
                        tableProductContents.row("#"+id).data(data).draw();
                        $('#modal-update-product-content').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }

                },
                error: function() {
                    alert('Error');
                }
            });
        }

      </script>
@endsection
