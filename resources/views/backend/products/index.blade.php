@extends('layouts.backend.main')

@section('title', 'Products')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Productos')

@section('content')
    <div class="card">
        @if(session()->has('success'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('success') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @elseif(session()->has('errors'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('errors') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-product">Agregar Producto</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableProducts" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Categoría</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($products as $product)
                        <tr id="{{ $product->id }}">
                            <td>
                                <img src="../{{ $product->image }}" alt="imagen" class="img-fluid" width="96" height="96">
                            </td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->subcategory }}</td>
                            <td>
                                @if($product->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getProduct({{ $product->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $product->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Categoría</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{-- Modal de Agregar Producto --}}
    <div class="modal fade" id="modal-add-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="container">
                            <div class="input-group mb-3">
                                <div class="form-group col-sm-12 text-center">
                                    <label>Subcategoría</label>
                                    <select class="custom-select" id="modal_subcategory_id" name="modal_subcategory_id" required="required" >
                                        <option value="">Seleccione</option>
                                        @foreach($subcategories as $subcategory)
                                            <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-utensils"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Nombre" id="modal_name" name="modal_name" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-tag"></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Etiqueta" id="modal_slug" name="modal_slug" required>
                            </div>
                            <div class="form-group">
                                <label for="modal_image">Imagen</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="modal_image" name="modal_image">
                                        <label class="custom-file-label" for="modal_image">Seleccione</label>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3 row">
                                <div class="form-group col-sm-12">
                                    <label>Descripción</label>
                                    <textarea class="form-control" rows="3" placeholder="Descripción" id="modal_description" name="modal_description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="saveProduct">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Producto -->

    {{-- Modal de Actualizar Producto --}}
    <div class="modal fade" id="modal-update-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.products.update') }}" method="POST" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <div class="modal-body">
                        <div class="container">
                            <div class="input-group mb-3">
                                <div class="form-group col-sm-12 text-center">
                                    <label>Subcategoría</label>
                                    <select class="custom-select" id="modal_update_subcategory_id" name="modal_update_subcategory_id" required="required" >
                                        <option value="">Seleccione</option>
                                        @foreach($subcategories as $subcategory)
                                            <option value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-utensils"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Nombre" id="modal_update_name" name="modal_update_name" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-tag"></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Etiqueta" id="modal_update_slug" name="modal_update_slug" required>
                            </div>
                            <div class="form-group mb-3 text-center">
                                <label for="image">Imagen</label><br>
                                <img src="" alt="imagen" class="img-fluid" width="128" height="128" id="image" name="image">
                            </div>
                            <div class="form-group">
                                <label for="modal_image">¿Cambiar Imagen?</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="modal_update_image" name="modal_update_image">
                                        <label class="custom-file-label" for="modal_image">Seleccione</label>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3 row">
                                <div class="form-group col-sm-12">
                                    <label>Descripción</label>
                                    <textarea class="form-control" rows="3" placeholder="Descripción" id="modal_update_description" name="modal_update_description"></textarea>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="form-group col-sm-12 text-center">
                                    <label>Status</label>
                                    <select class="custom-select" id="modal_update_status" name="modal_update_status">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" id="modal_update_id" name="modal_update_id">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="updateProduct">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal de Actualizar Producto -->


    {{-- Modal de Confirmacion Eliminar Producto --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Producto</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar el Producto?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-js')
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    {{-- bs-custom-file-input --}}
    <script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });

        $(function () {
            $('#tableProducts').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
                "order": [[ 1, "asc" ]]
            });
        });

        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function getProduct(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.products.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-update-product').modal('show');
                    $("#modal_update_subcategory_id").val(response.subcategory_id)
                    $("#modal_update_name").val(response.name);
                    $("#modal_update_description").val(response.description);
                    $("#modal_update_slug").val(response.slug);
                    $("#modal_update_content").val(response.content);

                    var imagen = "https://casitadorada.com/"+response.image;

                    $("#image").attr("src", imagen);
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#modal_update_id").val(id);

                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deleteProduct(id);
            });
        }

        function deleteProduct(id){
            var productsTable = $("#tableProducts").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.products.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    productsTable.row("#"+id).remove().draw();
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Producto Eliminado'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

    </script>
@endsection
