@extends('layouts.backend.main')

@section('title', 'Costo de Envíos')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Costo de Envíos')


@section('content')
    <div class="card">
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-shipping">Agregar Costo de Envío</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableShippings" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Tiempo</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($shippings as $item)
                        <tr id="{{ $item->id }}">
                            <td>{{ $item->name }}</td>
                            <td>$ {{ $item->price }}</td>
                            <td>{{ $item->time }}</td>
                            <td>
                                @if($item->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getShipping({{ $item->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $item->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Tiempo</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
            <!-- /.card-body -->
    </div>
    <!-- /.card -->

    {{-- Modal de Agregar Costo de Envío --}}
    <div class="modal fade" id="modal-add-shipping">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Costo de Envío</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form role="form" data-toggle="validator" >
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-user"></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Ejem. Zona 1" id="modal_name" name="modal_name" required data-error-msg="Ingrese el Nombre">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-dollar-sign"></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Precio" onkeypress="validate(event);" id="modal_price" name="modal_price" required data-error-msg="Ingrese el Precio">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-clock"></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Tiempo de Envío"  id="modal_time" name="modal_time"  required data-error-msg="Ingrese el Tiempo">

                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="saveShipping()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Costo de Envio -->

    {{-- Modal de Update Costo de Envio --}}
    <div class="modal fade" id="modal-update-shipping">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Costo de Envío</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Nombre" id="modal_update_name" name="modal_update_name" data-error-msg="Ingrese el Nombre">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-dollar-sign"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Precio" onkeypress="validate(event);" id="modal_update_price" name="modal_update_price" data-error-msg="Ingrese el Precio">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-clock"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Tiempo de Envío" id="modal_update_time" name="modal_update_time" data-error-msg="Ingrese el Tiempo">
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Status</label>
                                <select class="custom-select" id="modal_update_status">
                                    <option value="A">Activo</option>
                                    <option value="I">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="modal_update_user_id" id="modal_update_user_id">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn_modal_update" name="btn_modal_update">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Update Costo de Envío -->

    {{-- Modal de Confirmacion Eliminar Usuario --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Costo de Envío</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar el Costo de Envío?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-js')
    {{-- Datatables --}}
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    {{-- jQuery Validation --}}
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
          $('#tableShippings').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
          });
        });


        //Validacion de Solo Numeros en Telefono
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function saveShipping(){
            var name = document.getElementById("modal_name").value;
            var price = document.getElementById("modal_price").value;
            var time = document.getElementById("modal_time").value;
            var shippingsTable = $("#tableShippings").DataTable();

            $.ajax({
                type: "POST",
                url: '{{ route('admin.shippings.store') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    name: name,
                    price: price,
                    time: time
                },
                success: function(response) {
                    if(response.success === true){
                        id = response.shipping.id;
                        shippingsTable.row.add([
                        response.shipping.name,
                        '$ '+response.shipping.price,
                        response.shipping.time,
                        '<span class="badge bg-success">Activo</span>',
                        '<button class="btn btn-md btn-info px-3" type="button" onClick="getShipping('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'
                        ]).node().id = id;
                        shippingsTable.draw( false );
                        $("#modal_name").val("");
                        $("#modal_price").val("");
                        $("#modal_time").val("");
                        $('#modal-add-shipping').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });

                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                                Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            });
                            console.log("Toast Iniciado");
                        });

                    }


                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function deleteShipping(id){
            var shippingsTable = $("#tableShippings").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.shippings.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    shippingsTable.row("#"+id).remove().draw();
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Costo de Envío Eliminado'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function getShipping(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.shippings.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-update-shipping').modal('show');
                    $("#modal_update_name").val(response.name);
                    $("#modal_update_price").val(response.price);
                    $("#modal_update_time").val(response.time);
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#btn_modal_update").on('click', function() {
                        updateShipping(response.id);
                    });

                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function updateShipping(id){
            var shippingsTable = $("#tableShippings").DataTable();
            var name = document.getElementById("modal_update_name").value;
            var price = document.getElementById("modal_update_price").value;
            var time = document.getElementById("modal_update_time").value;
            var status = document.getElementById("modal_update_status").value;

            $.ajax({
                type: "PUT",
                url: '{{ route('admin.shippings.update') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id,
                    name: name,
                    price: price,
                    time: time,
                    status: status
                },
                success: function(response) {
                    if(response.success == true){
                        var status = response.shipping.status;
                        if(status == "A"){
                            status = '<span class="badge bg-success">Activo</span>';
                        }
                        else{
                            status = '<span class="badge bg-danger">Inactivo</span>';
                        }
                        var actions = '<button class="btn btn-md btn-info px-3" type="button" onClick="getShipping('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'

                        var data = [response.shipping.name, "$ "+response.shipping.price, response.shipping.time, status, actions];
                        shippingsTable.row("#"+id).data(data).draw();
                        $('#modal-update-shipping').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }

                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deleteShipping(id);
            });
        }
      </script>
@endsection
