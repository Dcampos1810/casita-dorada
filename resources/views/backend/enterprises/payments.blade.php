@extends('layouts.backend.main')

@section('title', 'Formas de Pago')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Formas de Pago')

@section('content')
    <div class="card">
        @if(session()->has('success'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('success') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @elseif(session()->has('errors'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('errors') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-payment">Agregar Forma de Pago</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tablePayments" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Pago</th>
                        <th>Logo</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($payments as $payment)
                        <tr id="{{ $payment->id }}">
                            <td>{{ $payment->name }}</td>
                            <td>
                                <img src="../{{ $payment->image }}" alt="Logo" class="img-fluid align-middle" width="96" height="96">
                            </td>
                            <td>
                                @if($payment->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getPayment({{ $payment->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $payment->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Logo</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
            <!-- /.card-body -->
    </div>
    <!-- /.card -->

     {{-- Modal de Actualizar Forma de PAgo --}}
     <div class="modal fade" id="modal-update-payment">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Forma de Pago</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.payments.update') }}" method="POST" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <div class="modal-body">
                        <div class="container">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fab fa-cc-visa"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Nombre" id="modal_update_name" name="modal_update_name" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-tag"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Etiqueta" id="modal_update_slug" name="modal_update_slug" required>
                            </div>
                            <div class="form-group mb-3 text-center">
                                <label for="image">Logo</label><br>
                                <img src="" alt="Logo" class="img-fluid" width="128" height="128" id="logo" name="logo" style="background-color: gray">
                            </div>
                            <div class="form-group">
                                <label for="modal_image">¿Cambiar Logo?</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="modal_update_logo" name="modal_update_logo">
                                        <label class="custom-file-label" for="modal_update_logo">Seleccione</label>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="form-group col-sm-12 text-center">
                                    <label>Status</label>
                                    <select class="custom-select" id="modal_update_status" name="modal_update_status">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" id="modal_update_id" name="modal_update_id">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="updatePayment">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal de Actualizar Forma de Pago -->

    {{-- Modal de Agregar Forma de Pago --}}
    <div class="modal fade" id="modal-add-payment">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Forma de Pago</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.payments.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="container">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-building"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Nombre" id="modal_name" name="modal_name" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-tag"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Etiqueta" id="modal_slug" name="modal_slug" required>
                            </div>
                            <div class="form-group">
                                <label for="modal_image">Logo</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="modal_logo" name="modal_logo" required>
                                        <label class="custom-file-label" for="modal_logo">Seleccione</label>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="addPayment">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Forma de Pago -->

    {{-- Modal de Confirmacion Eliminar Empresa --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Forma de Pago</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar la Forma de Pago?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- Modal de Confirmacion Eliminar Empresa --}}
@endsection

@section('custom-js')
    {{-- DataTables --}}
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    {{-- bs-custom-file-input --}}
    <script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    {{-- Custom Functions --}}
    <script type="text/javascript">

        $(document).ready(function () {
            bsCustomFileInput.init();
        });

        $(function () {
            $('#tablePayments').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
            });
        });

        function getPayment(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.payments.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    console.log(response);
                    $('#modal-update-payment').modal('show');
                    $("#modal_update_name").val(response.name);
                    $("#modal_update_slug").val(response.slug)
                    $("#logo").attr("src", "https://casitadorada.com/"+response.image);
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#modal_update_id").val(id);

                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deletePayment(id);
            });
        }

        function deletePayment(id){
            var paymentsTable = $("#tablePayments").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.payments.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    paymentsTable.row("#"+id).remove().draw();
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Forma de Pago Eliminada'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

    </script>
@endsection
