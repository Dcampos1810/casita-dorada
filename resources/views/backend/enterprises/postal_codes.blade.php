@extends('layouts.backend.main')

@section('title', 'Costo de Envíos')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Códigos Postales')


@section('content')
    <div class="card">
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-code">Agregar Código Postal</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableCodes" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Código</th>
                        <th>Zona de Envío</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($postalCodes as $code)
                        <tr id="{{ $code->id }}">
                            <td>{{ $code->code }}</td>
                            <td>{{ $code->zona }}</td>
                            <td>
                                @if($code->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getCode({{ $code->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $code->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th>Código</th>
                        <th>Zona de Envío</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
            <!-- /.card-body -->
    </div>
    <!-- /.card -->



    {{-- Modal de Agregar Codigo Postal --}}
    <div class="modal fade" id="modal-add-code">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Código Postal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Ejem. 99999" id="modal_code" name="modal_code" required maxlength="5">
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Zona de Envío</label>
                                <select class="custom-select" id="modal_shipping_id" name="modal_shipping_id" required="required" >
                                    <option value="">Seleccione</option>
                                    @foreach($shippings as $shipping)
                                        <option value="{{ $shipping->id }}">{{ $shipping->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="saveCode()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Código Postal -->

    {{-- Modal de Update Código Postal --}}
    <div class="modal fade" id="modal-update-code">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Código Postal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Ejem. 99999" id="modal_update_code" name="modal_update_code" required maxlength="5">
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Zona de Envío</label>
                                <select class="custom-select" id="modal_update_shipping_id" name="modal_update_shipping_id" required="required" >
                                    <option value="">Seleccione</option>
                                    @foreach($shippings as $shipping)
                                        <option value="{{ $shipping->id }}">{{ $shipping->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Status</label>
                                <select class="custom-select" id="modal_update_status">
                                    <option value="A">Activo</option>
                                    <option value="I">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="modal_update_code_id" id="modal_update_code_id">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn_modal_update" name="btn_modal_update">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Update Código Postal -->

    {{-- Modal de Confirmacion Eliminar Usuario --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Código Postal</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar el Código Postal?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-js')
    {{-- Datatables --}}
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    {{-- jQuery Validation --}}
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
          $('#tableCodes').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
          });
        });


        //Validacion de Solo Numeros en Telefono
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function saveCode(){
            var code = document.getElementById("modal_code").value;
            var shipping_id = document.getElementById("modal_shipping_id").value;
            var codesTable = $("#tableCodes").DataTable();

            $.ajax({
                type: "POST",
                url: '{{ route('admin.postalCodes.store') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    code: code,
                    shipping_id: shipping_id
                },
                success: function(response) {
                    if(response.success === true){
                        id = response.code.id;
                        codesTable.row.add([
                        response.code.code,
                        response.zona.name,
                        '<span class="badge bg-success">Activo</span>',
                        '<button class="btn btn-md btn-info px-3" type="button" onClick="getCode('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'
                        ]).node().id = id;
                        codesTable.draw( false );
                        $("#modal_code").val("");
                        $("#modal_shiiping_id").val("");
                        $('#modal-add-code').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });

                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                                Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            });
                            console.log("Toast Iniciado");
                        });

                    }


                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function deleteCode(id){
            var codesTable = $("#tableCodes").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.postalCodes.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    codesTable.row("#"+id).remove().draw();
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Código Postal Eliminado'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function getCode(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.postalCodes.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-update-code').modal('show');
                    $("#modal_update_code").val(response.code);
                    $("#modal_update_shipping_id").val(response.shipping_id);
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#btn_modal_update").on('click', function() {
                        updateCode(response.id);
                    });

                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function updateCode(id){
            var codesTable = $("#tableCodes").DataTable();
            var code = document.getElementById("modal_update_code").value;
            var shipping_id = document.getElementById("modal_update_shipping_id").value;
            var status = document.getElementById("modal_update_status").value;

            $.ajax({
                type: "PUT",
                url: '{{ route('admin.postalCodes.update') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id,
                    code: code,
                    shipping_id: shipping_id,
                    status: status
                },
                success: function(response) {
                    if(response.success == true){
                        var status = response.code.status;
                        if(status == "A"){
                            status = '<span class="badge bg-success">Activo</span>';
                        }
                        else{
                            status = '<span class="badge bg-danger">Inactivo</span>';
                        }
                        var actions = '<button class="btn btn-md btn-info px-3" type="button" onClick="getCode('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'

                        var data = [response.code.code, response.zona.name, status, actions];
                        codesTable.row("#"+id).data(data).draw();
                        $('#modal-update-code').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }

                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deleteCode(id);
            });
        }
      </script>
@endsection
