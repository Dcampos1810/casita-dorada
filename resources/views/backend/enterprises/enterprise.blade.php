@extends('layouts.backend.main')

@section('title', 'Empresas')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Empresas')

@section('content')
    <div class="card">
        @if(session()->has('success'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('success') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @elseif(session()->has('errors'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('errors') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-enterprise">Agregar Empresa</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableEnterprises" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Nombre</th>
                        <th>Logo</th>
                        <th>Dirección</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>WhatsApp</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($enterprises as $enterprise)
                        <tr id="{{ $enterprise->id }}">
                            <td>{{ $enterprise->name }}</td>
                            <td>
                                <img src="../{{ $enterprise->logo }}" alt="Logo" class="img-fluid align-middle" width="96" height="96">
                            </td>
                            <td>{{ $enterprise->address }}</td>
                            <td>{{ $enterprise->email }}</td>
                            <td>{{ $enterprise->phone }}</td>
                            <td>{{ $enterprise->whatsapp }}</td>
                            <td>
                                @if($enterprise->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getEnterprise({{ $enterprise->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $enterprise->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Logo</th>
                        <th>Dirección</th>
                        <th>Correo</th>
                        <th>Teléfono</th>
                        <th>WhatsApp</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
            <!-- /.card-body -->
    </div>
    <!-- /.card -->

     {{-- Modal de Actualizar Empresa --}}
     <div class="modal fade" id="modal-update-enterprise">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Empresa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.enterprises.update') }}" method="POST" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <div class="modal-body">
                        <div class="container">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-building"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Nombre" id="modal_update_name" name="modal_update_name" required>
                            </div>
                            <div class="form-group mb-3 text-center">
                                <label for="image">Logo</label><br>
                                <img src="" alt="Logo" class="img-fluid" width="128" height="128" id="logo" name="logo" style="background-color: gray">
                            </div>
                            <div class="form-group">
                                <label for="modal_image">¿Cambiar Logo?</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="modal_update_logo" name="modal_update_logo">
                                        <label class="custom-file-label" for="modal_image">Seleccione</label>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-map"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Dirección" id="modal_update_address" name="modal_update_address" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-envelope"></i></span>
                                    </div>
                                </div>
                                <input type="email" class="form-control datos" placeholder="Correo" id="modal_update_email" name="modal_update_email" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-phone-alt"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="Teléfono" id="modal_update_phone" name="modal_update_phone" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fab fa-whatsapp"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="WhatsApp" id="modal_update_whatsapp" name="modal_update_whatsapp" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fab fa-cc-visa"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="Número de Tarjeta" id="modal_update_card" name="modal_update_card" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-credit-card"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="Cuenta CLABE" id="modal_update_clabe" name="modal_update_clabe" required>
                            </div>
                            <div class="input-group mb-3 row">
                                <div class="form-group col-sm-12">
                                    <label>Descripción</label>
                                    <textarea class="form-control" rows="3" placeholder="Descripción" id="modal_update_description" name="modal_update_description" required></textarea>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="form-group col-sm-12 text-center">
                                    <label>Status</label>
                                    <select class="custom-select" id="modal_update_status" name="modal_update_status">
                                        <option value="A">Activo</option>
                                        <option value="I">Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" id="modal_update_id" name="modal_update_id">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="updateEnterprise">Actualizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Empresa -->

    {{-- Modal de Agregar Empresa --}}
    <div class="modal fade" id="modal-add-enterprise">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Empresa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.enterprises.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="container">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-building"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Nombre" id="modal_name" name="modal_name" required>
                            </div>
                            <div class="form-group">
                                <label for="modal_image">Logo</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="modal_logo" name="modal_logo" required>
                                        <label class="custom-file-label" for="modal_image">Seleccione</label>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-map"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Dirección" id="modal_address" name="modal_address" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-envelope"></i></span>
                                    </div>
                                </div>
                                <input type="email" class="form-control datos" placeholder="Correo" id="modal_email" name="modal_email" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-phone-alt"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="Teléfono" id="modal_phone" name="modal_phone" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fab fa-whatsapp"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="WhatsApp" id="modal_whatsapp" name="modal_whatsapp" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fab fa-cc-visa"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="Número de Tarjeta" id="modal_card" name="modal_card" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span><i class="fas fa-credit-card"></i></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" onkeypress="validate(event);" placeholder="Cuenta CLABE" id="modal_clabe" name="modal_clabe" required>
                            </div>
                            <div class="input-group mb-3 row">
                                <div class="form-group col-sm-12">
                                    <label>Descripción</label>
                                    <textarea class="form-control" rows="3" placeholder="Descripción" id="modal_description" name="modal_description" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="updateEnterprise">Agregar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Empresa -->

    {{-- Modal de Confirmacion Eliminar Empresa --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Empresa</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar la Empresa?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- Modal de Confirmacion Eliminar Empresa --}}
@endsection

@section('custom-js')
    {{-- DataTables --}}
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    {{-- bs-custom-file-input --}}
    <script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    {{-- Custom Functions --}}
    <script type="text/javascript">

        //Validacion de Solo Numeros en Telefono
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        $(document).ready(function () {
            bsCustomFileInput.init();
        });

        $(function () {
            $('#tableEnterprises').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
            });
        });

        function getEnterprise(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.enterprises.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    console.log(response);
                    $('#modal-update-enterprise').modal('show');
                    $("#modal_update_name").val(response.name);
                    $("#logo").attr("src", "https://casitadorada.com/"+response.logo);
                    $("#modal_update_address").val(response.address);
                    $("#modal_update_email").val(response.email);
                    $("#modal_update_phone").val(response.phone);
                    $("#modal_update_whatsapp").val(response.whatsapp);
                    $("#modal_update_description").val(response.description);
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#modal_update_id").val(id);

                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deleteEnterprise(id);
            });
        }

        function deleteEnterprise(id){
            var enterprisesTable = $("#tableEnterprises").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.enterprises.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    enterprisesTable.row("#"+id).remove().draw();
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Empresa Eliminada'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

    </script>
@endsection
