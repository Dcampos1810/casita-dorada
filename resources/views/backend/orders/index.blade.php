@extends('layouts.backend.main')

@section('title', 'Pedidos')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Pedidos')

@section('content')
    <div class="card">
        @if(session()->has('success'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('success') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @elseif(session()->has('errors'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('errors') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableOrders" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Dirección</th>
                        <th>Tipo de Envío</th>
                        <th>Método de Pago</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Fecha Compra</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($orders as $order)
                        <tr id="{{ $order->id }}">
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->customer }}</td>
                            <td class="text-left" style="width: 20%">
                              {{ $order->customer_address }}
                            </td>
                            <td>{{ $order->tipoEnvio }}</td>
                            <td>{{ $order->metodoPago }}</td>
                            <td>$ {{ $order->total }}</td>
                            <td style="width: 10%">
                                @if($order->status == 'recibido')
                                    <span class="badge bg-primary">ORDEN RECIBIDA</span>
                                @elseif($order->status == 'transaccion')
                                    <span class="badge bg-warning">ESPERANDO TRANSACCIÓN</span>
                                @elseif($order->status == "preparacion")
                                    <span class="bagde bg-info">EN PREPARACIÓN</span>
                                @elseif($order->status == "enviando")
                                    <span class="badge bg-secondary">ENVIANDO</span>
                                @elseif($order->status == "cancelado")
                                    <span class="badge bg-danger">CANCELADO</span>
                                @else
                                    <span class="badge bg-success">ENTREGADO AL CLIENTE</span>
                                @endif
                            </td>
                            <td>{{ $order->fechaCompra }}</td>
                            <td>
                                <button class="btn btn-md btn-danger btn-block" type="button" onclick="downloadPDF({{ $order->id }})">
                                    <i class="fas fa-file-pdf"></i>
                                    Descargar
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Dirección</th>
                        <th>Tipo de Envío</th>
                        <th>Método de Pago</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Fecha Compra</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


@endsection

@section('custom-js')
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    {{-- bs-custom-file-input --}}
    <script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
            $('#tableOrders').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
                "order": [[ 0, "desc" ]]
            });
        });

        function downloadPDF(order_id){
            $.ajax({
                url: '{{ route('admin.adminDownloadPDF') }}',
                method: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "order": order_id
                },
                xhrFields: {
                    responseType: 'blob'
                },
                success: function (data) {
                    var blob = new Blob([data]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Ticket de Compra #"+order_id+".pdf";
                    link.click();
                    // document.body.removeChild(link);
                },error: function(xhr, ajaxOptions, thrownError){
                    $('.loading').hide();
                    console.log(xhr.status+" ,"+" "+ajaxOptions+", "+thrownError);
                }
            }).done(function (response){

            });
        }

    </script>
@endsection
