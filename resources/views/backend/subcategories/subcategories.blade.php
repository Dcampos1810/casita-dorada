@extends('layouts.backend.main')

@section('title', 'Subcategorías')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Subcategorías')

@section('content')
    <div class="card">
        @if(session()->has('success'))
            <div class="row text-center">
                <div class="col-sm-3"></div>
                <div class="col-sm-6 text-center">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p class="text-white h5">{{ session()->get('success') }}</p>
                        <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @elseif(session()->has('errors'))
        <div class="row text-center">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 text-center">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <p class="text-white h5">{{ session()->get('errors') }}</p>
                    <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        @endif
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-subcategory">Agregar Subcategoría</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableSubcategories" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Nombre</th>
                        <th>Categoría</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($subs as $subcategory)
                        <tr id="{{ $subcategory->id }}">
                            <td>{{ $subcategory->name }}</td>
                            <td>{{ $subcategory->categoria }}</td>
                            <td>
                                @if($subcategory->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getSubcategory({{ $subcategory->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $subcategory->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th>Nombre</th>
                        <th>Categoría</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{-- Modal de Agregar Subcategoria --}}
    <div class="modal fade" id="modal-add-subcategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Subcategoria</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span><i class="fas fa-utensils"></i></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Nombre" id="modal_name" name="modal_name" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-tag"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Etiqueta" id="modal_slug" name="modal_slug" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Categoría</label>
                                <select class="custom-select" id="modal_category_id" name="modal_category_id" required="required" >
                                    <option value="">Seleccione</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="saveSubcategory()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Subcategoria -->

    {{-- Modal de Actualizar Subcategoria --}}
    <div class="modal fade" id="modal-update-subcategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Categoria</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span><i class="fas fa-utensils"></i></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Nombre" id="modal_update_name" name="modal_update_name" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-tag"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Etiqueta" id="modal_update_slug" name="modal_update_slug" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Categoría</label>
                                <select class="custom-select" id="modal_update_category_id" name="modal_update_category_id" required="required" >
                                    <option value="">Seleccione</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Status</label>
                                <select class="custom-select" id="modal_update_status" name="modal_update_status">
                                    <option value="A">Activo</option>
                                    <option value="I">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="modal_update_id" name="modal_update_id">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn_modal_update">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Actualizar Subcategoria -->


    {{-- Modal de Confirmacion Eliminar Categoria --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Subcategoria</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar la Subcategoria?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-js')
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    {{-- bs-custom-file-input --}}
    <script src="{{ asset('backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });

        $(function () {
            $('#tableSubcategories').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
            });
        });

        function saveSubcategory(){
            var subcategoriesTable = $("#tableSubcategories").DataTable();
            var name = document.getElementById("modal_name").value;
            var slug = document.getElementById("modal_slug").value;
            var category_id = document.getElementById("modal_category_id").value;

            $.ajax({
                type: "POST",
                url: '{{ route('admin.subcategories.store') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    modal_name: name,
                    modal_slug: slug,
                    modal_category_id: category_id
                },
                success: function(response){
                    console.log(response);
                    if(response.success == true){
                        var id = response.subcategory.id;
                        subcategoriesTable.row.add([
                        response.subcategory.name,
                        response.category.name,
                        '<span class="badge bg-danger">Inactivo</span>',
                        '<button class="btn btn-md btn-info px-3" type="button" onClick="getSubcategory('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'
                        ]).node().id = id;
                        subcategoriesTable.draw( false );
                        $("#modal_name").val("");
                        $("#modal_slug").val("");
                        $("#modal_category_id").val("");
                        $('#modal-add-subcategory').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                                Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            });
                            console.log("Toast Iniciado");
                        });
                    }
                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function getSubcategory(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.subcategories.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    console.log(response)
                    $('#modal-update-subcategory').modal('show');
                    $("#modal_update_name").val(response.name);
                    $("#modal_update_slug").val(response.slug);
                    $("#modal_update_category_id").val(response.category_id);
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#modal_update_id").val(id);
                    $("#btn_modal_update").on("click", function() {
                        updateSubcategory(response.id);
                    })

                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function updateSubcategory(id){
            var name = document.getElementById("modal_update_name").value;
            var slug = document.getElementById("modal_update_slug").value;
            var category_id = document.getElementById("modal_update_category_id").value;
            var status = document.getElementById("modal_update_status").value;
            var subcategoriesTable = $("#tableSubcategories").DataTable();
            $.ajax({
                url: '{{ route('admin.subcategories.update') }}',
                type: "PUT",
                data:{
                    "_token": "{{ csrf_token() }}",
                    modal_update_id: id,
                    modal_update_name: name,
                    modal_update_slug: slug,
                    modal_update_category_id: category_id,
                    modal_update_status: status
                },
                success: function(response) {
                    console.log(response);
                    if(response.success == true){
                        var status = response.subcategory.status;
                        if(status == "A"){
                            status = '<span class="badge bg-success">Activo</span>';
                        }
                        else{
                            status = '<span class="badge bg-danger">Inactivo</span>';
                        }
                        var actions = '<button class="btn btn-md btn-info px-3" type="button" onClick="getSubcategory('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'
                        var data = [response.subcategory.name, response.category.name, status, actions];
                        subcategoriesTable.row("#"+id).data(data).draw();
                        $('#modal-update-subcategory').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                                Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            });
                            console.log("Toast Iniciado");
                        });
                    }
                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deleteSubcategory(id);
            });
        }

        function deleteSubcategory(id){
            var subcategoriesTable = $("#tableSubcategories").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.subcategories.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    subcategoriesTable.row("#"+id).remove().draw();
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Subcategoria Eliminada'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

    </script>
@endsection
