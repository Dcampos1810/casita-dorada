@extends('layouts.backend.main')

@section('title', 'Usuarios')

@section('custom-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
@endsection

@section('page-title', 'Listado de Usuarios')


@section('content')
    <div class="card">
        <div class="card-header text-right">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-add-user">Agregar Usuario</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="tableUsers" class="table table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>Nombre Completo</th>
                        <th>Correo</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($users as $user)
                        <tr id="{{ $user->id }}">
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if($user->status == 'A')
                                    <span class="badge bg-success">Activo</span>
                                @else
                                    <span class="badge bg-danger">Inactivo</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-md btn-info px-3" type="button" onClick="getUser({{ $user->id }})">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete({{ $user->id }})">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="text-center">
                    <tr>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Status</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
            <!-- /.card-body -->
    </div>
    <!-- /.card -->

    {{-- Modal de Agregar Usuario --}}
    <div class="modal fade" id="modal-add-user">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Agregar Usuario</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form role="form" data-toggle="validator" >
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-user"></span>
                                    </div>
                                </div>
                                <input type="text" class="form-control datos" placeholder="Nombre y Apellido" id="modal_name" name="modal_name" required data-error-msg="Ingrese el Nombre">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-envelope"></span>
                                    </div>
                                </div>
                                <input type="email" class="form-control datos" placeholder="Correo" id="modal_email" name="modal_email" required data-error-msg="Ingrese el Correo">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                                <input type="password" class="form-control datos" placeholder="Password" id="modal_password" name="modal_password" autocomplete="new-password" required data-error-msg="Ingrese la Contraseña">

                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="saveUser()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Usuario -->

    {{-- Modal de Update Usuario --}}
    <div class="modal fade" id="modal-update-user">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title">Actualizar Usuario</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control datos" placeholder="Nombre y Apellido" id="modal_update_name" name="modal_update_name" data-error-msg="Ingrese el Nombre">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                            <input type="email" class="form-control datos" placeholder="Correo" id="modal_update_email" name="modal_update_email" data-error-msg="Ingrese un correo válido">
                        </div>
                        <div class="input-group mb-3">
                            <div class="form-group col-sm-12 text-center">
                                <label>Status</label>
                                <select class="custom-select" id="modal_update_status">
                                    <option value="A">Activo</option>
                                    <option value="I">Inactivo</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="modal_update_user_id" id="modal_update_user_id">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn_modal_update" name="btn_modal_update">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal de Agregar Usuario -->

    {{-- Modal de Confirmacion Eliminar Usuario --}}
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Eliminar Usuario</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>¿Realmente quiere eliminar al Usuario?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-outline-light" id="btn_confirm_delete">Eliminar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('custom-js')
    {{-- Datatables --}}
    <script src="{{ asset('backend/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    {{-- jQuery Validation --}}
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        $(function () {
          $('#tableUsers').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
          });
        });


        //Validacion de Solo Numeros en Telefono
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
            // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function saveUser(){
            var name = document.getElementById("modal_name").value;
            var email = document.getElementById("modal_email").value;
            var password = document.getElementById("modal_password").value;
            var usersTable = $("#tableUsers").DataTable();

            $.ajax({
                type: "POST",
                url: '{{ route('admin.users.store') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    name: name,
                    email: email,
                    password: password
                },
                success: function(response) {
                    if(response.success === true){
                        id = response.user.id;
                        usersTable.row.add([
                        response.user.name,
                        response.user.email,
                        '<span class="badge bg-danger">Inactivo</span>',
                        '<button class="btn btn-md btn-info px-3" type="button" onClick="getUser('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'
                        ]).node().id = id;
                        usersTable.draw( false );
                        $("#modal_name").val("");
                        $("#modal_email").val("");
                        $('#modal-add-user').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });

                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                                Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            });
                            console.log("Toast Iniciado");
                        });

                    }


                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function deleteUser(id){
            var usersTable = $("#tableUsers").DataTable();
            $.ajax({
                type: "POST",
                url: '{{ route('admin.users.delete') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-danger').modal('hide');
                    usersTable.row("#"+id).remove().draw();
                    $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: 'Usuario Eliminado'
                            })
                            console.log("Toast Iniciado");
                        });

                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function getUser(id){
            $.ajax({
                type: "POST",
                url: '{{ route('admin.users.show') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                success: function(response) {
                    $('#modal-update-user').modal('show');
                    $("#modal_update_name").val(response.name);
                    $("#modal_update_email").val(response.email);
                    if(response.status == 'A'){
                        $("#modal_update_status").val('A');
                    }
                    else{
                        $("#modal_update_status").val('I');
                    }
                    $("#btn_modal_update").on('click', function() {
                        updateUser(response.id);
                    });

                },
                error: function() {
                    alert('Error');
                }
            })
        }

        function updateUser(id){
            var usersTable = $("#tableUsers").DataTable();
            var name = document.getElementById("modal_update_name").value;
            var email = document.getElementById("modal_update_email").value;
            var status = document.getElementById("modal_update_status").value;

            $.ajax({
                type: "PUT",
                url: '{{ route('admin.users.update') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id,
                    name: name,
                    email: email,
                    status: status
                },
                success: function(response) {
                    if(response.success == true){
                        var status = response.user.status;
                        if(status == "A"){
                            status = '<span class="badge bg-success">Activo</span>';
                        }
                        else{
                            status = '<span class="badge bg-danger">Inactivo</span>';
                        }
                        var actions = '<button class="btn btn-md btn-info px-3" type="button" onClick="getUser('+id+')"><i class="fas fa-eye"></i></button> <button class="btn btn-md btn-danger px-3" type="button" onClick="confirmDelete('+id+')"><i class="fas fa-trash" aria-hidden="true"></i></button>'

                        var data = [response.user.name, response.user.email, status, actions];
                        console.log(data);
                        usersTable.row("#"+id).data(data).draw();
                        $('#modal-update-user').modal('hide');
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: '  '+response.message
                            })
                            console.log("Toast Iniciado");
                        });
                    }

                },
                error: function() {
                    alert('Error');
                }
            });
        }

        function confirmDelete(id){
            $('#modal-danger').modal('show');
            $("#btn_confirm_delete").on('click', function() {
                deleteUser(id);
            });
        }
      </script>
@endsection
