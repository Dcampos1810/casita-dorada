<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('backend/img/logos/logo_casita.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/logos/logo_casita.png') }}">

    <title>CCD @yield('title')</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('backend/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @yield('custom-css')
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-primary navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>

            </ul>
            <!-- SEARCH FORM -->
            <form class="form-inline ml-auto">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
                </div>
            </div>
            </form>
            <ul class="navbar-nav ml-auto">
                <div class="dropdown show">
                    <a class="btn btn-transparent dropdown-toggle text-white" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown">
                        {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Ver Perfil</a>
                        <a class="dropdown-item" href="{{ route('admin.logout') }}">Cerrar Sesión</a>
                    </div>
                </div>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="{{ route('admin.dashboard') }}" class="brand-link text-center">
                    <img src="{{ asset('backend/img/logos/logo_nombre.png') }}" alt="CCD Logo" class="img-fluid " width="120" height="40">
                </a>
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                                <li class="nav-item">
                                    <a href="{{ route('admin.dashboard') }}" class="nav-link">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>Dashboard</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.users') }}" class="nav-link">
                                        <i class="nav-icon fas fa-user"></i>
                                        <p>Usuarios</p>
                                    </a>
                                </li>
                                <li class="nav-item has-treewview" style="background-color: rgba(23, 23, 136, 0.801); color:white">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fas fa-building"></i>
                                        <p>
                                            Empresa
                                            <i class="right fas fa-angle-down"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="{{ route('admin.enterprises') }}" class="nav-link">
                                                <i class="nav-icon fas fa-info-circle"></i>
                                                <p class="text-white">Detalles de Empresa</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('admin.shippings') }}" class="nav-link">
                                                <i class="nav-icon fas fa-dollar-sign"></i>
                                                <p class="text-white">Costos de Envíos</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('admin.postalCodes') }}" class="nav-link">
                                                <i class="nav-icon fas fa-map-marked"></i>
                                                <p class="text-white">Códigos Postales</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('admin.payments') }}" class="nav-link">
                                                <i class="nav-icon fab fa-cc-visa"></i>
                                                <p class="text-white">Formas de Pago</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.categories') }}" class="nav-link">
                                        <i class="nav-icon fas fa-boxes"></i>
                                        <p>Categorías</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.subcategories') }}" class="nav-link">
                                        <i class="nav-icon fas fa-sitemap"></i>
                                        <p>Subcategorías</p>
                                    </a>
                                </li>
                                <li class="nav-item has-treewview" style="background-color: blue; color:white">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fas fa-warehouse"></i>
                                        <p>
                                            Inventario
                                            <i class="right fas fa-angle-down"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="{{ route('admin.products') }}" class="nav-link">
                                                <i class="nav-icon fas fa-fish"></i>
                                                <p>Platillos</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('admin.productContents') }}" class="nav-link">
                                                <i class="nav-icon fas fa-info-circle"></i>
                                                <p>Contenido de Platillos</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{ route('admin.bebidas') }}" class="nav-link">
                                                <i class="nav-icon fas fa-beer"></i>
                                                <p>Bebidas</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('admin.orders') }}" class="nav-link">
                                        <i class="nav-icon fas fa-clipboard-list"></i>
                                        <p>Pedidos</p>
                                    </a>
                                </li>
                            </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container">
                        <div class="row mb-2 text-center">
                            <div class="col-sm-12">
                                <h1 class="m-0 text-dark">@yield('page-title')</h1>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                @yield('content')
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
            Versión 1.0.0
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2021 <a href="https://www.casitadorada.com" target="blank">{{ config('app.name') }}</a>.</strong> Todos los Derechos Resevados.
        </footer>
    </div>
    <!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/js/adminlte.min.js') }}"></script>
@yield('custom-js')
</body>
</html>
