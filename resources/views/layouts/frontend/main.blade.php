<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('backend/img/logos/logo_casita.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/logos/logo_casita.png') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('frontend/css/mdb.min.css') }}" rel="stylesheet">
    {{-- Main SCSS --}}
    {{-- <link href="{{ asset('frontend/css/main.scss') }}" rel="stylesheet"> --}}
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">

    @yield('meta')
    <style>

        div.scrollmenu {
            overflow: auto;
            white-space: nowrap;
        }

        div.scrollmenu a {
            display: inline-block;
            color: black;
            text-align: center;
            padding: 14px;
            text-decoration: none;
        }

        div.scrollmenu a:hover {
            background-color: #777;
        }

    </style>

    {{-- Custom CSS --}}
    @yield('custom-css')
</head>
<body>
    <main>
        <header>
            @include('layouts.frontend.navbar')
            @include('layouts.frontend.carousel')
        </header>
        <div class="container-fluid" style="margin-bottom: 100px">
            <div class="row">
                <div class="col-sm-12 col-md-2">
                    {{-- Horizontal --}}
                    <div class="row">
                        <div class="col-12">
                            <div class="col-12 text-center d-sm-none d-md-none">
                                <h2 style="margin-bottom: 10px; margin-top: 10px"><b>Menú</b></h2>
                                <i class="fas fa-arrows-alt-h"></i>
                                <p class="small">Deslize para ver todo el menú</p>
                            </div>
                            <div class="nav navbar-fixed-top flex-column nav-pills d-md-none d-sm-none d-block d-sm-block scrollmenu text-center" aria-orientation="vertical" role="tablist" id="pills-tab">
                                <a class="nav-link active" href="#todos" id="todos-tab" data-toggle="tab" role="tab" aria-controls="todos" aria-selected="true">Todos</a>
                                @foreach ($subcate as $menu)
                                    <a class="nav-link" href="#{{ $menu->slug }}" id="{{ $menu->slug }}-tab" data-toggle="tab" role="tab" aria-controls="{{ $menu->slug }}" aria-selected="true">{{ $menu->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                        {{-- Vertical --}}
                    <ul class="nav nav-pills d-md-block d-xl-block d-lg-block d-sm-block d-none" id="pills-tab" role="tablist" style="margin-top: 50px;">
                        <h2 style="margin-left: 15px; margin-bottom: 10px"><b>Menú</b></h2>
                        <li class="nav-item active" role="presentation">
                            <a class="nav-link" id="todos-tab" data-toggle="tab" href="#todos" role="tab" aria-controls="todos" aria-selected="true">Todos los Productos</a>
                        </li>
                        @foreach ($subcate as $menu)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="{{ $menu->slug }}-tab" data-toggle="tab" href="#{{ $menu->slug }}" role="tab" aria-controls="{{ $menu->slug }}" aria-selected="false">{{ $menu->name }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-12 col-md-10">
                   <div class="tab-content text-center">
                        @yield('content')
                   </div>
                </div>
            </div>
        </div>
    @include('layouts.frontend.modalGetCP')
    @include('layouts.frontend.footer')
    @include('layouts.frontend.modalLoginForm')
    @include('layouts.frontend.modalRegisterForm')
    </main>


    {{-- SCRIPTS --}}
    <script type="text/javascript" src="{{ asset('frontend/js/jquery-3.4.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/js/mdb.min.js') }}"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>
    <!-- Initializations -->
    <script type="text/javascript">
        new WOW().init();

        function maxLengthCheck(object){
            if (object.value.length > object.maxLength)
            object.value = object.value.slice(0, object.maxLength)
        }

        function validateCP(cp){
            if(cp.length === 5){
                $.ajax({
                    type: "GET",
                    url:"https://api-sepomex.hckdrk.mx/query/search_cp/"+cp+"?limit=1",
                    beforeSend: function () {
                        $('#btnGetCP').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Validando...').addClass('disabled');
                    },
                    success: function (data) {
                        validarCobertura(data["response"]["cp"]["0"]);
                    },
                    error: function(data, ajaxOptions, thrownError){
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: 'Código Postal Inválido'
                            })
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                    }
                });
            }
            else{
                $(function() {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    Toast.fire({
                        icon: 'error',
                        title: 'El Código Postal debe contar con 5 números'
                    })
                });
            }
        }

        function validarCobertura(code){
            $.ajax({
                type: "GET",
                url: '{{ route('validarCobertura') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    code: code
                },
                success: function(response){
                    if(response.success == true){
                        document.getElementById("label_enviar").innerHTML = "Enviar a: ";
                        document.getElementById("address").innerHTML = code;
                        setCP(code);
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'bottom-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'success',
                                title: response.message
                            });
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                    }
                    else{
                        $(function() {
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            Toast.fire({
                                icon: 'error',
                                title: response.message
                            });
                        });
                        $('#btnGetCP').find('.spin').remove();
                        $("#btnGetCP").html('Validar').removeClass('disabled');
                        $("#btnGetCP").addClass('enabled')
                    }
                },
                error: function(){
                    alert("Tenemos inconvenientes encontrando su Código Postal");
                }
            })
        }

        function setCP(code){
            $.ajax({
                type: "POST",
                url: '{{ route('setCP') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    code: code
                },
                success: function(response){
                    $("#modalCP").modal("hide");
                    modalMayorEdad();
                    $("cp").val("");
                },
                error: function(data){
                    alert(data)
                }
            });
        }

        function modalMayorEdad(){
            $("#modalMayorEdad").modal("show");

        }
        $("#btn_mayor_si").on("click", function() {
                $("#modalLoginForm").modal("show");
                $("modalMayorEdad").modal("hide");
            });

    </script>

    {{-- Custom JS --}}
    @yield('custom-js')



</body>
</html>
