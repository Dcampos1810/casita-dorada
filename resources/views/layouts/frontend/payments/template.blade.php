<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Compra Exitosa</title>
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('backend/img/logos/logo_casita.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/logos/logo_casita.png') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('frontend/css/mdb.min.css') }}" rel="stylesheet">
    {{-- Main SCSS --}}
    {{-- <link href="{{ asset('frontend/css/main.scss') }}" rel="stylesheet"> --}}
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    @yield('meta')

    {{-- Custom CSS --}}
</head>
<body>
    <main>
        <header>
            @include('layouts.frontend.navbar')
        </header>

        <div class="container-fluid" style="margin-bottom: 100px; margin-top: 80px">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    @yield('content')
                </div>
            </div>
        </div>
    @include('layouts.frontend.footer')
    </main>


    {{-- SCRIPTS --}}
    <script type="text/javascript" src="{{ asset('frontend/js/jquery-3.4.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('frontend/js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('frontend/js/mdb.min.js') }}"></script>
    {{-- SweetAlert 2 --}}
    <script src="{{ asset('backend/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- Initializations -->
    <script type="text/javascript" src="https://unpkg.com/default-passive-events"></script>

    @yield('custom-js')
    <script type="text/javascript">
        function getMyOrders(customer){
            $.ajax({
                type: "POST",
                url: '{{ route('getMyOrders') }}',
                data:{
                    "_token": "{{ csrf_token() }}",
                    customer_id: customer
                },
                success: function(response){
                    $.each(response, function(i){
                        var row = ''
                        row += '<tr id="'+response[i]["id"]+'">'
                            row += '<td style="width: 10%" align="center" class="align-middle">'
                                row += ''+response[i]["id"]+''
                            row += '</td>'
                            row += '<td style="width: 30%" class="align-middle">'
                                row += ''+response[i]["status"]+''
                            row += '</td>'
                            row += '<td style="width: 60% class="text-center" class="align-middle">'
                                row += '<div class="d-flex justify-content-center">'
                                    row += '<button class="btn btn-md btn-danger" type="button" onclick="downloadPDF('+response[i]["id"]+')" id="btnDownloadPDF['+response[i]["id"]+']">'
                                        row += '<i class="far fa-file-pdf mr-2" aria-hidden="true"></i>'
                                            row += 'Descargar'
                                    row += '</button>'
                                row += '</div>'
                            row += '</td>'
                        row += '</tr>'
                        $("#tableOrders>tbody").append(row);
                    })
                },
                error: function(response){
                    console.log("Error de Backend");
                }
            })
        }

        getMyOrders({{ auth()->user()->id }})
    </script>

</body>
</html>
