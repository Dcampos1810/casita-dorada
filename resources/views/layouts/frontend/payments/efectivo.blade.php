@extends('layouts.frontend.payments.template')

@section('content')
    <div class="jumbotron text-center">
        <h3 class="display-4 text-center">Pago con Efectivo</h5>
        <p class="h5">Pedido # {{ $pedido }}</p>
        <p class="lead">Se ha creado exitosamente tu orden</p>
        <hr class="my-4">
        <p class="text-justify">Nuestro personal estará preparando tu pedido, cuando el repartidor llegue a tu domicilio entrégale la cantidad de: <b>$ {{ $total }}</b></p>
            <p>GRACIAS POR SU COMPRA</p>
        <h6>Descargar Ticket de Compra</h6>
        <p class="small">También podrás descargarlo desde el menú: <i>Mis Compras</i></p>
        <div class="col-md-12 text-center mb-3">
            <input type="hidden" value="{{ $pedido }}" name="order" id="order">
            <button class="btn btn-bg btn-danger px-3" type="button" onclick="return downloadPDF()" id="btnDownloadPDF">
                <i class="far fa-file-pdf mr-3" aria-hidden="true"></i>
                Descargar
            </button>
        </div>
        <a class="h3 btn btn-block btn-success" href="{{ route('welcome') }}">Regresar al Inicio</a>
    </div>
@endsection

@section('custom-js')
    <script type="text/javascript">
        function downloadPDF(){
            var order = document.getElementById("order").value;
            $.ajax({
                url: '{{ route('downloadPDF') }}',
                method: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "order": order
                },
                xhrFields: {
                    responseType: 'blob'
                },
                beforeSend: function(){
                    $('#btnDownloadPDF').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Descargando...').addClass('disabled');
                },
                success: function (data) {
                    var blob = new Blob([data]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Ticket de Compra #"+order+".pdf";
                    link.click();
                    $('#btnDownloadPDF').find('.spin').remove();
                    $("#btnDownloadPDF").html('<i class="far fa-file-pdf mr-3" aria-hidden="true"></i>Descargar').removeClass('disabled');
                    $("#btnDownloadPDF").addClass('enabled')
                    // document.body.removeChild(link);
                },error: function(xhr, ajaxOptions, thrownError){
                    $('.loading').hide();
                    console.log(xhr.status+" ,"+" "+ajaxOptions+", "+thrownError);
                }
            }).done(function (response){

            });
        }


    </script>
@endsection


