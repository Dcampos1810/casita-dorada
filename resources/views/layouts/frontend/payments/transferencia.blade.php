@extends('layouts.frontend.payments.template')

@section('content')
    <div class="jumbotron text-center">
        <h3 class="display-4 text-center">Pago con Transferencia </h3>
        <p class="h5">Pedido # {{ $pedido }}</p>
        <p class="lead">Se ha creado exitosamente tu orden</p>
        <hr class="my-4">
        <p class="text-justify">Para garantizar tu orden, necesitamos que realices una Transferencia SPEI usando los siguientes datos:</p>
        <div class="row mb-2 border p-2">
            <div class="col-sm-6 col-6 border p-2">
                <b>Banco</b>
            </div>
            <div class="col-sm-6 col-6 border p-2">
                <b>SANTANDER</b>
            </div>
            <div class="col-sm-6 col-6 border p-2">
                <b>Cuenta</b>
            </div>
            <div class="col-sm-6 col-6 border p-2">
                <b>5579 0890 0144 6957</b>
            </div>
            <div class="col-sm-6 col-6 border p-2">
                <b>Referencia</b>
            </div>
            <div class="col-sm-6 col-6 border p-2">
                <b>Pedido #{{ $pedido }}</b>
            </div>
            <div class="col-sm-6 col-6 border p-2">
                <b>Monto</b>
            </div>
            <div class="col-sm-6 col-6 border p-2">
                <b>$ {{ $total }}</b>
            </div>
        </div>
        <p class="text-justify">Cuando recibamos tu comprobante de transferencia, procederemos a preparar tu pedido</p>
        <h6>Descargar Ticket de Compra</h6>
        <p class="small">También podrás descargarlo desde el menú: <i>Mis Compras</i></p>
        <div class="col-md-12 text-center mb-3">
            <input type="hidden" value="{{ $pedido }}" name="order" id="order">
            <button class="btn btn-bg btn-danger px-3" type="button" onclick="return downloadPDF()" id="btnDownloadPDF">
                <i class="far fa-file-pdf" aria-hidden="true"></i>
                Descargar
            </button>
        </div>
        <a class="h3 btn btn-block btn-success" href="{{ route('welcome') }}">Regresar al Inicio</a>
    </div>
@endsection

@section('custom-js')
    <script type="text/javascript">
        function downloadPDF(){
            var order = document.getElementById("order").value;
            $.ajax({
                url: '{{ route('downloadPDF') }}',
                method: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "order": order
                },
                xhrFields: {
                    responseType: 'blob'
                },
                beforeSend: function(){
                    $('#btnDownloadPDF').html('<span class="spinner-border spinner-border-sm mr-2 spin" role="status" aria-hidden="true"></span>Descargando...').addClass('disabled');
                },
                success: function (data) {
                    $('.loading').hide();
                    var blob = new Blob([data]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Ticket de Compra #"+order+".pdf";
                    link.click();
                    $('#btnDownloadPDF').find('.spin').remove();
                    $("#btnDownloadPDF").html('<i class="far fa-file-pdf mr-3" aria-hidden="true"></i>Descargar').removeClass('disabled');
                    $("#btnDownloadPDF").addClass('enabled')
                    // document.body.removeChild(link);
                },error: function(xhr, ajaxOptions, thrownError){
                    $('.loading').hide();
                    console.log(xhr.status+" ,"+" "+ajaxOptions+", "+thrownError);
                }
            });
        }
    </script>
@endsection

