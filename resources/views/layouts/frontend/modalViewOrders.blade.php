<div class="modal fade right" id="modalViewOrders" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead">Mis Compras</p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">×</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <i class="far fa-file-alt fa-4x mb-3 animated rotateIn"></i>
                </div>
                <hr>

                <table class="table table-bordered" style="width: 100%" id="tableOrders">
                    <thead>
                        <tr>
                            <th scope="col" align="center">N°</th>
                            <th scope="col">Status</th>
                            <th class="text-center" scope="col">Descargar PDF</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <a type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal">Cerrar</a>
            </div>
        </div>
    </div>
</div>
