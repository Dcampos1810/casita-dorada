<!-- Footer -->
<footer class="page-footer font-small fixed-bottom mt-5" style="background-color: #3F51B5">

    <!-- Copyright -->
    <div class="footer-copyright py-3 text-center row">
        <div class="col-sm-6 mb-1 ">
            © 2021 Copyright: <a href="{{ route('welcome') }}"> {{ config('app.name') }}</a>
        </div>
        <div class="col-sm-6" >
            <a href="https://dcampos18.github.io/portafolio/" target="blank" >Desarrollado por Daniel Campos</a>
        </div>
    </div>

    <!-- Copyright -->

  </footer>
  <!-- Footer -->
