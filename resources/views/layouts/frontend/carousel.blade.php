<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel" style="margin-top: 74px; position: relative; width: 100%;">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleFade" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleFade" data-slide-to="1"></li>
        <li data-target="#carouselExampleFade" data-slide-to="2"></li>
        <li data-target="#carouselExampleFade" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="{{ asset('frontend/img/carousel/1.jpg') }}"
            alt="First slide" style="height: auto">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('frontend/img/carousel/2.jpg') }}"
            alt="Second slide" style="height: auto">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('frontend/img/carousel/3.jpg') }}"
            alt="Third slide" style="height: auto">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('frontend/img/carousel/4.jpg') }}"
            alt="Third slide" style="height: auto">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
  </div>

  @section('custom-js')

    <script type="text/javascript">
        $('.carousel').carousel({
            touch: true, // default
            interval: 6000
        })

    </script>

@endsection
