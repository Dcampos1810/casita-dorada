<div class="modal fade" id="modalAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <span class="modal-title w-100 font-weight-bold h4" id="title-address">Añadir Dirección</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form form-group">
                            <input type="text" id="name_address" name="name_address" class="form-control " placeholder="Ejem. Casa, Trabajo, etc." required>
                            <label data-error="wrong" data-success="right" for="name">Nombre de la Dirección</label>
                        </div>
                    </div>
                    <div class="col-md-6 col-6">
                        <div class="md-form form-group">
                            <input type="number" id="postal_code" name="postal_code" class="form-control "  maxlength="5" oninput="maxLengthCheck(this)" required placeholder="Ej. 97000">
                            <label data-error="wrong" data-success="right" for="postal_code">Código Postal</label>
                            <input type="hidden" id="code" name="code">
                        </div>
                    </div>
                    <div class="col-md-6 col-6">
                        <div class="md-form form-group">
                            <button type="button" class="btn btn-info btn-md btn-rounded btn-block" onclick="validateCP($('#postal_code').val())" id="btnGetCP">Validar</button>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="md-form form-group">
                            <input type="text" id="city" name="city" class="form-control " readonly placeholder="Delegación / Municipio" required>
                            <label data-error="wrong" data-success="right" for="add_city">Delegación / Municipio</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label data-error="wrong" data-success="right">Seleccione su Colonia</label>
                        <div class="md-form form-group">
                            <select class="browser-default custom-select custom-select-md mb-3 form-group" id="colony" name="colony" required>
                                <option disabled select>Introduzca su Código Postal</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form form-group">
                            <input type="text" id="direccion" name="direccion" class="form-control " required placeholder="Ej. Calle 1223923">
                            <label data-error="wrong" data-success="right" for="direccion">Dirección</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form form-group">
                            <textarea type="text" id="references" name="references" class="md-textarea form-control" rows="2" required placeholder="Cerca del Centro"></textarea>
                            <label data-error="wrong" data-success="right" for="form8">Referencias</label>
                        </div>
                    </div>
                    <input type="hidden" id="methodAddress" name="methodAddress" value="add">
                    <input type="hidden" id="address_id" name="address_id">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-success" id="btn-modal-address">Añadir Dirección</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
