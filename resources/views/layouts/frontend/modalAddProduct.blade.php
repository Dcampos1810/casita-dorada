<!-- Central Modal Small -->
<div class="modal fade" id="modalAddProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">

    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title w-100" id="myModalLabel">Agregar Producto</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row d-flex justify-content-center">
                <div class="col-sm-12 col-md-6 mb-3">
                    <img src="" alt="Imagen" class="img-fluid" id="imagen" name="imagen">
                </div>
                <div class="col-sm-12 col-md-6 text-center mb-3">
                    <p><span id="title" class="h3">Nombre</span></p>
                    <p><span id="description" class="h6">Descripción</span></p>
                    <p>Total</p>
                    <p><span class="h4">$ <span id="label_subtotal" class="h4"></span></span></p>
                    <input type="hidden" id="price" name="price">
                    <input type="hidden" name="product_content_id" id="product_content_id">
                    <p>Cantidad</p>
                    <div class="d-flex justify-content-center mb-3">
                        <button type="button" class="btn btn-sm btn-danger" onclick="restar()" id="btn_restar"><span class="h2">-</span></button>
                        <input type="number" onkeyup="multiplicar(this.value);" class="form-control" id="quantity" name="quantity" value="1" min="1" style="width: 30%; text-align: center; margin-top: 15px; border: 0px solid; outline:none;">
                        <button type="button" class="btn btn-sm btn-primary" onclick="sumar()"><span class="h2">+</span></button>
                    </div>
                    <textarea name="comments" id="comments" cols="30" rows="2" placeholder="Comentarios de este producto" class="form-control"></textarea>

                </div>
            </div>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-md mr-auto" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success btn-md" id="btn_add_cart">Agregar al Carrito</button>
            </div>
        </div>
    </div>
</div>
<!-- Central Modal Small -->
