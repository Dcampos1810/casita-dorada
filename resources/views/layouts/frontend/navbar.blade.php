<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark indigo" style="position: fixed; width: 100%">
    <div class="container-xl">

        <a class="navbar-brand" href="{{ route('welcome') }}">
            <img src="{{ asset('backend/img/logos/logo_nombre_blanco.png') }}" height="48" alt="ccd logo" >
        </a>

        <ul class="navbar-nav ml-auto mr-auto d-sm-block d-none">
            <li class="nav-item">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalCP" data-backdrop="false">
                    Ingresar C.P.
                </button>
                <span class="h6 text-white" id="label_enviar" name="label_enviar"></span><span id="address" name="address" class="h6 text-white"></span>
            </li>
        </ul>
        <ul class="nav justify-content-end ml-auto navbar-nav nav-flex-icons">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
                    @guest
                        <a href="" class="dropdown-item" data-toggle="modal" data-target="#modalRegisterForm">Registrarse</a>
                        <a href="" class="dropdown-item" data-toggle="modal" data-target="#modalLoginForm">Iniciar Sesión</a>
                    @else
                        <form action="{{ route('getProfile') }}" method="POST" id="formProfile">
                            @csrf
                            <input type="hidden" name="customer_id" id="customer_id" value="{{ auth()->user()->id }}">
                            <a class="dropdown-item" href="javascript:;" onclick="document.getElementById('formProfile').submit();">Cuenta</a>
                        </form>
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#modalViewOrders">Mis compras</a>
                        <a href="{{ route('customer.logout') }}" class="dropdown-item">Cerrar Sesión</a>
                    @endguest
                </div>
            </li>
            @if(auth()->check())
                <li class="nav-item" style="margin-top: 3px">
                    <form action="{{ route('getCart') }}" method="POST" id="formCart">
                        @csrf
                        <a href="javascript:;" onclick="document.getElementById('formCart').submit();" class="nav-link">
                            <i class="fas fa-shopping-cart "></i>
                        </a>
                    </form>
                </li>
                <li class="nav-item" style="margin-left: -10px; margin-top: -6px; display: none" id="counter" name="counter" >
                    <span id="cartItems" name="cartItems" class="badge badge-pill badge-default"></span>
                </li>
            @endif
        </ul>


    </div>
</nav>

@extends('layouts.frontend.modalViewOrders')

