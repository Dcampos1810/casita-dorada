<div class="modal fade right" id="modalViewOrders" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-notify modal-info modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <span class="heading lead"></span>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">×</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <i class="far fa-file-alt fa-4x mb-3 animated rotateIn"></i>
                </div>
                <hr>
                {{-- Tabla de Items --}}
                <table class="table table-borderless" style="width: 100%" id="itemsTable">
                    <thead class="info-color-dark">
                        <tr>
                            <th class="text-left"><h6 class="text-white">Imagen</h6></th>
                            <th class="text-left"><h6 class="text-white">Producto</h6></th>
                            <th class="text-right"><h6 class="text-white">Total</h6></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order->order_details as $item)
                            <tr id="{{ $item->id }}">
                                <td style="width: 25%">
                                    <img src="../{{ $item->imagenProducto }}" alt="item" width="100" height="100" class="rounded"/>
                                </td>
                                <td style="width: 45%">
                                    <div class="row">
                                        <div class="col-12 ml-auto mr-auto">
                                            <p class="h6">{{ $item->name }}</p>
                                            <div class="d-flex justify-content-start mb-1">
                                                <input type="number"  class="form-control" value="{{ $item->quantity }}" min="1" disabled style="width: 35%; text-align: center; margin-top: -5px; border: 0px solid; outline:none;">
                                            </div>
                                            <p><b>Precio: ${{ $item->price }} C/U</b></p>
                                            @if($item->comments != "")
                                                <p class="small"><b>Comentarios:</b> {{ $item->comments }}</p>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 35%" class="justify-content-end" align="right">
                                    <span class="h6 total-producto " onchange="subtotal();" id="label_subtotal[{{ $item->id }}]">$ {{ $item->total }}</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot id="tfoot-items">
                        <tr>
                            <td style="width: 30%"></td>
                            <td align="right" style="width: 45%">
                                <p class="h6">Subtotal: </p>
                            </td>
                            <td align="right" style="width: 35%" colspan="1">
                                <span class="h6" id="subtotal" name="subtotal">$ </span>
                            </td>
                        </tr>
                        <tr id="row-envio">
                            <td style="width: 30%"></td>
                            <td align="right" style="width: 45%">
                                <p class="h6">Costo de envío: </p>
                            </td>
                            <td align="right" style="width: 35%" colspan="1">
                                <span class="h6" id="envio" name="envio"></span>
                            </td>
                        </tr>
                    </tfoot>
                </table>

                {{-- Total --}}
                <table class="table table-borderless" style="width: 100%; margin-top: -15px">
                    <tbody>
                        <tr>
                            <td style="width: 30%"></td>
                            <td align="right" style="width: 45%">
                                <p class="h6">Total: </p>
                            </td>
                            <td align="right" style="width: 35%" colspan="1">
                                <span class="h6" id="total" name="total">{{ $order->total }}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>

                {{-- Dirección --}}
                <div class="container" style="margin-bottom: 50px">
                    <h5 class="text-center mb-5"><b>Enviado a:</b></h5>
                    <div class="container">
                        <div class="card">
                            <div class="card-horizontal" style="display: flex; flex: 1 1 auto;">
                                <div class="card-body">
                                    <h4 class="card-title" id="profile_name_address"</h4>
                                    <p class="card-text" id="profile_address">{{ $order->address }} Col. {{ $order->colony }} C.P. {{ $order->codigoPostal }} {{ $order->city }}</p>
                                    <p class="card-text" id="profile_references">{{ $order->references }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Formas de Pago --}}
                <div class="container" style="margin-bottom: 50px">
                    <h5 class="text-center mb-5"><b>Forma de Pago</b></h5>
                    <table class="table table-borderless" style="width: 100%">
                        <tbody>
                            <tr>
                                <td style="width: 50%" class="align-middle justify-content-center">
                                    <img src="https://casitadorada.com/{{ $order->imagenPago }}" alt="logo" class="img-fluid" width="64" height="64">
                                </td>
                                <td style="width: 50%" class="align-middle justify-content-center">
                                    <p class="h6">{{ $order->metodoPago }}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <a type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal">Cerrar</a>
            </div>
        </div>
    </div>
</div>
