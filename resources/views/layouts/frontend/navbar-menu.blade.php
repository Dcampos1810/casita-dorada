<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark indigo mr-auto" style="position: fixed; width: 100%">

    <ul class="navbar-nav mr-auto ml-auto">
        <li class="nav-item" style="margin-left: 30px">
            <a class="navbar-brand" href="{{ route('menu') }}">
                <img src="{{ asset('backend/img/logos/logo_nombre_blanco.png') }}" height="48" alt="ccd logo" >
            </a>
        </li>
    </ul>
</nav>
