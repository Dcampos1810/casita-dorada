<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Registrarse</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <form action="{{ route('customer.store') }}" method="POST" id="formRegister">
                    @csrf
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix black-text"></i>
                        <input type="text" id="name_register" name="name_register" class="form-control validate" required>
                        <label data-error="wrong" data-success="right" for="name_register">Nombre</label>
                    </div>
                    <div class="md-form mb-5">
                        <i class="fas fa-phone-alt prefix blue-text"></i>
                        <input type="number" id="phone_register" name="phone_register" class="form-control validate" maxlength="10" oninput="maxLengthCheck(this)" required>
                        <label data-error="wrong" data-success="right" for="phone_register">Teléfono</label>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fas fa-lock prefix grey-text"></i>
                        <input type="password" id="password-register" name="password_register" class="form-control validate" required autocomplete="on">
                        <label data-error="wrong" data-success="right" for="password_register">Contraseña</label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" id="btn_register">Registrarse</button>
                </form>
            </div>
            <div class="modal-footer" style="margin: auto">
                <p class="btn-block text-center mb-2">¿Ya tienes un Usuario?</p>
                <button type="button" class="btn btn-outline-info waves-effect btn-block" data-dismiss="modal" onclick="openLoginModal()">Inicia Sesión</button>
            </div>
        </div>
    </div>
</div>



