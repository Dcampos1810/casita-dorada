<!-- Central Modal Medium Warning -->
<div class="modal fade" id="modalMayorEdad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-notify modal-warning" role="document">
  <!--Content-->
  <div class="modal-content">
    <!--Header-->
    <div class="modal-header">
      <p class="heading lead">¡Atención!</p>

      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="white-text">&times;</span>
      </button>
    </div>

    <!--Body-->
    <div class="modal-body">
      <div class="text-center">
        <i class="fas fa-exclamation-triangle fa-4x mb-3 animated rotateIn"></i>
        <p>¿Eres Mayor de Edad?</p>
      </div>
    </div>

    <!--Footer-->
    <div class="modal-footer justify-content-center">
      <a type="button" class="btn btn-warning" id="btn_mayor_si" data-dismiss="modal">Si</i></a>
      <a type="button" class="btn btn-outline-warning waves-effect" data-dismiss="modal">No</a>
    </div>
  </div>
  <!--/.Content-->
</div>
</div>
<!-- Central Modal Medium Warning-->
