<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Iniciar Sesión</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="md-form mb-5">
                    <i class="fas fa-phone-alt prefix blue-text"></i>
                    <input type="text" id="phone" name="phone" class="form-control validate" required maxlength="10" oninput="maxLengthCheck(this)">
                    <label data-error="wrong" data-success="right" for="defaultForm-email">Teléfono</label>
                </div>

                <div class="md-form mb-4">
                    <i class="fas fa-lock prefix grey-text"></i>
                    <input type="password" id="password" name="password" class="form-control validate" required autocomplete="on">
                    <label data-error="wrong" data-success="right" for="defaultForm-pass">Contraseña</label>
                </div>

                <button type="button" class="btn btn-primary btn-block" id="btn-login">Iniciar Sesión</button>
            </div>
            <div class="modal-footer" style="margin: auto">
                <p class="btn-block text-center mb-2">¿No tienes un Usuario?</p>
                <button type="button" class="btn btn-outline-info waves-effect btn-block" data-dismiss="modal" onclick="openRegisterModal()">Regístrate</button>
            </div>
        </div>
    </div>
</div>
