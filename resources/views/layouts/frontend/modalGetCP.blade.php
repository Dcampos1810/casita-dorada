<!-- Modal -->
  <div class="modal fade" id="modalCP" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body text-center" style="margin: auto">
            <p class="mb-3 h4 mt-3"><b>¿CUÁL ES TU CÓDIGO POSTAL?</b></p>
            <p class="mb-3 h5 mt-3">Queremos verificar si podemos hacer entregas a este Código Postal</p>
                <input type="number" id="cp" name="cp" placeholder="Ej. 97000" maxlength="5" class="form-control mb-3" oninput="maxLengthCheck(this)" required style="width: 50%; margin: auto; text-align: center">
            <a id="btnGetCP" class="btn btn-info btn-md" href="javascript:;" onclick="validateCP($('#cp').val())">Validar</a>

        </div>
        <div class="modal-footer" style="margin: auto">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_close_modalCP">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
{{-- Modal --}}

