<!-- Central Modal Small -->
<div class="modal fade" id="modalViewProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">

    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">Agregar Producto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row d-flex justify-content-center">
                    <div class="col-sm-12 col-md-6 mb-3">
                        <img src="" alt="Imagen" class="img-fluid" id="imagen-view" name="imagen-view">
                    </div>
                    <div class="col-sm-12 col-md-6 text-center mb-3">
                        <p><span id="title-view" class="h3">Nombre</span></p>
                        <p><span id="description-view" class="h6">Descripción</span></p>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-md mr-auto ml-auto" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- Central Modal Small -->
