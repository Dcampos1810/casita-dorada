<!DOCTYPE html>
<html>
<head>
    <title>Pedido #{{ $pedido['order_id'] }}</title>
</head>
<body>
    <h1>Venta de Casita Cheviche Dorada</h1>
    <h3>Comprado por: {{ $pedido['customer'] }}</h3>
    <p>Método de Pago: {{ $pedido['payment_method'] }}</p>
    <p>Monto total: $ {{ $pedido['total'] }}</p>


</body>
</html>
