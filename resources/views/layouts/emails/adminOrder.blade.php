<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://casitadorada.com/frontend/css/bootstrap.min.css'" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://casitadorada.com/frontend/css/mdb.min.css" rel="stylesheet">
    <style type="text/css">
        .bg {
            background: url("https://casitadorada.com/backend/img/logos/logo_casita.png");
            background-size: cover contain;
            position: absolute;
            height: 150px;
            width: 150px;
            margin-left: 50px;
        }
    @page {
      margin: 0;
    }

    table {
      width: 100%;
      page-break-inside:auto;
    }

    </style>
</head>
<body>
    <main>
        <div class="bg" style="margin-top: 30px">
        </div>
        <div class="container" style="position: relative; margin-left: 50px; margin-right: 50px; margin-top: 25px">
            <table class="table table-borderless" style="margin-bottom: 50px">
                <tr>
                    <td align="right">
                        <p class="h4">Pedido # {{ $order->id }} <br>
                            Fecha: {{ $fecha }}</p>
                    </td>
                </tr>
                <tr>
                    <td align="center"><h3 class="title-font">COMPROBANTE DE COMPRA</h3></td>
                </tr>
                <tr>
                    <td align="left">Cliente: #{{ $order->customer_id }} <br>
                        Nombre Completo: {{ $order->nombre }} <br>
                        Dirección: {{ $order->address }}  Col. {{ $order->colony }} C.P. {{ $order->code }}, {{ $order->city }}<br>
                        Teléfono: {{ $order->phone }}
                    </td>
                </tr>
            </table>

            <table class="table table-borderless table-sm">
                <thead class="black white-text">
                    <tr>
                        <th align="left">DESCRIPCIÓN</th>
                        <th align="center">CANT.</th>
                        <th align="center">PRECIO</th>
                        <th align="right">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($details as $detail)
                        <tr>
                            <td align="justify">{{ $detail->name }}</td>
                            <td align="center">{{ $detail->quantity }}</td>
                            <td align="center">$ {{ $detail->price }}</td>
                            <td align="right">$ {{ $detail->total }}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" align="right">Subtotal</td>
                        <td align="right">$ {{ $subtotal }}</td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">Costo de Envío</td>
                        <td align="right">$ {{ $order->precioEnvio }}</td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">Total con Envío</td>
                        <td align="right">$ {{ $order->total }}</td>
                    </tr>
                </tfoot>
            </table>
            <table class="table table-borderless">
                <tbody>
                    <tr style="margin-bottom: 30px">
                        <td style="width: 40%">TIPO DE PAGO<br>
                            Efectivo / Contraentrega
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </main>
</body>
</html>
