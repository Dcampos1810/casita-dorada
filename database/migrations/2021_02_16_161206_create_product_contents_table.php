<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_contents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->enum('type', ['GRANDE', 'MEDIANO', 'PLATILLO', 'UNIDAD', 'ENTRADA', 'BEBIDA']);
            $table->integer('price');
            $table->enum('status', ['A', 'I'])->default('I');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_contents', function (Blueprint $table){
            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('product_contents');
    }
}
