<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartshopItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cartshop_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cartshop_id');
            $table->unsignedBigInteger('product_content_id');
            $table->integer('quantity');
            $table->integer('price');
            $table->integer('total');
            $table->text('comments')->nullable();
            $table->timestamps();

            $table->foreign('cartshop_id')->references('id')->on('cartshops')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('product_content_id')->references('id')->on('product_contents')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cartshop_items', function (Blueprint $table) {
            $table->dropForeign(['cartshop_id']);
            $table->dropForeign(['product_content_id']);
        });
        Schema::dropIfExists('cartshop_items');
    }
}
