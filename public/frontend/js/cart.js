document.getElementById('envio').innerHTML = "$ 0";
function multiplicar(cantidad, precio, id){

    var total = 0;
    var cart = 'total';

    var cantidad = parseInt(cantidad);
    var precio = parseInt(precio);
    var id = parseInt(id);

    total = (cantidad * precio);

    total = (total == null || total == undefined || total == "") ? 0 : total;

    var price = document.getElementById(id);

    price = total;

    document.getElementById(id).innerHTML = "$ " + total;

    $.ajax({
        url: '{{ route("update.item.cart") }}',
        method: 'put',
        data: {
            "_token": "{{ csrf_token() }}",
            "id": id,
            "quantity": cantidad
        },
        success: function (data) {
            //do something
        },error: function(xhr, ajaxOptions, thrownError){
                console.log(xhr.status+" ,"+" "+ajaxOptions+", "+thrownError);
            }
        }
    );

}

function subtotal(){
    var sum = 0;
    var total = 0;
    var envio = 0;

    $(".total").each(function() {
        var valueArr = $.trim( $( this ).html() ).split( "$ " );
        sum += parseFloat( valueArr[ 1 ]);
    });
    $(".envio").each(function() {
        var valueArr = $.trim( $( this ).html() ).split("$ ");
        envio += parseFloat( valueArr[1]);
    });
    total = (sum + envio);
    document.getElementById('subtotal').innerHTML = "$ "+total;
}

$("span").on('DOMSubtreeModified', function () {
    subtotal();
});

subtotal();

function validateCP(cp){
    if(cp.length === 5){
        $('#md_add_select_colony').empty();
        $.ajax({
            type: "GET",
            url:"https://api-sepomex.hckdrk.mx/query/info_cp/"+cp,
            dataType: "json",
            success: function (data) {
                $("#add_city").val(data[0]["response"]["municipio"]);
                $("#add_state").val(data[0]["response"]["estado"]);

            },
            error: function(data, ajaxOptions, thrownError){
                alert(JSON.stringify(data["responseJSON"]["error_message"]));
            }
        });
    }
    else{
        alert("El CP debe tener 5 Caracteres");
    }
}

function sendCP(postal_code){
    var postalCode = postal_code;
    $.ajax({
        type: "GET",
        url: "{{ route('getPCPrice') }}",
        dataType: "json",
        data: {
            "_token": "{{ csrf_token() }}",
            "cp": postalCode
        },
        beforeSend: function() {
            $('.loading').show();
        },
        success: function (data) {
            if(Object.keys(data).length > 0){
                $('#modalCP').modal('hide');
                $('.loading').hide();
                document.getElementById('add_city').innerHTML = "";
                document.getElementById('add_state').innerHTML = "";
                document.getElementById('postal_code').innerHTML = "";
                $("#div_shipping_type").show();
                document.getElementById('envio').innerHTML = "$ "+data[0]["price"];
                countItemsCart()
            }
            else{
                $("#div_shipping_type").hide();
                document.getElementById('envio').innerHTML = "$ 0"
                alert("Todavia no tenemos servicio por tu área");
            }

        },
        error: function(data, ajaxOptions, thrownError){
            console.log("FUCK IT");
        }
    });
}

function getShippingType(select){
    var shipping_type = select.value;
    var envio = document.getElementById("envio");

    if(shipping_type === "estandar")
    {
        $("#div-ship-estandar").show();
        $("#div_shipping_type").hide();
        document.getElementById('envio').innerHTML = "$ 0";
        countItemsCart()

    }
    else if(shipping_type === "express")
    {
        $("#div-ship-estandar").hide();
        $("#div_shipping_type").show();
        document.getElementById('envio').innerHTML = "$ 120";
        countItemsCart()
    }
    else{
        $("#div-ship-estandar").hide();
        $("#div_shipping_type").hide();
        document.getElementById('envio').innerHTML = "$ 0";
        countItemsCart();
    }
}

function getAddressCP(select){
    var postal_code = select.value;
    sendCP(postal_code);
}


function countItemsCart(){
    var sum = 0;
    var button = document.getElementById("btn_checkout");
    var envio = document.getElementById("envio").innerHTML;
    $(".quantity").each(function() {
        sum += parseFloat( this.value );
        if(envio == "$ 0")
        {
            button.classList.add('btn-info');
            button.disabled = true;
            button.innerText = "Seleccione el Tipo de Envío";
        }
        else{
            if(sum >= 3){
                button.classList.remove('btn-info');
                button.classList.remove('btn-warning');
                button.classList.add('btn-success');
                button.disabled = false;
                button.innerText = "Procesar Pedido";
            }
            else{
                button.classList.remove('btn-info');
                button.classList.remove('btn-success');
                button.classList.add('btn-warning');
                button.disabled = true;
                button.innerText = "Mínimo 3 Artículos";
            }
        }

    });
}
countItemsCart();
