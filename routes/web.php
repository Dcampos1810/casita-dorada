<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::prefix('/admin')->name('admin.')->group(function() {

    // Rutas de Inicio de Sesión y Registro de Usuarios
    Route::get('/login', [App\Http\Controllers\Backend\LoginController::class, 'showLogin'])->name('showLogin');
    Route::post('/login', [App\Http\Controllers\Backend\LoginController::class, 'login'])->name('login');
    Route::get('/register', [App\Http\Controllers\Backend\RegisterController::class, 'showRegister'])->name('showRegister');
    Route::post('/register', [App\Http\Controllers\Backend\RegisterController::class, 'store'])->name('store');

    Route::group(['middleware' => ['auth:admin']], function () {

        // Dashboard
        Route::get('/dashboard', [App\Http\Controllers\Backend\DashboardController::class, 'dashboard'])->name('dashboard');

        // Rutas de Usuarios
        Route::get('/users', [App\Http\Controllers\Backend\UsersController::class, 'getUsers'])->name('users');
        Route::post('/saveUser', [App\Http\Controllers\Backend\UsersController::class, 'saveUser'])->name('users.store');
        Route::post('/getUser', [App\Http\Controllers\Backend\UsersController::class, 'getUser'])->name('users.show');
        Route::put('/updateUser', [App\Http\Controllers\Backend\UsersController::class, 'updateUser'])->name('users.update');
        Route::post('/deleteUser', [App\Http\Controllers\Backend\UsersController::class, 'deleteUser'])->name('users.delete');
        Route::get('/logout', [App\Http\Controllers\Backend\LoginController::class, 'logout'])->name('logout');

        // Rutas de Empresas
        Route::get('/enterprises', [App\Http\Controllers\Backend\EnterprisesController::class, 'getEnterprises'])->name('enterprises');
        Route::post('/saveEnterprise', [App\Http\Controllers\Backend\EnterprisesController::class, 'saveEnterprise'])->name('enterprises.store');
        Route::post('/getEnterprise', [App\Http\Controllers\Backend\EnterprisesController::class, 'getEnterprise'])->name('enterprises.show');
        Route::put('/updateEnterprise', [App\Http\Controllers\Backend\EnterprisesController::class, 'updateEnterprise'])->name('enterprises.update');
        Route::post('/deleteEnterprise', [App\Http\Controllers\Backend\EnterprisesController::class, 'deleteEnterprise'])->name('enterprises.delete');

        // Rutas de Categorias
        Route::get('/categories', [App\Http\Controllers\Backend\CategoriesController::class, 'getCategories'])->name('categories');
        Route::post('/saveCategory', [App\Http\Controllers\Backend\CategoriesController::class, 'saveCategory'])->name('categories.store');
        Route::post('/getCategory', [App\Http\Controllers\Backend\CategoriesController::class, 'getCategory'])->name('categories.show');
        Route::put('/updateCategory', [App\Http\Controllers\Backend\CategoriesController::class, 'updateCategory'])->name('categories.update');
        Route::post('/deleteCategory', [App\Http\Controllers\Backend\CategoriesController::class, 'deleteCategory'])->name('categories.delete');

        // Rutas de Subcategorias
        Route::get('/subcategories', [App\Http\Controllers\Backend\SubcategoriesController::class, 'getSubcategories'])->name('subcategories');
        Route::post('/saveSubcategory', [App\Http\Controllers\Backend\SubcategoriesController::class, 'saveSubcategory'])->name('subcategories.store');
        Route::post('/getSubcategory', [App\Http\Controllers\Backend\SubcategoriesController::class, 'getSubcategory'])->name('subcategories.show');
        Route::put('/updateSubcategory', [App\Http\Controllers\Backend\SubcategoriesController::class, 'updateSubcategory'])->name('subcategories.update');
        Route::post('/deleteSubcategory', [App\Http\Controllers\Backend\SubcategoriesController::class, 'deleteSubcategory'])->name('subcategories.delete');

        // Rutas de Productos
        Route::get('/products', [App\Http\Controllers\Backend\ProductsController::class, 'getProducts'])->name('products');
        Route::post('/saveProduct', [App\Http\Controllers\Backend\ProductsController::class, 'saveProduct'])->name('products.store');
        Route::post('/getProduct', [App\Http\Controllers\Backend\ProductsController::class, 'getProduct'])->name('products.show');
        Route::put('/updateProduct', [App\Http\Controllers\Backend\ProductsController::class, 'updateProduct'])->name('products.update');
        Route::post('/deleteProduct', [App\Http\Controllers\Backend\ProductsController::class, 'deleteProduct'])->name('products.delete');

        // Rutas de Contenido de Productos
        Route::get('/productContents', [App\Http\Controllers\Backend\ProductContentsController::class, 'getProductContents'])->name('productContents');
        Route::post('/saveProductContent', [App\Http\Controllers\Backend\ProductContentsController::class, 'saveProductContent'])->name('productContents.store');
        Route::post('/getProductContent', [App\Http\Controllers\Backend\ProductContentsController::class, 'getProductContent'])->name('productContents.show');
        Route::put('/updateProductContent', [App\Http\Controllers\Backend\ProductContentsController::class, 'updateProductContent'])->name('productContents.update');
        Route::post('/deleteProductContent', [App\Http\Controllers\Backend\ProductContentsController::class, 'deleteProductContent'])->name('productContents.delete');

        // Rutas de Bebidas
        Route::get('/bebidas', [App\Http\Controllers\Backend\BebidasController::class, 'getBebidas'])->name('bebidas');
        Route::post('/saveBebida', [App\Http\Controllers\Backend\BebidasController::class, 'saveBebida'])->name('bebidas.store');
        Route::post('/getBebida', [App\Http\Controllers\Backend\BebidasController::class, 'getBebida'])->name('bebidas.show');
        Route::put('/updateBebida', [App\Http\Controllers\Backend\BebidasController::class, 'updateBebida'])->name('bebidas.update');
        Route::post('/deleteBebida', [App\Http\Controllers\Backend\BebidasController::class, 'deleteBebida'])->name('bebidas.delete');

        // Rutas de Pedidos
        Route::get('/orders', [App\Http\Controllers\Backend\OrdersController::class, 'getOrders'])->name('orders');
        Route::post('/adminDownloadPDF', [App\Http\Controllers\Backend\OrdersController::class, 'adminDownloadPDF'])->name('adminDownloadPDF');

        //  Rutas de Detalles del Pedido
        Route::get('/orderDetails', [App\Http\Controllers\Backend\OrderDetailsController::class, 'getOrderDetails'])->name('orderDetails');
        Route::post('/getOrderDetail', [App\Http\Controllers\Backend\OrderDetailsController::class, 'getOrderDetail'])->name('orderDetails.show');
        Route::put('/updateOrderDetail', [App\Http\Controllers\Backend\OrderDetailsController::class, 'updateOrderDetail'])->name('orderDetails.update');
        Route::post('/deleteOrderDetail', [App\Http\Controllers\Backend\OrderDetailsController::class, 'deleteOrderDetail'])->name('orderDetails.delete');

        // Rutas de Costos de Envio
        Route::get('/shippings', [App\Http\Controllers\Backend\ShippingsController::class, 'getShippings'])->name('shippings');
        Route::post('/saveShipping', [App\Http\Controllers\Backend\ShippingsController::class, 'saveShipping'])->name('shippings.store');
        Route::post('/getShipping', [App\Http\Controllers\Backend\ShippingsController::class, 'getShipping'])->name('shippings.show');
        Route::put('/updateShipping', [App\Http\Controllers\Backend\ShippingsController::class, 'updateShipping'])->name('shippings.update');
        Route::post('/deleteShipping', [App\Http\Controllers\Backend\ShippingsController::class, 'deleteShipping'])->name('shippings.delete');

        // Rutas de Codigos Postales
        Route::get('/postalCodes', [App\Http\Controllers\Backend\PostalCodesController::class, 'getPostalCodes'])->name('postalCodes');
        Route::post('/savePostalCode', [App\Http\Controllers\Backend\PostalCodesController::class, 'savePostalCode'])->name('postalCodes.store');
        Route::post('/getPostalCode', [App\Http\Controllers\Backend\PostalCodesController::class, 'getPostalCode'])->name('postalCodes.show');
        Route::put('/updatePostalCode', [App\Http\Controllers\Backend\PostalCodesController::class, 'updatePostalCode'])->name('postalCodes.update');
        Route::post('/deletePostalCode', [App\Http\Controllers\Backend\PostalCodesController::class, 'deletePostalCode'])->name('postalCodes.delete');

        // Rutas de Formas de Pago
        Route::get('/payments', [App\Http\Controllers\Backend\PaymentsController::class, 'getPayments'])->name('payments');
        Route::post('/savePayment', [App\Http\Controllers\Backend\PaymentsController::class, 'savePayment'])->name('payments.store');
        Route::post('/getPayment', [App\Http\Controllers\Backend\PaymentsController::class, 'getPayment'])->name('payments.show');
        Route::put('/updatePayment', [App\Http\Controllers\Backend\PaymentsController::class, 'updatePayment'])->name('payments.update');
        Route::post('/deletePayment', [App\Http\Controllers\Backend\PaymentsController::class, 'deletePayment'])->name('payments.delete');
    });
});

Route::prefix('/')->group(function (){
    Route::get('/', [App\Http\Controllers\Frontend\WelcomeController::class, 'welcome'])->name('welcome');

    //Rutas de Productos
    Route::get('/especialidades', [App\Http\Controllers\Frontend\EspecialidadesController::class, 'getEspecialidades'])->name('getEspecialidades');
    Route::get('/cheviches', [App\Http\Controllers\Frontend\ChevichesController::class, 'getCheviches'])->name('getCheviches');
    Route::get('/empanizados', [App\Http\Controllers\Frontend\EmpanizadosController::class, 'getEmpanizados'])->name('getEmpanizados');
    Route::get('/pastas', [App\Http\Controllers\Frontend\PastasController::class, 'getPastas'])->name('getPastas');
    Route::get('/aguachiles', [App\Http\Controllers\Frontend\AguachilesController::class, 'getAguachiles'])->name('getAguachiles');
    Route::get('/hits', [App\Http\Controllers\Frontend\HitsController::class, 'getHits'])->name('getHits');
    Route::get('/cocteles', [App\Http\Controllers\Frontend\CoctelesController::class, 'getCocteles'])->name('getCocteles');
    Route::get('/bebidas', [App\Http\Controllers\Frontend\BebidasController::class, 'getBebidas'])->name('getBebidas');
    Route::get('/cervezas', [App\Http\Controllers\Frontend\CervezasController::class, 'getCervezas'])->name('getCervezas');
    Route::get('/micheladas', [App\Http\Controllers\Frontend\MicheladasController::class, 'getMicheladas'])->name('getMicheladas');
    Route::get('/cocteleria', [App\Http\Controllers\Frontend\CocteleriaController::class, 'getCocteleria'])->name('getCocteleria');

    // Obtener el Producto
    Route::post('/getProduct', [App\Http\Controllers\Frontend\ProductsController::class, 'getProduct'])->name('getProduct');


    //Add to Cart
    Route::post('/addToCart', [App\Http\Controllers\Frontend\CartController::class, 'addToCart'])->name('addToCart');

    // Validar que el CP esté en nuestra cobertura
    Route::get('/validarCobertura', [App\Http\Controllers\Backend\PostalCodesController::class, 'validarCobertura'])->name('validarCobertura');
    Route::post('/setCP', [App\Http\Controllers\Backend\PostalCodesController::class, 'setCP'])->name('setCP');

    // Rutas de Inicio de Sesión y Registo de Clientes
    Route::post('/register', [App\Http\Controllers\Frontend\RegisterController::class, 'store'])->name('customer.store');
    Route::post('/login', [App\Http\Controllers\Frontend\LoginController::class, 'login'])->name('customer.login');
    Route::get('/logout', [App\Http\Controllers\Frontend\LoginController::class, 'destroy'])->name('customer.logout');
    Route::post('/verifyCustomer', [App\Http\Controllers\Frontend\RegisterController::class, 'verifyCustomer'])->name('verifyCustomer');

    // Ruta del Menú Casita Dorada
    Route::get('/menu', [App\Http\Controllers\Frontend\MenuController::class, 'getMenu'])->name('menu');

    Route::group(['middleware' => ['auth:customers']], function () {

        // Rutas del Carrito de Compras
        Route::post('/getCountItemsCart', [App\Http\Controllers\Frontend\CartController::class, 'getCountItemsCart'])->name('getCountItemsCart');
        Route::match(array('GET', 'POST'), '/cart', [App\Http\Controllers\Frontend\CartController::class, 'getCart'])->name('getCart');
        Route::put('/updateItemCart', [App\Http\Controllers\Frontend\CartController::class, 'updateItemCart'])->name('updateItemCart');
        Route::delete('/deleteItemCart', [App\Http\Controllers\Frontend\CartController::class, 'deleteItemCart'])->name('deleteItemCart');
        Route::post('/getAdicional', [App\Http\Controllers\Frontend\ProductsController::class, 'getAdicional'])->name('getAdicional');
        Route::post('/updateCart', [App\Http\Controllers\Frontend\CartController::class, 'updateCart'])->name('updateCart');

        // Rutas del Perfil de Usuario
        Route::match(array('GET', 'POST'), '/profile', [App\Http\Controllers\Frontend\CustomersController::class, 'getProfile'])->name('getProfile');
        Route::post('/getAddresses', [App\Http\Controllers\Frontend\CustomerAddressesController::class, 'getAddresses'])->name('getAddresses');
        Route::posT('/saveAddress', [App\Http\Controllers\Frontend\CustomerAddressesController::class, 'saveAddress'])->name('saveAddress');
        Route::get('/getAddress', [App\Http\Controllers\Frontend\CustomerAddressesController::class, 'getAddress'])->name('getAddress');
        Route::put('/updateAddress', [App\Http\Controllers\Frontend\CustomerAddressesController::class, 'updateAddress'])->name('updateAddress');
        Route::delete('/deleteAddress', [App\Http\Controllers\Frontend\CustomerAddressesController::class, 'deleteAddress'])->name('deleteAddress');
        Route::post('/getMyOrders', [App\Http\Controllers\Frontend\CustomersController::class, 'getMyOrders'])->name('getMyOrders');
        Route::post('/orderDetails', [App\Http\Controllers\Frontend\OrdersController::class, 'getOrderDetails'])->name('getOrderDetails');

        //Ruta de Resumen del Pedido
        Route::post('/createOrder', [App\Http\Controllers\Frontend\OrdersController::class, 'createOrder'])->name('createOrder');
        Route::post('/getShippingPrice', [App\Http\Controllers\Backend\PostalCodesController::class, 'getShippingPrice'])->name('getShippingPrice');

        // Ruta para descargar Ticket de Compra PDF
        Route::post('/downloadPDF', [App\Http\Controllers\Frontend\OrdersController::class, 'downloadPDF'])->name('downloadPDF');
    });

});



